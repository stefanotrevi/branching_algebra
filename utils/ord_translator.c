#include "lits/cnf.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define RED "\x1b[91m"
#define GREEN "\x1b[92m"
#define YELLOW "\x1b[93m"
#define BLUE "\x1b[94m"
#define MAGENTA "\x1b[95m"
#define CYAN "\x1b[96m"
#define RESET "\x1b[0m"
#define PRINT_N 4

int main(int argc, char **argv)
{
    int color = argc > 1 && !strcmp(argv[1], "-color");

    for (int c = getchar(), enable = 0, old = 0; c != EOF; old = c, c = getchar())
        if (enable && isalpha(c))
        {
            static char buf[64];
            static const char *const colors[PRINT_N] = {RED, GREEN, BLUE, YELLOW};
            
            clsToStr(buf, clsInsChar(clsInit(), (char)c, old == '!' ? L_POS : L_NEG));
            printf("%s%s", color ? colors[enable - 1] : "", buf);
        }
        else if (c == '&')
            printf(" v ");
        else if (c == '|')
        {
            printf(", %s", color ? RESET : "");
            if (++enable > PRINT_N) // uso enable per contare le clausole
            {
                putchar('\n');
                enable = 1;
            }
        }
        // non stampo i punti esclamativi, le parentesi e gli spazi
        else if (c == '=')
        {
            enable = 1;
            puts("= {");
        }
        else if (c == ';')
        {
            enable = 0;
            printf("%s};\n", color ? RESET : "");
        }
        else if (!enable)
            putchar(c);

    putchar('\n');

    return 0;
}
