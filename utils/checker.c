#include "algs/ralg.h"
#include "files.h"
#include "lut.h"
#include "rels/brel.h"
#include "rels/rel.h"
#include "sets/closeflags.h"
#include "sets/rset.h"

#include <ctype.h>
#include <cuda_runtime.h>
#include <immintrin.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>

#define RED "\x1b[91m"
#define GREEN "\x1b[92m"
#define YELLOW "\x1b[93m"
#define BLUE "\x1b[94m"
#define MAGENTA "\x1b[95m"
#define CYAN "\x1b[96m"
#define RESET "\x1b[0m"
#define BLUE_NUM BLUE "%d" RESET
#define BLUE_STR BLUE "%s" RESET
#define GREEN_STR GREEN "%s" RESET
#define RED_STR RED "%s" RESET

static const char *fst_name, *snd_name;
static rel_t fst_rels[REL_N], snd_rels[REL_N];
static rset_t fst_set, snd_set;
static int fst_n = 0, snd_n = 0;
static FILE *out;

// Estrae i dati dai file
static void getData(const char *f1, const char *f2)
{
    FILE *iaf, *baf;
    char full_f1[FILENAME_MAX] = {FILE_DIR_OUT "/"}, full_f2[FILENAME_MAX] = {FILE_DIR_OUT "/"};

    fprintf(out, "Estraggo i dati... ");
    printf("Estraggo i dati... ");
    strcat(strcat(full_f1, f1), ".txt");
    strcat(strcat(full_f2, f2), ".txt");

    if (!(iaf = fopen(full_f1, "rt")))
    {
        perror(full_f1);
        exit(EXIT_FAILURE);
    }
    fst_set = rsetInit();
    fst_name = f1;

    fst_set = rsetInitByFile(iaf);
    fst_n = rsetToArr(fst_set, fst_rels);
    fclose(iaf);
    fprintf(out, "raccolte %d relazioni in %s, ", fst_n, f1);
    printf("raccolte " BLUE_NUM " relazioni in %s, ", fst_n, f1);

    if (!(baf = fopen(full_f2, "rt")))
    {
        perror(full_f2);
        exit(EXIT_FAILURE);
    }

    snd_set = rsetInit();
    snd_name = f2;

    snd_set = rsetInitByFile(baf);
    snd_n = rsetToArr(snd_set, snd_rels);
    fclose(baf);
    fprintf(out, "%d in %s \n", snd_n, f2);
    printf(BLUE_NUM " in %s \n", snd_n, f2);
}

// dice quante relazioni del primo insieme sono anche nel secondo, e stampa fst \sec snd
static int checkCommon(void)
{
    int count = 0;

    fprintf(out, "Relazioni comuni fra i due insiemi: ");
    printf("Relazioni comuni fra i due insiemi: ");

    for (int i = 0; i < fst_n; ++i)
        if (rsetGet(snd_set, fst_rels[i]))
        {
            relPrint(out, fst_rels[i]);
            ++count;
        }

    fprintf(out, "Totale relazioni: %d\n", count);
    printf(BLUE_NUM "\n", count);

    return count;
}

// dice quante relazioni del primo insieme sono anche nel secondo, e stampa snd - fst
static int checkDiff(void)
{
    int count = 0;

    fprintf(out, "Relazioni in %s - %s: ", snd_name, fst_name);
    printf("Relazioni in %s - %s: ", snd_name, fst_name);

    for (int i = 0; i < snd_n; ++i)
        if (!rsetGet(fst_set, snd_rels[i]))
        {
            relPrint(out, snd_rels[i]);
            ++count;
        }

    fprintf(out, "Totale relazioni: %d\n", count);
    printf(BLUE_NUM "\n", count);

    return count;
}

// dice quante relazioni lineari sono nei due insiemi
static int checkLinear(void)
{
    int count = 0;

    fprintf(out, "Relazioni lineari nei due insiemi: ");
    printf("Relazioni lineari nei due insiemi: ");

    for (int i = 0; i < fst_n; ++i)
        if (_tzcnt_u32(fst_rels[i]) >= BABR_N)
            ++count;
    fprintf(out, "%d/8192; ", count);
    printf(BLUE_NUM "/8192; ", count);

    count = 0;
    for (int i = 0; i < snd_n; ++i)
        if (_tzcnt_u32(snd_rels[i]) >= BABR_N)
            ++count;
    fprintf(out, "%d/8192\n", count);
    printf(BLUE_NUM "/8192\n", count);

    return count;
}

// dice quante relazioni lineari sono nei due insiemi
static void checkBasic(void)
{
    char c = '\0';

    fprintf(out, "Relazioni base in %s: {", fst_name);
    printf("Relazioni base in " BLUE_STR ": {", fst_name);
    for (brel_t br = BR0; br < BREL_N; ++br)
        if (rsetGet(fst_set, brelToRel(br)))
        {
            printf("%c %s", c, brelToStr(br));
            c = ',';
        }

    c = '\0';
    fprintf(out, " }\nRelazioni base in %s: {", snd_name);
    printf(" }\nRelazioni base in " BLUE_STR ": {", snd_name);
    for (brel_t br = BR0; br < BREL_N; ++br)
        if (rsetGet(snd_set, brelToRel(br)))
        {
            printf("%c %s", c, brelToStr(br));
            c = ',';
        }

    fputs(" }\n", out);
    puts(" }");
}

// dice se c'è la relazione tutto
static void checkAllRel(void)
{
    fprintf(out, "Relazione tutto in %s: ", fst_name);
    printf("Relazione tutto in " BLUE_STR ": ", fst_name);
    if (rsetGet(fst_set, REL_ALL))
    {
        fputs("si\n", out);
        puts(GREEN "si" RESET);
    }
    else
    {
        fputs("no\n", out);
        puts(RED "no" RESET);
    }

    fprintf(out, "Relazione tutto in %s: ", snd_name);
    printf("Relazione tutto in " BLUE_STR ": ", snd_name);
    if (rsetGet(snd_set, REL_ALL))
    {
        fputs("si\n", out);
        puts(GREEN "si" RESET);
    }
    else
    {
        fputs("no\n", out);
        puts(RED "no" RESET);
    }
}

// Controlla la chiusura
static int checkClose(closeflag_t flag)
{
    rset_t set1 = rsetInitCopy(fst_set);
    rset_t set2 = rsetInitCopy(snd_set);
    rel_t check = REL0;

    fprintf(out, "Chiusura di %s per %s: ", fst_name, closeflagToStr(flag));
    printf("Chiusura di " BLUE_STR " per %s: ", fst_name, closeflagToStr(flag));
    rsetClose(&set1, flag);
    for (rel_t r = relInit(); !check && r < REL_ALL; r += REL_STEP)
        if (rsetGet(set1, r) > rsetGet(fst_set, r))
            check = r;

    if (check)
    {
        fprintf(out, "Aperto! Manca %u\n", check);
        printf(RED "Aperto!" RESET " Manca %u\n", check);
    }
    else
    {
        fputs("Chiuso!\n", out);
        puts(GREEN "Chiuso!" RESET);
    }

    check = REL0;
    fprintf(out, "Chiusura di %s per %s: ", snd_name, closeflagToStr(flag));
    printf("Chiusura di " BLUE_STR " per %s: ", snd_name, closeflagToStr(flag));
    rsetClose(&set2, flag);
    for (rel_t r = relInit(); !check && r < REL_ALL; r += REL_STEP)
        if (rsetGet(set2, r) > rsetGet(snd_set, r))
            check = r;

    if (check)
    {
        fprintf(out, "Aperto! Manca %u\n", check);
        printf(RED "Aperto!" RESET " Manca %u\n", check);
    }
    else
    {
        fputs("Chiuso!\n", out);
        puts(GREEN "Chiuso" RESET);
    }

    rsetDel(set1);
    rsetDel(set2);

    return relIsEmpty(check);
}

// Controlla la chiusura per composizione forte del secondo insieme (può essere molto lento)
static int checkScomp(void)
{
    rel_t path = REL0;

    fprintf(out, "Chiusura di %s per %s: ", snd_name, closeflagToStr(CLOSE_SCOMP));
    printf("Chiusura di " BLUE_STR " per %s: ", snd_name, closeflagToStr(CLOSE_SCOMP));

#pragma omp parallel for
    for (int i = 0; i < snd_n; ++i)
        for (int j = 0; j < snd_n && !path; ++j)
            for (int k = 0; k < snd_n && !path; ++k)
            {
                rel_t p = relPath(snd_rels[i], snd_rels[j], snd_rels[k]);

                if (!rsetGet(snd_set, p))
                    path = p;
            }

    if (path)
    {
        fprintf(out, "Aperto!, manca: %u\n", path);
        printf(RED "Aperto!" RESET ", manca: %u\n", path);
    }
    else
    {
        fputs("Chiuso!\n", out);
        puts(GREEN "Chiuso" RESET);
    }

    return relIsEmpty(path);
}

// Calcola la chisura algebrica dell'insieme
static int computeClosure(closeflags_t flags)
{
    rset_t cpy = rsetInitCopy(snd_set);
    int tot = rsetClose(&cpy, flags);

    rsetPrint(out, cpy);
    fprintf(out, "Totale relazioni: %d\n", tot);
    printf("Totale relazioni: %d\n", tot);

    rsetDel(cpy);

    return tot;
}

// Calcola il kernel dell'insieme
static int computeKernel(void)
{
    rset_t cpy = rsetInitCopy(snd_set);
    int tot = ralgKernel(&cpy);

    rsetPrint(out, cpy);
    fprintf(out, "Totale relazioni: %d\n", tot);
    printf("Totale relazioni: %d\n", tot);

    rsetDel(cpy);

    return tot;
}

static void cleanup(void)
{
    rsetDel(fst_set);
    rsetDel(snd_set);
    fclose(out);
}

static int getwschar(void)
{
    int ch;

    do
        ch = getchar();
    while (isspace(ch));

    return ch;
}

int main(int argc, char **argv)
{
    int rank = 0;

    if (argc < 3)
    {
        fprintf(stderr, "Sintassi corretta:\nchecker [insieme_partenza], [insieme_interesse]\n");
        exit(EXIT_FAILURE);
    }
    if (!(out = fopen("out.txt", "wt")))
    {
        perror("out.txt");
        exit(EXIT_FAILURE);
    }

    lutInit(rank);
    getData(argv[1], argv[2]);

    checkCommon();
    checkDiff();
    checkLinear();
    checkBasic();
    checkAllRel();

    checkClose(CLOSE_CONV);
    checkClose(CLOSE_SEC);
    checkClose(CLOSE_WCOMP);
    checkClose(CLOSE_SCOMP);
    //    checkClose(CLOSE_UNION);

    printf("Vuoi calcolare il nucleo di " BLUE_STR "? (y/n) ", snd_name);
    if (getwschar() == 'y')
        computeKernel();

    printf("Vuoi calcolare la chiusura 'impropria' di " BLUE_STR "? (y/n) ", snd_name);
    if (getwschar() == 'y')
        computeClosure(CLOSE_PRINT | CLOSE_FULL);

    printf("Vuoi calcolare la chiusura 'propria' di " BLUE_STR "? (y/n) ", snd_name);
    if (getwschar() == 'y')
        computeClosure(CLOSE_PRINT | CLOSE_CONV | CLOSE_SCOMP);

    cleanup();
    lutFinish();

    return 0;
}
