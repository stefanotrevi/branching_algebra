#include "lut.h"
#include "sets/rset.h"

#include <stdlib.h>

static int contains(rset_t *sets, size_t sets_n, rset_t set)
{
    for (size_t i = 0; i < sets_n; ++i)
        if (rsetEquals(sets[i], set))
            return 1;

    return 0;
}

static void generate(void)
{
    FILE *out = fopen("out.txt", "wt");
    rset_t *algs = malloc(sizeof *algs);
    size_t n = 0, max_n = 1;

    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
    {
        rset_t alg = rsetInit();

        rsetIns(&alg, r);
        rsetClose(&alg, CLOSE_SCOMP | CLOSE_CONV);
        if (!contains(algs, n, alg))
        {
            if (n >= max_n)
            {
                max_n *= 2;
                algs = realloc(algs, max_n * sizeof *algs);
            }
            algs[n++] = alg;
            rsetPrint(out, alg);
            printf("%d\n", alg.num);
        }
        else
            rsetDel(alg);
        printf("%u/%u\r", r, REL_N);
        fflush(stdout);
    }

    fprintf(out, "Algebre generate: %llu\n", n);
    printf("Algebre generate: %llu\n", n);

    for (size_t i = 0; i < n; ++i)
        rsetDel(algs[i]);
    free(algs);
    fclose(out);
}

int main(void)
{
    int rank = 0;

    lutInit(rank);
    generate();
    lutFinish();

    return 0;
}
