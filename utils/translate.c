#include "rels/rel.h"

int main(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i)
    {
        char buf[64] = {0};
        rel_t r = relFromStr(argv[i]);

        relToStr(buf, r);
        printf("%u: %s\n", r, buf);
    }

    return 0;
}
