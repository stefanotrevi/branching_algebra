#include "../include/job.h"
#include "../include/plan.h"

#include <stdlib.h>

job_t jobInit(int n_task, int id, int tsk_0)
{
    job_t job = {.tsk = malloc(n_task * sizeof(task_t)), .size = n_task, .id = id};

    for (int i = 0; i < job.size; ++i)
        job.tsk[i] = tsk_0 + i;

    return job;
}

void jobDel(job_t job)
{
    free(job.tsk);
}

task_t jobGet(job_t job, int i)
{
    return job.tsk[i];
}
