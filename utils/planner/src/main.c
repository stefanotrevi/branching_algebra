#include "../../ba/include/nets/bpnet.h"
#include "../../ba/include/nets/pnet.h"
#include "../../ba/include/rels/bprel.h"

#include <stdlib.h>

#define EVENT_MAX 10
#define EVENT_LEN 128

/*
static int storyIsOk(FILE *story)
{
    char events[EVENT_MAX][EVENT_LEN];
    int n_ev, n_rel;
    net_t net;
    bnet_t model;

    // Scansiono gli eventi
    fscanf(story, "Eventi: %d\n", &n_ev);
    net = netInit(n_ev);
    for (int i = 0; i < n_ev; ++i)
        fgets(events[i], EVENT_LEN, story);

    getc(story);
    fscanf(story, "Cause-effetti: %d\n", &n_rel);
    for (int i = 0; i < n_rel; ++i)
    {
        char rel;
        int e0, e1;

        fscanf(story, "%d %c %d", &e0, &rel, &e1);
        if (rel == '/')
            netSet(net, e0, e1, brelToRel(B));
        else
            netSet(net, e0, e1, brelToRel(UN));
    }

    if (!netPC(net))
    {
        puts("La rete dei rapporti causa-effetto non è consistente!");
        netDel(net);
        return 0;
    }
    model = netModel(net);
    bnetPrint(stdout, model);

    netDel(net);
    bnetDel(model);

    return 1;
}
*/
int main(void)
{
    pnet_t net = pnetInit(4);
    int n_cons = 0, n_tot = 0;

    pnetPut(net, 0, 1, BPRADD3(PEQ, PB, PBI));
    pnetPut(net, 0, 2, BPRADD3(PEQ, PB, PBI));
    pnetPut(net, 0, 3, BPRADD3(PEQ, PB, PBI));
    pnetPut(net, 1, 2, BPRADD2(PUN, PBI));
    pnetPut(net, 1, 3, BPRADD2(PUN, PB));
    pnetPut(net, 2, 3, BPRADD2(PUN, PBI));

    puts("Staring network:");
    pnetPrint(stdout, net, 0);
    pnetPC(net);
    puts("\nCerco modelli consistenti...");

    for (int i0 = 0; i0 < 3; ++i0)                     // 0-1
        for (int i1 = 0; i1 < 3; ++i1)                 // 0-2
            for (int i2 = 0; i2 < 3; ++i2)             // 0-3
                for (int i3 = 0; i3 < 2; ++i3)         // 1-2
                    for (int i4 = 0; i4 < 2; ++i4)     // 1-3
                        for (int i5 = 0; i5 < 2; ++i5) // 2-3
                        {
                            bpnet_t model = pnetModel(net, (const int[]){i0, i1, i2, i3, i4, i5});

                            if (bpnetPC(model))
                            {
                                printf("\n{%d, %d, %d, %d, %d, %d}:\n", i0, i1, i2, i3, i4, i5);
                                bpnetPrint(stdout, model);
                                ++n_cons;
                            }
                            ++n_tot;
                            bpnetDel(model);
                        }

    printf("\nTotale modelli consistenti: %d/%d\n", n_cons, n_tot);
    pnetDel(net);
    /*    FILE *story = fopen("story.txt", "rt");

    puts(storyIsOk(story) ? "La storia può avvenire" : "La storia non può avvenire");

    fclose(story);
*/
    return 0;
}
