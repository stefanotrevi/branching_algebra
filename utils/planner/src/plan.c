#include "../include/plan.h"
#include "../../ba/include/net.h"
#include "../../ba/include/rel.h"
#include "../include/job.h"

#include <math.h>
#include <stdlib.h>

plan_t planInit(int n_jobs, int max_tasks)
{
    return (plan_t){.jobs = malloc(n_jobs * sizeof(job_t)),
                    .size = n_jobs,
                    .net = netInit(max_tasks)};
}

plan_t planInitRand(int jobs, int tsk_per_job, int dens, int only_horn)
{
    int tsks = jobs * tsk_per_job;
    plan_t plan = {.jobs = malloc(jobs * sizeof(job_t)),
                   .size = jobs,
                   .net = netInitRand(tsks, dens, only_horn)};

    for (int i = 0; i < jobs; ++i)
        planAdd(&plan, tsk_per_job);

    return plan;
}

void planDel(plan_t plan)
{
    for (int i = 0; i < plan.size; ++i)
        jobDel(plan.jobs[i]);
    free(plan.jobs);

    netDel(plan.net);
}

plan_t *planAdd(plan_t *plan, int job_sz)
{
    int old = plan->tsk_n;

    // Imposto gli indici del job e dei suoi task
    plan->jobs[plan->job_n] = jobInit(job_sz, plan->job_n, plan->tsk_n);
    plan->tsk_n += job_sz;
    ++plan->job_n;

    // I task di un job sono tutti sulla stessa linea temporale
    for (int i = old; i < plan->tsk_n; ++i)
        for (int j = i + 1; j < plan->tsk_n; ++j)
        {
            netSet(plan->net, i, j, ALLIA);
            netSet(plan->net, j, i, ALLIA);
        }

    return plan;
}

plan_t planRel(plan_t plan, int job_i, int tsk_i, int job_j, int tsk_j, rel_t rel)
{
    netSet(plan.net, jobGet(plan.jobs[job_i], tsk_i), jobGet(plan.jobs[job_j], tsk_j), rel);

    return plan;
}

int planSolve(plan_t plan)
{
    // uso solo la porzione della rete veramente occupata
    plan.net.size = plan.tsk_n;

    return netPC(plan.net);
}

double planDens(plan_t plan)
{
    plan.net.size = plan.tsk_n;

    return netDens(plan.net);
}
