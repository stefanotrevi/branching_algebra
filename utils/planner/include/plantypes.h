#pragma once

#include "jobtypes.h"
#include "../../ba/include/nettypes.h"

typedef struct plan
{
    job_t *jobs;
    int size;
    int job_n;
    int tsk_n;
    net_t net;
} plan_t;
