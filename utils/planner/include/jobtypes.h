#pragma once

#include "tasktypes.h"

typedef struct job
{
    task_t *tsk;
    int size;
    int id;
} job_t;
