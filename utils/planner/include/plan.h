#pragma once

#include "plantypes.h"

// Inizializza un piano (cioè un insieme di lavori)
plan_t planInit(int n_jobs, int max_tasks);

// Inizializza un piano casuale
plan_t planInitRand(int jobs, int tsk_per_job, int dens, int only_horn);

// Elimina un piano
void planDel(plan_t plan);

// Inserisce un "lavoro" all'interno del piano (cioè un programma di lavoro locale)
plan_t *planAdd(plan_t *plan, int job_sz);

// Mette in relazione l'i-esimo task dell'i-esimo job con il j-esimo task del j-esimo job
plan_t planRel(plan_t plan, int job_i, int tsk_i, int job_j, int tsk_j, rel_t rel);

// Minimizza un piano, e ritorna 0 se è contraddittorio
int planSolve(plan_t plan);

// Ritorna la "densità" del piano
double planDens(plan_t plan);
