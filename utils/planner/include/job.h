#pragma once

#include "jobtypes.h"

// Inizializza un nuovo lavoro
job_t jobInit(int n_task, int id, int tsk_0);

// Elimina un lavoro
void jobDel(job_t job);

// Imposta gli indici
job_t jobSetIdx(job_t job, int id, int tsk_id0);

// Restituisce l'i-esimo task
task_t jobGet(job_t job, int i);
