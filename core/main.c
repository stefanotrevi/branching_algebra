#include "algs/palg.h"
#include "algs/ralg.h"
#include "chronolib.h"
#include "files.h"
#include "generator.h"
#include "lut.h"
#include "nets/net.h"
#include "rels/bprel.h"
#include "rels/brel.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"
#include "sets/pset.h"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef MPI_ENABLE
    #include <mpi.h>
#endif

#include <math.h>

static int rank = 0, size = 1;   // rank e size CPU
static int grank = 0, gsize = 1; // rank e size GPU

// Inizializza tutto il sistema (lookup tables, MPI, CUDA...)
static void initialize(int argc, char **argv)
{
#ifdef MPI_ENABLE
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
#else
    (void)argc;
    (void)argv;
    (void)size;
#endif
    grank = lutInit(rank);
    cudaGetDeviceCount(&gsize);

    fprintf(stderr, "CPU: %d/%d; GPU: %d/%d\n", rank, size, grank, gsize);
}

// Cleanup delle risorse
static void finalize(void)
{
    lutFinish();

#ifdef MPI_ENABLE
    MPI_Finalize();
#endif
}

static void benchmark(void)
{
    int (*foo[])(net_t) = {netPC, netPC2, netPC3};
    int n = 20;
    net_t t = netInit(n);

    for (int i = 0; i < 20; ++i)
    {
        net_t net = netInitRand(n, 80, 70, NULL, 0);

        for (int j = 0; j < 3; ++j)
        {
            netCopy(t, net);
            printf("%d ", foo[j](t));
        }
        netDel(net);
        putchar('\n');
    }

    netDel(t);
}

static void testCounter(void)
{
    net_t net = netInit(5);

    netPut(net, 0, 1, brelAtoRel(array(brel_t, D, DI, O, OI)));
    netPut(net, 0, 2, brelAtoRel(array(brel_t, IM, IMI, D, O)));
    netPut(net, 0, 3, brelAtoRel(array(brel_t, IM, IMI, DI, OI)));
    netPut(net, 0, 4, brelAtoRel(array(brel_t, IMI, D)));
    netPut(net, 1, 2, brelAtoRel(array(brel_t, EQ, S, SI)));
    netPut(net, 1, 3, brelAtoRel(array(brel_t, EQ, S, SI)));
    netPut(net, 1, 4, brelAtoRel(array(brel_t, IMI, OI)));
    netPut(net, 2, 3, brelAtoRel(array(brel_t, IE, EQ, S)));
    netPut(net, 2, 4, brelAtoRel(array(brel_t, D, OI)));
    netPut(net, 3, 4, brelAtoRel(array(brel_t, D, OI)));

    net_t net2 = netInitCopy(net);
    int pc = netPC(net);
    int fc = netFC(net2, PART_BASIC);

    netPrint(stdout, net, 0);
    printf("%d, %d\n", pc, fc);

    netDel(net);
    netDel(net2);
}

static void findCounter(void)
{
    /*    rel_t allow[] = {
        brelAtoRel(array(brel_t, D, DI, O, OI)),    brelAtoRel(array(brel_t, IM, IMI, D, O)),
        brelAtoRel(array(brel_t, IM, IMI, DI, OI)), brelAtoRel(array(brel_t, IMI, D)),
        brelAtoRel(array(brel_t, EQ, S, SI)),       brelAtoRel(array(brel_t, IMI, OI)),
        brelAtoRel(array(brel_t, IE, EQ, S)),       brelAtoRel(array(brel_t, D, OI)),
    };*/
    rel_t *allow = malloc(POINT_N * sizeof *allow);
    int idx[10] = {0};
    uint64_t i = 0;
    net_t net = netInit(4), net2 = netInit(4);

    for (int i = 0; i < POINT_N; ++i)
        allow[i] = lutPointGet(i);

    rset_t allow_set = rsetInitByArr(allow, POINT_N);
    int n = 19;
    ralgKernel(&allow_set);

    rsetToArr(allow_set, allow);
    rsetClose(&allow_set, CLOSE_FULL);
    rsetPrint(stdout, allow_set);
    printf("%d, %d\n", allow_set.num, n);
    
    for (idx[0] = 0; idx[0] < n; ++idx[0])
        for (idx[1] = 0; idx[1] < n; ++idx[1])
            for (idx[2] = 0; idx[2] < n; ++idx[2])
                for (idx[3] = 0; idx[3] < n; ++idx[3])
                    for (idx[4] = 0; idx[4] < n; ++idx[4])
                        for (idx[5] = 0; idx[5] < n; ++idx[5])
                        {
                            netPut(net, 0, 1, allow[idx[0]]);
                            netPut(net, 0, 2, allow[idx[1]]);
                            netPut(net, 0, 3, allow[idx[2]]);
                            netPut(net, 1, 2, allow[idx[3]]);
                            netPut(net, 1, 3, allow[idx[4]]);
                            netPut(net, 2, 3, allow[idx[5]]);

                            //        netCopy(net2, net);
                            if (netPC(net) != netFC(net, PART_BASIC))
                            {
                                netPrint(stdout, net, 0);
                                netPrint(stdout, net, 0);
                                goto finish;
                            }
                            if (!(i++ & ((1 << 15) - 1)))
                            {
                                printf("%lu\r", i);
                                fflush(stdout);
                            }
                            netReset(net);
                        }

finish:
    puts("\nBye");
    netDel(net);
    netDel(net2);
}

int main(int argc, char **argv)
{
    initialize(argc, argv);

    findCounter();
    finalize();

    return 0;
}
