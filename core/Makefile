# Location for storing lookup tables and outputs (string)
#STORE_PREFIX := "/home/strevisa/ba/core"
#STORE_PREFIX := "C:/Users/stefa/Documents/universita/Terzo_Anno/tirocinio/ba/core"
STORE_PREFIX := "/home/stefano/Documenti/universita/Terzo_Anno/tirocinio/ba/core"
# Binary Name
BINNAME := ordhorn
# Enables Branching or Linear mode (boolean)
BRANCH := 1
# Enables debug mode (boolean)
DEBUG := 0
# Enables MPI compilation (boolean)
MPI_ENABLE := 0
# Enables Intel Compiler
INTEL_COMPILER := 1
# Enables Intel IPO (boolean)
INTEL_IPO := 0

ifeq ($(INTEL_IPO), 1)
    INTEL_COMPILER := 1
endif

ifeq ($(INTEL_COMPILER), 1)
    CC := $(IC)
    CFLAGS := $(ICFLAGS)
endif

ifeq ($(CC), )
    CC := cc
    CFLAGS := -O3 -march=native -std=c18
endif

ifeq ($(MPI_ENABLE), 1)
    CC := mpicc
    CFLAGS += -DMPI_ENABLE
endif

ifeq ($(NVCC), )
    NVCC := nvcc
    NVCCFLAGS := -O3 -arch=compute_37 -code=compute_37 -std=c++17
endif

ifeq ($(DEBUG), 1)
    ifeq ($(OS), Windows_NT)
        CFLAGS := -debug:inline-debug-info -nologo -Qstd=c18
    else
        CFLAGS := -debug inline-debug-info -nologo -std=c18
    endif
    NVCCFLAGS += -g
endif

ifeq ($(BRANCH), 1)
    BRANCH := BA
else
    BRANCH := IA
endif

SRCPATH := src
SRC := $(wildcard $(SRCPATH)/*.c $(SRCPATH)/**/*.c) main.c
NVSRC := $(wildcard $(SRCPATH)/*.cu $(SRCPATH)/**/*.cu)

INCPATH := include
INC := $(wildcard $(INCPATH)/*.h $(INCPATH)/**/*.h)
NVINC := $(wildcard $(INCPATH)/*.cuh $(INCPATH)/**/*.cuh)

BUILDDIR := build
BINDIR := bin
RESDIR := res
OUTDIR := out

ifeq ($(OS), Windows_NT)
    ifeq ($(INTEL_IPO), 1)
        CFLAGS += -Qipo
    endif
    CFLAGS += -Qopenmp -QMMD -Fo:$(BUILDDIR)/
    NVCCFLAGS += -MD
    OEXT := obj
    LDFLAGS := -link cudart.lib
    PDB := $(SRC:.c=.pdb)
    MKDIR = if not exist $(1) mkdir $(1)
    RM = if exist $(1) del /F /Q $(1)
else
    ifeq ($(INTEL_IPO), 1)
        CFLAGS += -ipo
    endif
    CFLAGS += -fopenmp
    LDFLAGS := -lcudart -pthread -fopenmp
    ifneq ($(CUDALIB), )
        LDFLAGS += -L$(CUDALIB)
    endif
    OEXT := o
    PDB :=
    MKDIR = mkdir -p $(1)
    RM = rm -rf $(1)/*
endif

CFLAGS += -D$(BRANCH) -DSTORE_PREFIX=$(STORE_PREFIX) -I$(INCPATH)
NVCCFLAGS += -D$(BRANCH) -DSTORE_PREFIX=$(STORE_PREFIX) -I$(INCPATH)

SRCNAMES := $(notdir $(SRC))
OBJ := $(SRCNAMES:%.c=$(BUILDDIR)/%.$(OEXT))

NVSRCNAMES := $(notdir $(NVSRC))
NVOBJ := $(NVSRCNAMES:%.cu=$(BUILDDIR)/%.$(OEXT))

DEP := $(OBJ:%.$(OEXT)=%.d) $(NVOBJ:%.$(OEXT)=%.d)

all: build_dirs $(BINNAME)

clean:
	$(call RM,$(BUILDDIR))

build_dirs:
	$(call MKDIR,$(BUILDDIR))
	$(call MKDIR,$(BINDIR))

ifeq ($(INTEL_IPO), 1)
$(BINNAME): $(SRC) $(NVOBJ) $(INC) $(NVINC)
	$(CC) $(CFLAGS) $(SRC) $(NVOBJ) -o $(BINDIR)/$@ $(LDFLAGS)
else
$(BINNAME): $(OBJ) $(NVOBJ)
	$(CC) $(CFLAGS) $^ -o $(BINDIR)/$@ $(LDFLAGS)
endif
    
-include $(DEP)

$(BUILDDIR)/%.$(OEXT): %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.$(OEXT): $(SRCPATH)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.$(OEXT): $(SRCPATH)/**/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.$(OEXT): $(SRCPATH)/%.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@

$(BUILDDIR)/%.$(OEXT): $(SRCPATH)/**/%.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@

