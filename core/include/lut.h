#pragma once

#include "rels/preltypes.h"
#include "rels/reltypes.h"

#ifdef BA
    #define HORN_N 4510
    #define POINT_N 961
#else
    #define HORN_N 868
    #define POINT_N 183
#endif

// La mappa deve essere partizionata in modo omogeneo per minimizzare lo spazio occupato
#define COMPLUT_R 10U                  // numero di bit della parte destra
#define COMPLUT_L (BREL_N - COMPLUT_R) // numero di bit della parte sinistra
#define COMPLUT_NR (1U << COMPLUT_R)   // numero di elementi della parte destra
#define COMPLUT_NL (1U << COMPLUT_L)   // numero di elementi della parte sinistra

typedef enum lutidx
{
    COMPLUT_RR,
    COMPLUT_RL = COMPLUT_NR << COMPLUT_R,
    COMPLUT_LR = COMPLUT_RL + (COMPLUT_NR << COMPLUT_L),
    COMPLUT_LL = COMPLUT_LR + (COMPLUT_NR << COMPLUT_L),
#ifdef BA
    COMPLUT_N = COMPLUT_LL + (COMPLUT_NL << COMPLUT_L),
#else
    COMPLUT_N = REL_NIA * REL_NIA,
#endif
} lutidx_t;

#define COMPLUT_MASK (COMPLUT_NR - 1U)

#define COMPLUT_GETRIGHT(r) ((r)&COMPLUT_MASK)                  // estrae i bit a destra
#define COMPLUT_GETLEFT(r) (((r) & ~COMPLUT_MASK) >> COMPLUT_R) // estrae i bit a sinistra

// Inizializza le lut, ritorna 0 in caso di successo
int lutInit(int rank);

// Rimuove le lut dalla memoria
void lutFinish(void);

// Ritorna l'iesima relazione Horn, se i > HORN_N, ritorna la relazione TUTTO
rel_t lutHornGet(unsigned i);

// Ritorna se l'elemento è presente nella mappa delle relazioni Horn
int lutHornMapGet(rel_t r);

// Ritorna l'iesima relazione puntizzabile, se i > HORN_N, ritorna la relazione TUTTO
rel_t lutPointGet(unsigned i);

// Ritorna se l'elemento è presente nella mappa delle relazioni puntizzabili
int lutPointMapGet(rel_t r);

// Ritorna un elemento della tabella di composizione fra relazioni
#ifdef BA
rel_t compGet(rel_t r1, rel_t r2, lutidx_t lidx, uint ld);
#else
rel_t compGet(rel_t r1, rel_t r2);
#endif

// Ritorna un elemento della tabella di composizione fra relazioni puntuali
prel_t pCompGet(prel_t r1, prel_t r2);

// Ritorna la lista di partizione di r
const rel_t *hornPartGet(rel_t r);

// Costruisce la LUT delle relazioni TORD-Horn
int lutBuildHorn(void);

// Costruisce la LUT delle relazioni puntizzabili
int lutBuildPoint(void);

// Costruisce la LUT di composizione delle relazioni puntuali
int lutBuildPcomp(void);

// Costruisce la LUT di composizione delle relazioni intervallari
int lutBuildComp(void);

// Costruisce la LUT di partizione di HornBA
int lutBuildHornPartition(void);
