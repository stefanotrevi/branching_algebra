#pragma once

#include <inttypes.h>

typedef struct rset
{
    uint8_t *data;
    int num;
} rset_t;
