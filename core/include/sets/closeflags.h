#pragma once

typedef enum closeflag // Flag di chisura
{
    CLOSE_CONV = 1 << 0,
    CLOSE_SEC = 1 << 1,
    CLOSE_UNION = 1 << 2,
    CLOSE_WCOMP = 1 << 3,
    CLOSE_FULL = CLOSE_CONV | CLOSE_SEC | CLOSE_WCOMP,
    CLOSE_SCOMP = 1 << 4,
    CLOSE_PRINT = 1 << 5, // stampa info di progresso
} closeflag_t;

typedef int closeflags_t;

// Ritorna una stringa con il nome della flag
const char *closeflagToStr(closeflag_t flag);
