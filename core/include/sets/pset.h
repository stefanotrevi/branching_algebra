#pragma once

#include "psettypes.h"
#include "rels/preltypes.h"

#include <stdio.h>

// Inizializza un insieme di dimensione PREL_N
pset_t psetInit(void);

// Elimina un insieme di relazioni puntuali
void psetDel(pset_t pset);

// Inizializza un insieme di relazioni puntuali partendo da un array
pset_t psetInitByArr(prel_t *array, int n);

// Ritorna un elemento dell'insieme
uint8_t psetGet(pset_t pset, prel_t pr);

// Inserisce un elemento nell'insieme, impostando un valore a piacere
void psetPut(pset_t *pset, prel_t pr, uint8_t val);

// Inserisce un elemento nell'insieme
void psetIns(pset_t *pset, prel_t pr);

// Rimuove un elemento dall'insieme
void psetRem(pset_t *pset, prel_t pr);

// Stampa l'insieme
void psetPrint(FILE *f, pset_t pset);

// Converte un insieme di relazioni puntuali in un array, e ne ritorna la lunghezza
int psetToArray(pset_t pset, prel_t *array);
