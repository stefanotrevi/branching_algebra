#pragma once

#include <inttypes.h>

typedef struct pset
{
    uint8_t *data;
    int num;
} pset_t;
