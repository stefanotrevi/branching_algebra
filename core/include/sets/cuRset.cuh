#pragma once

#include "closeflags.h"
#include "rels/reltypes.h"
#include "sets/rsettypes.h"

#include <driver_types.h>

#ifdef __cplusplus
extern "C" {
#endif
// Inizializza l'ambiente CUDA, e ritorna la GPU assegnata
int cuLutInit(int rank, const rel_t *comp_set);

// Rimuove dalla GPU le LUT
void cuLutFinish(void);

// Ritorna lo stream i
cudaStream_t cuGetStream(int i);

// Calcola un passo della chiusura
void cuRsetClose(uint8_t *set, rel_t *fst, uint n_fst, rel_t *snd, uint n_snd, cudaStream_t stream,
                 closeflags_t flags);

// Calcola un passo della partizione delle relazioni
void cuPartition(rel_t *pset, uint *wset, rel_t *fst, uint fst_n, rel_t *snd, uint snd_n);
#ifdef __cplusplus
}
#endif
