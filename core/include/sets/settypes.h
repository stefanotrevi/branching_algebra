#pragma once

#include "defaults.h"

#include <inttypes.h>

typedef struct set // Array associativo per trovare ed inserire elementi in tempo O(1)
{
    uint8_t *data;
    uint dim;
    int nset;
} set_t;
