#pragma once

#ifndef _POSIX_C_SOURCE
    #define _POSIX_C_SOURCE 199309L
    #define __USE_POSIX199309
#endif

#include <time.h>

typedef struct timespec chrono_t;

#ifndef _WIN32
// Imposta l'orologio da utilizzare come riferimento
void chronoSet(clockid_t clk_id);
#endif

// Ritorna l'istante in cui viene chiamato
chrono_t chronoNow(void);

// Converte in un intero a 64 bit (non è detto che ci stia tutto!)
unsigned long long chronoToULL(chrono_t t);

// Converte in un double, in secondi, un timespec
double chronoToD(chrono_t t);

// Ritorna la differenza fra due timespec
chrono_t chronoSub(chrono_t end, chrono_t start);

//Ritorna la somma fra due timespec
chrono_t chronoSum(chrono_t a, chrono_t b);

//Ritorna il tempo passato dal timespec specificato
chrono_t chronoElap(chrono_t start);

#define CHRONO_GLUE0(x, y) x##y
#define CHRONO_GLUE(x, y) CHRONO_GLUE0(x, y)
#define CHRONO_UNIQUE(name) CHRONO_GLUE(CHRONO_GLUE(name, __), __LINE__)

// Evito warning di aliasing nomi e valuto una volta sola il secondo parametro
#define CHRONO_MEASURE_UNIQUE(function, unsafe_runs, elap, i, runs) \
    do                                                              \
    {                                                               \
        chrono_t elap = chronoNow();                                \
        int runs = (int)unsafe_runs;                                \
                                                                    \
        for (int i = 0; i < (runs); ++i)                            \
            function;                                               \
                                                                    \
        elap = chronoElap(elap);                                    \
        for (char *i = #function; *i != '(' && *i; ++i)             \
            putchar(*i);                                            \
                                                                    \
        printf(" (x%d): %lf sec.\n", runs, chronoToD(elap));        \
    } while (0)

// Esegue una funzione runs_n volte e stampa il tempo totale e la media di esecuzione
#define chronoMeasure(function, runs_n) \
    CHRONO_MEASURE_UNIQUE(function, runs_n, CHRONO_UNIQUE(elap), CHRONO_UNIQUE(i), CHRONO_UNIQUE(runs))
