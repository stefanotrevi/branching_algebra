#pragma once

#include "dnftypes.h"
#include "rels/reltypes.h"
#include <stddef.h>

// inizializza una dnf
dnf_t dnfInit(void);

// converte una relazione in una dnf
dnf_t relToDnf(rel_t r);

// inserisce una relazione in una dnf
dnf_t *dnfInsert(dnf_t *dnf, rel_t r);

// inserisce una relazione base in una dnf
dnf_t *dnfInsertbr(dnf_t *dnf, brel_t br);

// Converte in una equazione ORD la formula DNF
size_t DNFToStr(char *s, dnf_t dnf, int iter);
