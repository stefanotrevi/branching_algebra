#pragma once

#include "bnettypes.h"

#include <stdio.h>

// Inizializza una bnet (solo gli elementi diagonali saranno validi)
bnet_t bnetInit(int size);

// Elimina una rete
void bnetDel(bnet_t bnet);

// Restituisce la relazione fra gli intervalli i e j nella rete
brel_t bnetGet(bnet_t bnet, int i, int j);

// Inserisce la relazione r fra gli intervalli i e j
bnet_t bnetSet(bnet_t bnet, int i, int j, brel_t br);

// Inserisce la relazione r fra gli intervalli i e j, ed il suo converso fra j ed i
bnet_t bnetPut(bnet_t bnet, int i, int j, brel_t br);

// Ritorna 1 se la rete è consistente (cioè se non contiene la relazione vuota)
int bnetIsCons(bnet_t bnet);

// Stampa una rete di relazioni base
bnet_t bnetPrint(FILE *f, bnet_t bnet);

// Applica la path consistency ad una rete di sole relazioni base
int bnetPC(bnet_t bnet);
