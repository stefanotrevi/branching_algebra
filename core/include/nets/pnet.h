#pragma once

#include "bpnettypes.h"
#include "pnettypes.h"

#include <stdio.h>

// Inizializza una rete di dimensione size
pnet_t pnetInit(int size);

// Inizializza una rete casuale (densità va da 0 a 100)
pnet_t pnetInitRand(int size, int dens);

// Elimina una rete
void pnetDel(pnet_t pnet);

// Restituisce la relazione fra gli intervalli i e j nella rete
prel_t pnetGet(pnet_t pnet, int i, int j);

// Inserisce la relazione r fra gli intervalli i e j, senza propagazione e senza converso
pnet_t pnetSet(pnet_t pnet, int i, int j, prel_t r);

// Come pnetSet, ma imposta sia (i, j) che (j, i)
pnet_t pnetPut(pnet_t pnet, int i, int j, prel_t r);

// Applica la path consistency sulla rete. Ritorna 1 se è path-consistente
int pnetPC(pnet_t pnet);

// Versione migliorata, usa un buffer circolare ottimizzato
int pnetPC2(pnet_t pnet);

// Aggiunge r ad una rete già 3-consistente, ed impone PC -> più veloce di pnetPC()
int pnetAdd(pnet_t pnet, int i, int j, prel_t r);

// Stampa la rete sotto forma di tabella. Se hex è 1, stampa i valori numerici delle relazioni
pnet_t pnetPrint(FILE *f, pnet_t pnet, int hex);

// Ritorna 1 se le due reti sono uguali, 0 altrimenti
int pnetEquals(pnet_t n1, pnet_t n2);

// Copia src in dst, e lo ritorna. Se dst == pnetAlloc(0), viene inizializzato automaticamente.
pnet_t pnetCopy(pnet_t dst, pnet_t src);

// Ritorna 1 se la rete contiene l'elemento cercato, 0 altrimenti.
int pnetHas(pnet_t pnet, prel_t r);

// Ritorna 1 se la rete è consistente (cioè se non contiene la relazione vuota)
int pnetIsCons(pnet_t pnet);

// Ritorna la densità della rete
double pnetDens(pnet_t pnet);

// Genera un modello corrispondente al vettore di indici idx
bpnet_t pnetModel(pnet_t pnet, const int *idx);
