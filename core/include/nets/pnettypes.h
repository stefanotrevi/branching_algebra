#pragma once

#include "rels/preltypes.h"

typedef struct pnet // una rete BPA è un grafo, in cui gli indici sono intervalli
{
    int size;
    prel_t *rels; // matrice dei adiacenza delle relazioni nella rete
} pnet_t;
