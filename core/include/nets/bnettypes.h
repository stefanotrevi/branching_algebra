#pragma once

#include "rels/breltypes.h"

typedef struct bnet // una rete BA con sole relazioni base
{
    int size;
    brel_t *brs;
} bnet_t;
