#pragma once

#include "bpnettypes.h"

#include <stdio.h>

// Inizializza una bpnet (solo gli elementi diagonali saranno validi)
bpnet_t bpnetInit(int size);

// Elimina una rete
void bpnetDel(bpnet_t bpnet);

// Restituisce la relazione fra gli intervalli i e j nella rete
bprel_t bpnetGet(bpnet_t bpnet, int i, int j);

// Inserisce la relazione r fra gli intervalli i e j
bpnet_t bpnetSet(bpnet_t bpnet, int i, int j, bprel_t br);

// Inserisce la relazione r fra gli intervalli i e j, ed il suo converso fra j ed i
bpnet_t bpnetPut(bpnet_t bpnet, int i, int j, bprel_t br);

// Ritorna 1 se la rete è consistente (cioè se non contiene la relazione vuota)
int bpnetIsCons(bpnet_t bpnet);

// Stampa una rete di relazioni base
bpnet_t bpnetPrint(FILE *f, bpnet_t bpnet);

// Applica la path consistency ad una rete di sole relazioni base
int bpnetPC(bpnet_t bpnet);
