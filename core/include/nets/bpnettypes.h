#pragma once

#include "rels/bpreltypes.h"

typedef struct bpnet // una rete BPA con sole relazioni base
{
    int size;
    bprel_t *brs;
} bpnet_t;
