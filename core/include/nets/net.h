#pragma once

#include "bnettypes.h"
#include "nettypes.h"

#include <stdio.h>

// Inizializza una rete di dimensione size
net_t netInit(int size);

// Ritorna una copia di src
net_t netInitCopy(net_t src);

// Inizializza una rete leggendo da un file
net_t netInitByFile(FILE *f);

// Inizializza una rete dal file con nome fname
net_t netInitByFname(const char *fname);

// Inizializza una rete casuale (*_dens va da 0 a 100) scegliendo da allow (se != NULL)
net_t netInitRand(int size, int net_dens, int rel_dens, const rel_t *allow, int allow_n);

// Elimina una rete
void netDel(net_t net);

// Restituisce la relazione fra gli intervalli i e j nella rete
rel_t netGet(net_t net, int i, int j);

// Inserisce la relazione r fra gli intervalli i e j, senza propagazione e senza converso
void netSet(net_t net, int i, int j, rel_t r);

// Come netSet, ma imposta sia (i, j) che (j, i)
void netPut(net_t net, int i, int j, rel_t r);

// Applica la path consistency sulla rete. Ritorna 1 se è path-consistente
int netPC(net_t net);

// Versione "naive"
int netPC1(net_t net);

// Versione ottimizzata che usa come coda un array di lunghezza potenza di 2
int netPC2(net_t net);

// Versione ottimizzata che usa la moltiplicazione fra matrici in-place
int netPC3(net_t net);

// Calcola la consistenza completa della rete (tempo esponenziale), usando l'euristica specificata
int netFC(net_t net, partflag_t eur);

// Aggiunge r ad una rete già 3-consistente, ed impone PC -> più veloce di netPC()
int netAdd(net_t net, int i, int j, rel_t r);

// Stampa la rete sotto forma di tabella. Se hex è 1, stampa i valori numerici delle relazioni
net_t netPrint(FILE *f, net_t net, int hex);

// Ritorna 1 se le due reti sono uguali, 0 altrimenti
int netEquals(net_t n1, net_t n2);

// Copia src in dst, e lo ritorna
void netCopy(net_t dst, net_t src);

// Ritorna 1 se la rete è composta da sole relazioni Horn
int netIsHorn(net_t net);

// Ritorna 1 se la rete contiene l'elemento cercato, 0 altrimenti
int netHas(net_t net, rel_t r);

// Ritorna 1 se la rete non contiene la relazione vuota (non applica algoritmi di consistenza)
int netIsCons(net_t net);

// Ritorna la densità della rete
double netGetDens(net_t net);

// Genera un modello con il metodo di Ligozat, che ha senso solo se la rete è Horn e 3-consistente
bnet_t netModel(net_t net);

// Resetta una rete
void netReset(net_t net);
