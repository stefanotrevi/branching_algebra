#pragma once

#define GLUE(x, y) x##y
#define STRING0(x) #x
#define STRING(x) STRING0(x)

#define __INTEL_COMPILER_USE_INTRINSIC_PROTOTYPES

#ifdef _WIN32
    #define X86INTRIN_H <intrin.h>
#else
    #define X86INTRIN_H <x86intrin.h>
#endif

// abbreviamo un po'
typedef unsigned int uint;

// trova la lunghezza di un array o di una lista di inizializzazione
#define lengthof(...) (sizeof(__VA_ARGS__) / sizeof((__VA_ARGS__)[0]))

// Usiamo due paia di parentesi per aggirare la "feature" sulle costanti enum
// https://stackoverflow.com/questions/31160787/why-does-typeof-enum-constant-generate-a-warning-when-compared-to-a-variable-o
#define arrayof(type, ...)                                                                         \
    (type[]) { __VA_ARGS__ }

#define array(type, ...) arrayof(type, __VA_ARGS__), lengthof(arrayof(type, __VA_ARGS__))
