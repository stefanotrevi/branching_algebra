#include "rels/reltypes.h"

#define THREAD_N 4                    // numero di thread dalanciare in parallello
#define REL_PER_TH (REL_N / THREAD_N) // numero di elementi per ongi thread
#define LINELEN (1U << 12U)           // lunghezza massima di una linea
#define MAX_EQN 128U                  // numero massimo di equazioni per eqntott/espresso
#define CHILD_N 2                     // numero di figli
#define R_END 0                       // pipe read-end
#define W_END 1                       // pipe write-end

// Trova tutte le relazioni di Horn: (relazione->DNF->CNF->è Horn?). Su Windows fa solo il check
void filterHorn(void);
