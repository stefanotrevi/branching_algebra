#pragma once

#include "defaults.h"
#include <stdio.h>

// lunghezza buffer nome file (più modesto di FILENAME_MAX)
#define FILE_NAMELEN 512

// cartella radice
#ifndef STORE_PREFIX
    #define FILE_PREFIX "/home/ordhorn"
#else
    #define FILE_PREFIX STRING(STORE_PREFIX)
#endif

#define FILE_DIR_OUT FILE_PREFIX "/out"      // cartella di output
#define FILE_DIR_RES FILE_PREFIX "/res"      // cartella delle mappe
#define FILE_DIR_POINT FILE_DIR_OUT "/point" // cartella output delle trasformazioni puntuali

#ifdef BA
    #define FILE_SET_NAME "_ba" // estensione del file di output in caso BA
#else
    #define FILE_SET_NAME "_ia" // estensione del file di output in caso IA
#endif

#define FILE_EXT_TXT FILE_SET_NAME ".txt" // estensione file di testo
#define FILE_EXT_DAT FILE_SET_NAME ".dat" // estensione file binari

// File di output
#define FILE_CNF FILE_DIR_OUT "/cnf" FILE_EXT_TXT  // file di output formule CNF
#define FILE_DNF FILE_DIR_OUT "/dnf" FILE_EXT_TXT  // file di output formule DNF
#define FILE_OUT FILE_DIR_OUT "/out" FILE_EXT_TXT  // file di output risultati finali
#define BFILE_OUT FILE_DIR_OUT "/out" FILE_EXT_DAT // mappa risultati finali

// File di risorse/input
#define FILE_HORNMAP FILE_DIR_RES "/horn" FILE_EXT_DAT      // mappa relazioni Horn
#define FILE_POINTMAP FILE_DIR_RES "/point" FILE_EXT_DAT    // mappa relazioni puntizzabili
#define FILE_PCOMPMAP FILE_DIR_RES "/pcomplut" FILE_EXT_DAT // mappa composizioni fra punti
#define FILE_HORNPART FILE_DIR_RES "/hornpart" FILE_EXT_DAT // mappa partizioni Horn
#define FILE_COMPMAP FILE_DIR_RES "/complut" FILE_EXT_DAT   // mappa composizioni fra intervalli

// Ritorna 1 se il file con il nome fname esiste
int fileExists(char *fname);

// Versione portatile di mmap (read-only)
void *fileMap(char *fname, size_t size);

// Versione portatile di munmap
void fileUnmap(void *map, size_t size);

// Copia src in dst
long fileCopy(FILE *src, FILE *dst);

// Crea una nuova cartella
int fileMkdir(char *dname);
