#pragma once

#include "defaults.h"

typedef enum brel
{
    // BRANCHING RELATIONS
    UN,  // unrelated (interval)
    IE,  // initially equals
    IB,  // initially before
    IBI, // initially after
    IM,  // initially meets
    IMI, // initially met-by
    // LINEAR RELATIONS
    EQ,     // equals (interval)
    B,      // before
    BI,     // after
    D,      // during
    DI,     // contains
    F,      // finishes
    FI,     // finished-by
    M,      // meets
    MI,     // met-by
    O,      // overlaps
    OI,     // overlapped-by
    S,      // starts
    SI,     // started-by
    BREL_N, // numero di relazioni base
} brel_t;

#ifdef BA
    #define BR0 UN // prima relazione base (utile nei cicli)
#else
    #define BR0 EQ
#endif

#define BABR_N 6U // numero di relazioni base branching
