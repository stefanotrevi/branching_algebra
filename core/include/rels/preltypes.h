#pragma once

#include "bpreltypes.h"

#include <inttypes.h>

// 8-bit map per rappresentare una relazione tra punti (max. BPREL_N bit usati effettivamente!)
typedef uint8_t prel_t;

#define PREL0 ((prel_t)0)                // macro da usare per i contatori
#define PREL_N ((prel_t)(1U << BPREL_N)) // numero di relazioni tra punti
#define PREL_ALL ((prel_t)(PREL_N - 1U)) // relazione fra punti "tutto"
