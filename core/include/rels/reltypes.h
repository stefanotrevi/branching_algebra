#pragma once

#include "breltypes.h"

#include <inttypes.h>

typedef uint32_t rel_t; // 32-bit map per le relazioni IA/BA

#define MPI_REL_T MPI_UINT32_T // tipo per MPI

typedef enum partflag // flag per l'euristica della full-consistency
{
    PART_BASIC,
    PART_POINT,
    PART_HORN,
} partflag_t;


#define REL_N (1U << (uint)BREL_N)            // totale relazioni BA
#define REL_NIA (REL_N >> BABR_N)             // totale relazioni IA
#define REL_STEPIA (1U << BABR_N)             // step per ottenere solo le relazioni IA
#define REL_ALLBA (REL_STEPIA - 1U)           // relazione "tutte le branching"
#define REL_ALLIA ((REL_N - 1U) & ~REL_ALLBA) // relazione "tutte le lineari"
#define REL0 (rel_t)0                         //relazione da usare per i contatori

#ifdef BA
    #define REL_ALL (REL_N - 1U) // relazione "tutto" (in IA coincide con LINREL)
    #define ACTREL_N REL_N       // numero vero di relazioni a disposizione
    #define REL_IASHIFT 0U       // shift per passare alla relazione successiva
    #define REL_STEP 1U          // step per passare da una relazione alla successiva
    #define BLACK_N 8U           // totale relazioni base "nere"
    #define LAT_DIM_X 3          // dimensione lattice (X)
    #define LAT_DIM_Y 5          // dimensione lattice (Y)
    #define LAT_DIM_Z 5          // dimensione lattice (Z)

#else
    #define REL_ALL REL_ALLIA
    #define ACTREL_N REL_NIA
    #define REL_IASHIFT BABR_N
    #define REL_STEP REL_STEPIA
    #define BLACK_N 7
    #define LAT_DIM_X 5
    #define LAT_DIM_Y 5
    #define LAT_DIM_Z 1
#endif

//#define ONLY_HORN

#define MAX_BLACK (1U << BLACK_N) // totale relazioni "nere"

/* Una relazione è rappresentata con una bitmap del tipo "hhg gffe eddc cXbb aaXX". Per ottenere
il converso, dobbiamo scambiare le coppie, ignorando le relazioni "X" (simmetriche). Shiftiamo a 
sinistra e a destra di una posizione la bitmap, per poi rimuovere i bit superflui. */
#define CONV_LMASK 0b01010101010100101000U // tiene solo i bit per lo shift a sinistra
#define CONV_RMASK 0b00101010101010010100U // tiene solo i bit per lo shift a destra
#define CONV_MASK 0b1000011U               // tiene solo i bit delle relazioni simmetriche

/* 
 * Le seguenti macro servono per:
 * 1) Forzare l'inline nelle tabelle
 * 2) Inizializzare variabili statiche o globali
 * 3) Per non dover replicare le funzioni in CUDA
 * Anche se è garantita l'equivalenza con le rispettive funzioni, queste ultime DEVONO essere 
 * usate in codice esterno. (solo per comodità non sono state inserite in un header privato)
 * ATTENZIONE AGLI EFFETTI COLLATERALI!
*/

#define RELCONV(r) ((((r) << 1U) & CONV_LMASK) | (((r) >> 1U) & CONV_RMASK) | ((r)&CONV_MASK))
#define RELINV(r) (~(r)&REL_ALL)
#define RELADD(r1, r2) ((r1) | (r2))
#define RELSEC(r1, r2) ((r1) & (r2))
#define RELSUB(r1, r2) RELSEC(r1, RELINV(r2))
#define BRTOR(b) (1U << (b))
#define RELHAS(r, br) (!!RELSEC((r), BRTOR(br)))
#define RELREM(r, br) RELSUB(r, BRTOR(br))
#define RELPUT(r, br) RELADD(r, BRTOR(br))
#define RELISSUBSET(r1, r2) (RELSEC(r1, r2) == (r1))
#define RELISEMPTY(r) ((r) == REL0)
#define RELISIA(r) (((r)&REL_ALLIA) == (r))
#define RELISALL(r) ((r) >= REL_ALL)

// Sono equivalenti a brelAtoRel((brel_t[]){b1, ..., bn}, n), dove n è indicato nel nome
#define BRADD2(b1, b2) \
    RELADD(BRTOR(b1), BRTOR(b2))

#define BRADD3(b1, b2, b3) \
    RELADD(BRADD2(b1, b2), BRTOR(b3))

#define BRADD4(b1, b2, b3, b4) \
    RELADD(BRADD3(b1, b2, b3), BRTOR(b4))

#define BRADD5(b1, b2, b3, b4, b5) \
    RELADD(BRADD4(b1, b2, b3, b4), BRTOR(b5))

#define BRADD6(b1, b2, b3, b4, b5, b6) \
    RELADD(BRADD5(b1, b2, b3, b4, b5), BRTOR(b6))

#define BRADD7(b1, b2, b3, b4, b5, b6, b7) \
    RELADD(BRADD6(b1, b2, b3, b4, b5, b6), BRTOR(b7))

#define BRADD8(b1, b2, b3, b4, b5, b6, b7, b8) \
    RELADD(BRADD7(b1, b2, b3, b4, b5, b6, b7), BRTOR(b8))

#define BRADD9(b1, b2, b3, b4, b5, b6, b7, b8, b9) \
    RELADD(BRADD8(b1, b2, b3, b4, b5, b6, b7, b8), BRTOR(b9))

#define BRADD10(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10) \
    RELADD(BRADD9(b1, b2, b3, b4, b5, b6, b7, b8, b9), BRTOR(b10))

#define BRADD11(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11) \
    RELADD(BRADD10(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10), BRTOR(b11))

#define BRADD12(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12) \
    RELADD(BRADD11(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11), BRTOR(b12))

#define BRADD13(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13) \
    RELADD(BRADD12(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12), BRTOR(b13))

#define BRADD14(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14) \
    RELADD(BRADD13(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13), BRTOR(b14))

#define BRADD15(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15) \
    RELADD(BRADD14(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14), BRTOR(b15))

#define BRADD16(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16) \
    RELADD(BRADD15(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15), BRTOR(b16))

#define BRADD17(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17) \
    RELADD(BRADD16(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16), BRTOR(b17))

#define BRADD18(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18) \
    RELADD(BRADD17(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17), BRTOR(b18))
