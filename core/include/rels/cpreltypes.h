#pragma once

#include <inttypes.h>

// (4 * 8)-bit map per rappresentare la mappatura rel_t -> prel_t fra endpoint
typedef uint32_t cprel_t;

#define CPREL0 ((cprel_t)0)                 // macro da usare per i contatori
#define CPREL_N (1U << (BPREL_N * COUPLES_N)) // numero di relazioni tra endpoint
#define CPREL_ALL ((cprel_t)(CPREL_N - 1))   // relazione fra endpoint "tutto"
