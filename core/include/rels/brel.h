#pragma once

#include "cpreltypes.h"
#include "preltypes.h"
#include "reltypes.h"

// Ritorna la "prima" relazione base
brel_t brInit(void);

// Converte una relazione base in una relazione
rel_t brelToRel(brel_t br);

// Converte un array di relazioni base in una relazione
rel_t brelAtoRel(brel_t *br, int n);

// Ritorna le relazioni base minori di br, sotto forma di rel_t
rel_t brelLesser(brel_t br);

// Ritorna le relazioni base maggiori di br, sotto forma di rel_t
rel_t brelBigger(brel_t br);

// Ritorna la dimensione di br (cioè il numero di estremi NON coincidenti), o -1 se br non è valida
int brelDim(brel_t br);

// Ritorna la relazione base conversa di br
brel_t brelConv(brel_t br);

// Calcola la path-consistency fra relazioni base tra gli intervalli ij e jk, trovando ik
brel_t brelPath(brel_t ij, brel_t jk, brel_t ik);

// trasforma una relazione base in stringa
const char *brelToStr(brel_t br);

// trasforma una stringa in una relazione base
brel_t brelFromStr(const char *str);

// mappa le relazioni base fra intervalli su relazioni di estremi
cprel_t brelTocprel(brel_t br);

// ritorna la relazione composizione di due relazioni base
rel_t brelComp(brel_t r1, brel_t r2);
