#pragma once

#include "cpreltypes.h"
#include "preltypes.h"
#include "reltypes.h"

#define CPRELADD(cr, cs) ((cr) | (cs))
#define CPRELSHIFT(pc) ((pc)*BPREL_N)
#define PREL_TO_CPREL(pr, pc) ((cprel_t)(pr) << CPRELSHIFT(pc))
#define BPRTOCPR(bpr, pc) PREL_TO_CPREL(BPRTOPR(bpr), pc)

#define CPRELADD3(r1, r2, r3) CPRELADD(CPRELADD(r1, r2), r3)
#define CPRELADD4(r1, r2, r3, r4) CPRELADD(CPRELADD3(r1, r2, r3), r4)

#define BPREL4_TO_CPREL(b1, b2, b3, b4)                                                            \
    CPRELADD4(BPRTOCPR(b1, AMBM), BPRTOCPR(b2, AMBP), BPRTOCPR(b3, APBM), BPRTOCPR(b4, APBP))

// Inizializza un cprel vuoto
cprel_t cprelInit(void);

// Ritorna lo shift necessario per inserire un prel in un cprel alla posizione pc
cprel_t cprelShift(pcouples_t pc);

// Verifica se l'iesima relazione nella relazione composta contiene la relazione base indicata
int cprelHas(cprel_t cr, bprel_t br, pcouples_t pc);

// Interseca due cprel
cprel_t cprelSec(cprel_t cr1, cprel_t cr2);

// Somma due cprel
cprel_t cprelAdd(cprel_t cr1, cprel_t cr2);

// Inserisce un prel alla posizione specificata nel cprel (il contenuto viene sommato)
cprel_t cprelPut(cprel_t cr, pcouples_t pc, prel_t pr);

// Ritorna la relazione fra gli estremi pc
prel_t cprelGet(cprel_t cr, pcouples_t pc);

// Ritorna la composizione di due comprel
cprel_t cprelComp(cprel_t cr1, cprel_t cr2);

// Ritorna la relazione corrispondente ad un comprel
rel_t cprelToRel(cprel_t cr);
