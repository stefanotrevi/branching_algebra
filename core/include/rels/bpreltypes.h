#pragma once

typedef enum bprel
{
    PUN,   // unrelated
    PEQ,   // equals
    PB,    // before
    PBI,   // after
    BPREL_N, // numero di relazioni base fra punti
} bprel_t;

typedef enum pcouples
{
    AMBM,      // [A-, B-]
    AMBP,      // [A-, B+]
    APBM,      // [A+, B-]
    APBP,      // [A+, B+]
    COUPLES_N, // numero di coppie
} pcouples_t;

#define BPR0 PUN                                // macro da usare per i contatori
#define ALL_COUP ((1U << (uint)COUPLES_N) - 1U) // maschera che seleziona tutte le coppie
