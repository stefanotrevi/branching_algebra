#pragma once


#include "sets/settypes.h"
#include "preltypes.h"
#include "reltypes.h"

#include <stdio.h>

// Trasforma un contatore in una relazione nera
rel_t blackGet(uint i);

// Inizializza una relazione
rel_t relInit(void);

// Ritorna se la relazione r è la relazione vuota
int relIsEmpty(rel_t r);

// Ritorna se la relazione r è la relazione tutto
int relIsAll(rel_t r);

// Ritorna se la relazione r è lineare
int relIsIA(rel_t r);

// Ritorna se la relazione è in Horn
int relIsHorn(rel_t r);

// Ritorna se la relazione è puntizzabile
int relIsPoint(rel_t r);

// Verifica se la relazione base è contenuta o meno
int relHas(rel_t r, brel_t br);

// Verifica se r1 è un sottoinsieme di r2
int relSubSet(rel_t r1, rel_t r2);

// Verifica se r1 è un sottoinsieme proprio di r2
int relPropSet(rel_t r1, rel_t r2);

// interseca due relazioni
rel_t relSec(rel_t r1, rel_t r2);

// Aggiunge una relazione ad un'altra relazione
rel_t relAdd(rel_t r1, rel_t r2);

// Rimuove una relazione da un'altra relazione
rel_t relSub(rel_t r1, rel_t r2);

// Aggiunge una relazione base ad una relazione
rel_t relPut(rel_t r, brel_t br);

// Rimuove la relazione base br da r
rel_t relRem(rel_t r, brel_t br);

// Ritorna la relazione inversa di r (es. per "B, E", ritorna tutte le altre)
rel_t relInv(rel_t r);

// Ritorna la relazione conversa di r (es. per "B, E", ritorna "B', E")
rel_t relConv(rel_t r);

// Ritorna la parte "sinistra" di una relazione per la LUT
rel_t relLeft(rel_t r);

// Ritorna la parte "destra" di una relazione per la LUT
rel_t relRight(rel_t r);

// Ritorna la relazione composta usando la mappa relazioni base -> relazioni
rel_t relComp(rel_t r1, rel_t r2);

// Ritorna la relazione composta usando la tabella di composizione fra relazioni base
rel_t relComp2(rel_t r1, rel_t r2);

// Ritorna la relazione composta usando la mappatura su punti
rel_t relComp3(rel_t r1, rel_t r2);

// Calcola la path-consistency fra gli intervalli ij e jk (trovando ik)
rel_t relPath(rel_t ij, rel_t jk, rel_t ik);

// Ritorna una relazione casuale di densità dens
rel_t relRand(int dens);

// Ritorna una relazione casuale in allow
rel_t relSample(const rel_t *allow, int size);

// Restituisce la prima brel contenuta in r
brel_t relFirstBr(rel_t r);

// Ritorna il numero di relazioni base contenute in r
int relSize(rel_t r);

// Ritorna le brel successive a quelle in r (es. r = (1, 2, 5, 7) -> (2, 3, 6, 8))
rel_t relNext(rel_t r);

// Stampa una relazione
FILE *relPrint(FILE *f, rel_t r);

// Trasforma una stringa in una relazione BA
rel_t relFromStr(const char *str);

// Legge una relazione da un file
rel_t relFromFile(FILE *f);

// Trasforma una relazione BA in una stringa
int relToStr(char *buf, rel_t r);

// Ritorna un puntatore al partizionamento di r secondo il frammento specificato
const rel_t *relPartition(rel_t r, partflag_t flag);
