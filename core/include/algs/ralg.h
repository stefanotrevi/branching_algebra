#pragma once

#include "sets/rsettypes.h"
#include "palgtypes.h"
#include "ralgtypes.h"
#include "rels/bpreltypes.h"
#include "rels/reltypes.h"

// Ritorna l'insieme di relazioni dell'algebra scelta
rset_t ralgInit(ralgname_t name);

// Ritorna l'algebra derivante dall'algebra a punti scelta
rset_t ralgInitByPalg(palgname_t name);

// Ritorna l'algebra derivante dal prodotto: fst x snd*
rset_t ralgInitByDisj(palgname_t fst, palgname_t snd);

// Ritorna se l'algebra scelta contiene la relazione r
int ralgHasRel(ralgname_t name, rel_t r);

// Converte il nome dell'algebra in una stringa
const char *ralgName(ralgname_t name);

// Ricava il nome dell'algebra da una stringa
ralgname_t ralgFromStr(const char *name);

// Ritorna la lista delle algebre disponibili
void ralgNotFound(void);

// Ritorna se l'algebra è trattabile
int ralgIsTractable(rset_t alg);

// Ritorna se l'algebra generata da alg è massimale (non importa che alg sia già chiusa)
int ralgIsMaximal(rset_t alg, int rank, int size);

// Ritorna il "nucleo" dell'algebra (rimuove le intersezioni, i conversi e le composizioni)
int ralgKernel(rset_t *alg);

// Ritorna 1 se PathConsistency e FullConsistency sono equivalenti, 0 altrimenti
int ralgCheckPC(rset_t alg);

// Estende l'algebra con le relazioni dell'estensione selezionata
int ralgExtend(rset_t *alg, ralgname_t ext);
