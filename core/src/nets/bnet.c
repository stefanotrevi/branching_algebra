#include "nets/bnet.h"
#include "rels/brel.h"
#include "nets/queue.h"

#include <stdlib.h>

static bnet_t bnetAlloc(int size)
{
    return size ? (bnet_t){.size = size, .brs = malloc(size * size * sizeof(brel_t))} : (bnet_t){0};
}

bnet_t bnetInit(int size)
{
    bnet_t bnet = bnetAlloc(size);

    for (int i = 0; i < bnet.size; ++i)
    {
        bnetSet(bnet, i, i, EQ);
        for (int j = 0; j < i; ++j)
        {
            bnetSet(bnet, i, j, BREL_N);
            bnetSet(bnet, j, i, BREL_N);
        }
    }

    return bnet;
}

void bnetDel(bnet_t bnet)
{
    free(bnet.brs);
}

brel_t bnetGet(bnet_t bnet, int i, int j)
{
    return bnet.brs[i * bnet.size + j];
}

bnet_t bnetSet(bnet_t bnet, int i, int j, brel_t br)
{
    bnet.brs[i * bnet.size + j] = br;

    return bnet;
}

bnet_t bnetPut(bnet_t bnet, int i, int j, brel_t br)
{
    bnetSet(bnet, j, i, brelConv(br));

    return bnetSet(bnet, i, j, br);
}

int bnetIsCons(bnet_t bnet)
{
    int size = bnet.size * bnet.size;
    brel_t *brs = bnet.brs;

    for (int i = 0; i < size; ++i)
        if (brs[i] == BREL_N)
            return 0;

    return 1;
}

bnet_t bnetPrint(FILE *f, bnet_t bnet)
{
    for (int i = 0; i < bnet.size; ++i)
        fprintf(f, "\t%d", i);

    for (int i = 0; i < bnet.size; ++i)
    {
        fprintf(f, "\n%d", i);
        for (int j = 0; j < bnet.size; ++j)
            fprintf(f, "\t%s", brelToStr(bnetGet(bnet, i, j)));
    }
    putc('\n', f);

    return bnet;
}

static queue_t *putRelated(queue_t *q, int size, int i, int j)
{
    for (int k = 0; k < size; ++k)
        if (k != i && k != j)
        {
            queuePut(q, i);
            queuePut(q, j);
            queuePut(q, k);

            queuePut(q, k);
            queuePut(q, i);
            queuePut(q, j);
        }

    return q;
}

int bnetPC(bnet_t bnet)
{
    queue_t q = queueInit();

    for (int i = 0; i < bnet.size; ++i)
        for (int j = 0; j < bnet.size; ++j)
            if (i != j)
                putRelated(&q, bnet.size, i, j);

    while (!queueIsEmpty(q))
    {
        int i = queueGet(&q), j = queueGet(&q), k = queueGet(&q);
        brel_t path = brelPath(bnetGet(bnet, i, j), bnetGet(bnet, j, k), bnetGet(bnet, i, k));

        if (path != bnetGet(bnet, i, k))
        {
            bnetPut(bnet, i, k, path);
            putRelated(&q, bnet.size, i, k);
        }
    }
    queueDel(q);

    return bnetIsCons(bnet);
}
