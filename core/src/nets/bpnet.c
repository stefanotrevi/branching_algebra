#include "nets/bpnet.h"
#include "rels/bprel.h"
#include "nets/queue.h"

#include <stdlib.h>

static bpnet_t bpnetAlloc(int size)
{
    return size ? (bpnet_t){.size = size, .brs = malloc(size * size * sizeof(bprel_t))} : (bpnet_t){0};
}

bpnet_t bpnetInit(int size)
{
    bpnet_t bpnet = bpnetAlloc(size);

    for (int i = 0; i < bpnet.size; ++i)
    {
        bpnetSet(bpnet, i, i, PEQ);
        for (int j = 0; j < i; ++j)
        {
            bpnetSet(bpnet, i, j, BPREL_N);
            bpnetSet(bpnet, j, i, BPREL_N);
        }
    }

    return bpnet;
}

void bpnetDel(bpnet_t bpnet)
{
    free(bpnet.brs);
}

bprel_t bpnetGet(bpnet_t bpnet, int i, int j)
{
    return bpnet.brs[i * bpnet.size + j];
}

bpnet_t bpnetSet(bpnet_t bpnet, int i, int j, bprel_t br)
{
    bpnet.brs[i * bpnet.size + j] = br;

    return bpnet;
}

bpnet_t bpnetPut(bpnet_t bpnet, int i, int j, bprel_t br)
{
    bpnetSet(bpnet, j, i, bprelConv(br));

    return bpnetSet(bpnet, i, j, br);
}

int bpnetIsCons(bpnet_t bpnet)
{
    int size = bpnet.size * bpnet.size;
    bprel_t *brs = bpnet.brs;

    for (int i = 0; i < size; ++i)
        if (brs[i] == BPREL_N)
            return 0;

    return 1;
}

bpnet_t bpnetPrint(FILE *f, bpnet_t bpnet)
{
    for (int i = 0; i < bpnet.size; ++i)
        fprintf(f, "\t%d", i);

    for (int i = 0; i < bpnet.size; ++i)
    {
        fprintf(f, "\n%d", i);
        for (int j = 0; j < bpnet.size; ++j)
            fprintf(f, "\t%s", bprelToStr(bpnetGet(bpnet, i, j)));
    }
    putc('\n', f);

    return bpnet;
}

static queue_t *putRelated(queue_t *q, int size, int i, int j)
{
    for (int k = 0; k < size; ++k)
        if (k != i && k != j)
        {
            queuePut(q, i);
            queuePut(q, j);
            queuePut(q, k);

            queuePut(q, k);
            queuePut(q, i);
            queuePut(q, j);
        }

    return q;
}

int bpnetPC(bpnet_t bpnet)
{
    queue_t q = queueInit();

    for (int i = 0; i < bpnet.size; ++i)
        for (int j = 0; j < bpnet.size; ++j)
            if (i != j)
                putRelated(&q, bpnet.size, i, j);

    while (!queueIsEmpty(q))
    {
        int i = queueGet(&q), j = queueGet(&q), k = queueGet(&q);
        bprel_t path = bprelPath(bpnetGet(bpnet, i, j), bpnetGet(bpnet, j, k), bpnetGet(bpnet, i, k));

        if (path != bpnetGet(bpnet, i, k))
        {
            bpnetPut(bpnet, i, k, path);
            putRelated(&q, bpnet.size, i, k);
        }
    }
    queueDel(q);

    return bpnetIsCons(bpnet);
}
