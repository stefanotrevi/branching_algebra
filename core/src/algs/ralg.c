#include "algs/ralg.h"
#include "algs/palg.h"
#include "files.h"
#include "nets/net.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"
#include "sets/cuRset.cuh"
#include "sets/pset.h"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef MPI_ENABLE
    #include <mpi.h>
#endif

#ifndef _WIN32
    #include <strings.h>
#else
    #define strcasecmp _stricmp
#endif

// Valuta: ((R \sec S)^(\pm 1) != 0) ==> T \subset R
#define RALG_COND(r, s, t)                                                                         \
    ((RELISEMPTY(RELSEC(r, s)) || RELISSUBSET(t, r)) &&                                            \
     (RELISEMPTY(RELSEC(r, RELCONV(s))) || RELISSUBSET(RELCONV(t), r)))

// Valuta: ((R \sec S1)^(\pm 1) != 0) && ((R \sec S2)^(\pm 1) != 0) ==> T \subset R
#define RALG_COND2(r, s1, s2, t)                                                                   \
    ((RELISEMPTY(RELSEC(r, s1)) || RELISEMPTY(RELSEC(r, s2)) || RELISSUBSET(t, r)) &&              \
     (RELISEMPTY(RELSEC(r, RELCONV(s1))) || RELISEMPTY(RELSEC(r, RELCONV(s2))) ||                  \
      RELISSUBSET(RELCONV(t), r)))

// Valuta: ((R \sec S1)^(\pm 1) != 0) && !(R \subset S2)^(\pm 1) ==> T \subset R
#define RALG_COND3(r, s1, s2, t)                                                                   \
    ((RELISEMPTY(RELSEC(r, s1)) || RELISSUBSET(r, s2) || RELISSUBSET(t, r)) &&                     \
     (RELISEMPTY(RELSEC(r, RELCONV(s1))) || RELISSUBSET(r, RELCONV(s2)) ||                         \
      RELISSUBSET(RELCONV(t), r)))

rset_t ralgInit(ralgname_t name)
{
    rset_t alg;

    if (RALG_POINT <= name && name < RALG_LIN_N)
    {
        alg = name < RALG_POINT_N ? ralgInitByPalg(PALG_GAMMA_A)
                                  : ralgInitByDisj(PALG_GAMMA_B, PALG_DELTA_B);
        if (ralgExtend(&alg, name))
            rsetClose(&alg, CLOSE_FULL);
    }
    else
    {
        alg = rsetInit();
        for (rel_t r = REL0; r < REL_N; r += REL_STEP)
            if (ralgHasRel(name, r))
                rsetIns(&alg, r);

        //rsetClose(&alg, CLOSE_FULL);
    }

    return alg;
}

rset_t ralgInitByPalg(palgname_t name)
{
    pset_t palg = palgInit(name);
    rset_t ralg = rsetInitByPset(palg);

    rsetClose(&ralg, CLOSE_FULL | CLOSE_PRINT);

    psetDel(palg);

    return ralg;
}

rset_t ralgInitByDisj(palgname_t fst, palgname_t snd)
{
    pset_t pset1 = palgInit(fst);
    pset_t pset2 = palgInit(snd);
    rset_t ralg = rsetInitByPdisj(pset1, pset2);


    rsetClose(&ralg, CLOSE_FULL);
    psetDel(pset1);
    psetDel(pset2);

    return ralg;
}

int ralgHasRel(ralgname_t name, rel_t r)
{
    switch (name)
    {
        // clang-format off
    case RALG_SIGMA_B: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(B));
    case RALG_SIGMA_D: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(DI));
    case RALG_SIGMA_O: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(O));
    case RALG_ALPHA_1: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(SI));
    case RALG_ALPHA_2: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(S));
    case RALG_ALPHA_3: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, F),   BRTOR(S));
    case RALG_ALPHA_4: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, FI),  BRTOR(S));
    case RALG_EPSI_B:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, S),   BRTOR(B));
    case RALG_EPSI_D:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, S),   BRTOR(D));
    case RALG_EPSI_O:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, S),   BRTOR(O));
    case RALG_BETA_1:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, S),   BRTOR(FI));
    case RALG_BETA_2:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, D, S),   BRTOR(F));
    case RALG_BETA_3:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, SI), BRTOR(FI));
    case RALG_BETA_4:  return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, S),  BRTOR(FI));
    case RALG_SIGMA_S: return  RELISIA(r) && RALG_COND(r, BRADD5(B, M, O, DI, FI), BRTOR(FI)) &&
                                RALG_COND(r, BRADD2(S, SI), BRTOR(EQ));
    case RALG_EPSI_S:  return  RELISIA(r) && RALG_COND(r,  BRADD5(B, M, O, D, S),  BRTOR(S))  &&
                                RALG_COND(r, BRADD2(F, FI), BRTOR(EQ));
    case RALG_HORN_IA: return  RELISIA(r) && RALG_COND2(r, BRADD2(O, S), BRADD2(OI, F),  BRTOR(D)) &&
                                RALG_COND2(r, BRADD2(D, S), BRADD2(DI, FI), BRTOR(O)) &&
                                RALG_COND3(r, BRADD2(B, M), BRADD2(B, M),   BRTOR(O));
        // clang-format on

    case RALG_EQUAL: return RELHAS(r, EQ);
    case RALG_HORN: return relIsHorn(r);
    case RALG_POINT: return relIsPoint(r);
    default: return 0;
    }
}

#define case_name(x)                                                                               \
    case GLUE(RALG_, x): return STRING(x)

const char *ralgName(ralgname_t name)
{
    switch (name)
    {
        case_name(SIGMA_B);
        case_name(SIGMA_D);
        case_name(SIGMA_O);
        case_name(ALPHA_1);
        case_name(ALPHA_2);
        case_name(ALPHA_3);
        case_name(ALPHA_4);
        case_name(EPSI_B);
        case_name(EPSI_D);
        case_name(EPSI_O);
        case_name(BETA_1);
        case_name(BETA_2);
        case_name(BETA_3);
        case_name(BETA_4);
        case_name(SIGMA_S);
        case_name(EPSI_S);
        case_name(HORN_IA);
        case_name(EQUAL);
        case_name(HORN);
        case_name(POINT);
        case_name(POINT_1);
        case_name(POINT_2);
        case_name(POINT_3);
        case_name(POINT_4);
        case_name(POINT_5);
        case_name(LIN);
        case_name(LIN_1);
        case_name(LIN_2);
        case_name(LIN_3);
        case_name(LIN_4);
        case_name(LIN_5);
        case_name(LIN_6);
        case_name(LIN_2_3);
        case_name(LIN_2_4);
        case_name(LIN_4_5);
    default: return "Ø";
    }
}

#define compare_and_ret(x)                                                                         \
    if (!strcasecmp(name, STRING(x)))                                                              \
    return GLUE(RALG_, x)

ralgname_t ralgFromStr(const char *name)
{
    compare_and_ret(SIGMA_B);
    compare_and_ret(SIGMA_D);
    compare_and_ret(SIGMA_O);
    compare_and_ret(SIGMA_S);
    compare_and_ret(ALPHA_1);
    compare_and_ret(ALPHA_2);
    compare_and_ret(ALPHA_3);
    compare_and_ret(ALPHA_4);
    compare_and_ret(EPSI_B);
    compare_and_ret(EPSI_D);
    compare_and_ret(EPSI_O);
    compare_and_ret(EPSI_S);
    compare_and_ret(BETA_1);
    compare_and_ret(BETA_2);
    compare_and_ret(BETA_3);
    compare_and_ret(BETA_4);
    compare_and_ret(HORN_IA);
    compare_and_ret(EQUAL);
    compare_and_ret(HORN);
    compare_and_ret(POINT);
    compare_and_ret(POINT_1);
    compare_and_ret(POINT_2);
    compare_and_ret(POINT_3);
    compare_and_ret(POINT_4);
    compare_and_ret(POINT_5);
    compare_and_ret(LIN);
    compare_and_ret(LIN_1);
    compare_and_ret(LIN_2);
    compare_and_ret(LIN_3);
    compare_and_ret(LIN_4);
    compare_and_ret(LIN_5);
    compare_and_ret(LIN_6);
    compare_and_ret(LIN_2_3);
    compare_and_ret(LIN_2_4);
    compare_and_ret(LIN_4_5);

    return RALG_N;
}

void ralgNotFound(void)
{
    fprintf(stderr, "Algebra non trovata! Algebre disponibili:\n");

    for (ralgname_t i = (ralgname_t)0; i < RALG_N; ++i)
        fprintf(stderr, "%s ", ralgName(i));

    putc('\n', stderr);
}

// Ritorna quante sovralgebre di alg sono NP-complete
static int ralgIsMaximalWorker(rset_t alg, int rank, int size)
{
    FILE *out;
    char fname[FILE_NAMELEN];
    uint8_t *pin_alg;                       // copia di alg in memoria pinned
    rel_t *known;                           // array della riduzione di pin_alg
    uint8_t *sup;                           // sovralgebra di alg
    uint8_t *hmap, *dmap;                   // mappe host e device ausiliarie per closeCheck
    rel_t *done, *todo, *todo_new;          // array ausiliari per closeCheck
    int ret = 0;                            // valore di ritorno
    uint todo_n;                            // nuero di elementi da processare
    rel_t first = REL_N / size * rank;      // relazione iniziale
    rel_t last = REL_N / size * (rank + 1); // relazione finale
    rel_t *extra = calloc(REL_N, sizeof *extra);

    sprintf(fname, FILE_OUT "_%d_%d", alg.num, rank);
    out = fopen(fname, "wt");

    cudaHostAlloc((void **)&todo_new, REL_N * sizeof *todo_new, cudaHostAllocDefault);
    cudaHostAlloc((void **)&sup, REL_N * sizeof *sup, cudaHostAllocDefault);
    cudaHostAlloc((void **)&hmap, REL_N * sizeof *hmap, cudaHostAllocDefault);
    cudaHostAlloc((void **)&pin_alg, REL_N * sizeof *pin_alg, cudaHostAllocDefault);
    cudaHostAlloc((void **)&known, REL_N * sizeof *known, cudaHostAllocDefault);
    cudaMalloc((void **)&dmap, REL_N * sizeof *dmap);
    cudaMalloc((void **)&done, REL_N * sizeof *done);
    cudaMalloc((void **)&todo, REL_N * sizeof *todo);

    cudaMemset(dmap, 0, REL_N * sizeof *dmap);
    memcpy(pin_alg, alg.data, REL_N * sizeof *pin_alg);
    todo_n = rsetToArr(alg, known) + 1;

    fprintf(out, "Rank: %d, Start: %u, End: %u\n", rank, first, last);
    for (int i = 0; i < 2 * rank; ++i)
        putchar('\t');
    printf("%d: [%u, %u]\n", rank, first, last);

    for (rel_t r = first; r < last; r += REL_STEP)
        if (!pin_alg[r] && r <= relConv(r)) // non devo controllare le converse
        {
            int done_n;

            pin_alg[r] = 1;
            cudaMemcpyAsync(sup, pin_alg, REL_N * sizeof *pin_alg, cudaMemcpyHostToHost,
                            cuGetStream(0));
            known[todo_n - 1] = r;
            cudaMemcpyAsync(todo, known, todo_n * sizeof *known, cudaMemcpyHostToDevice,
                            cuGetStream(0));
            done_n = rsetCloseAndCheck(sup, hmap, dmap, done, todo, todo_n, todo_new);

            if (done_n > 0)
            {
                fprintf(out, "%u: (%d)\n", r, done_n);
                extra[done_n] = r;
            }

            if (!(r & 127))
            {
                fprintf(out, "%u\n", r);
                fflush(out);
            }
            pin_alg[r] = 0;
        }

    fprintf(out, "Extra sets:\n");
    for (uint i = 0; i < REL_N; ++i)
        if (extra[i])
            fprintf(out, "%u: %u\n", extra[i], i);
    fprintf(out, "Finished!\n");

    cudaFreeHost(todo_new);
    cudaFreeHost(sup);
    cudaFreeHost(hmap);
    cudaFreeHost(pin_alg);
    cudaFreeHost(known);
    cudaFree(dmap);
    cudaFree(done);
    cudaFree(todo);
    free(extra);
    fclose(out);

    return ret;
}

int ralgIsTractable(rset_t alg)
{
    return rsetCheckCorner(alg);
}

int ralgIsMaximal(rset_t alg, int rank, int size)
{
    int all_ret = 0;
    int ret;

    // assicuriamoci sia effettivamente un'algebra
    rsetClose(&alg, CLOSE_FULL);

    if (!ralgIsTractable(alg))
        return -1;

    ret = ralgIsMaximalWorker(alg, rank, size);

#ifdef MPI_ENABLE
    MPI_Reduce(&ret, &all_ret, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
#else
    (void)size;
    all_ret = ret;
#endif

    return !all_ret; // tutti i processi hanno avuto successo (somma == 0)
}

int ralgKernel(rset_t *alg)
{
    rel_t *buf = malloc(REL_N * sizeof *buf);
    int n = rsetToArr(*alg, buf);

    // Rimuoviamo tutti i conversi
    for (int i = 0; i < n; ++i)
    {
        rel_t conv = relConv(buf[i]);

        if (buf[i] != conv && rsetGet(*alg, buf[i]) && rsetGet(*alg, conv))
            rsetRem(alg, conv);
    }

    // Rimuoviamo tutte le intersezioni
    for (int end = 0; !end;)
    {
        end = 1;
        n = rsetToArr(*alg, buf);

        for (int i = 0; i < n; ++i)
            if (rsetGet(*alg, buf[i]))
                for (int j = 0; j < n; ++j)
                {
                    rel_t sec = relSec(buf[i], buf[j]);

                    if (sec != buf[i] && sec != buf[j] && rsetGet(*alg, sec) &&
                        rsetGet(*alg, buf[j]))
                    {
                        rsetRem(alg, sec);
                        end = 0;
                    }
                }
    }

    // Rimuoviamo tutte le composizioni
    for (int end = 0; !end;)
    {
        end = 1;
        n = rsetToArr(*alg, buf);

        for (int i = 0; i < n; ++i)
            if (rsetGet(*alg, buf[i]))
                for (int j = 0; j < n; ++j)
                {
                    rel_t comp = relComp(buf[i], buf[j]);

                    if (comp != buf[i] && comp != buf[j] && rsetGet(*alg, comp) &&
                        rsetGet(*alg, buf[j]))
                    {
                        rsetRem(alg, comp);
                        end = 0;
                    }
                }
    }

    free(buf);

    return alg->num;
}

static int ralgCheckPCInner(rel_t *rels, int i[6], net_t net_pc, net_t net_fc)
{
    netPut(net_pc, 0, 1, rels[i[0]]);
    netPut(net_pc, 0, 2, rels[i[1]]);
    netPut(net_pc, 0, 3, rels[i[2]]);
    netPut(net_pc, 1, 2, rels[i[3]]);
    netPut(net_pc, 1, 3, rels[i[4]]);
    netPut(net_pc, 2, 3, rels[i[5]]);
    netCopy(net_fc, net_pc);

    return netPC2(net_pc) == netFC(net_fc, PART_BASIC);
}

static uint64_t ipow(uint64_t base, uint64_t exp)
{
    uint64_t result = 1;

    for (exp <<= 1; exp >>= 1; base *= base)
        if (exp & 1)
            result *= base;

    return result;
}

int ralgCheckPC(rset_t alg)
{
    rel_t *buf = malloc(alg.num * sizeof *buf);
    net_t net_pc = netInit(4);
    net_t net_fc = netInit(4);
    int i[6];
    uint64_t current = 0;
    uint64_t total = ipow(alg.num, 6);

    printf("The given set has %d relations, number of tests to perform: %llu\n", alg.num, total);
    rsetToArr(alg, buf);

    for (i[0] = 0; i[0] < alg.num; ++i[0])
        for (i[1] = 0; i[1] < alg.num; ++i[1])
            for (i[2] = 0; i[2] < alg.num; ++i[2])
            {
                for (i[3] = 0; i[3] < alg.num; ++i[3])
                    for (i[4] = 0; i[4] < alg.num; ++i[4])
                        for (i[5] = 0; i[5] < alg.num; ++i[5], ++current)
                            if (!ralgCheckPCInner(buf, i, net_pc, net_fc))
                                goto outside;
                printf("Runs performed: %llu/%llu\r", current, total);
                fflush(stdout);
            }

outside:

    printf("\nidx: {%d, %d, %d, %d, %d, %d}\n", i[0], i[1], i[2], i[3], i[4], i[5]);
    netPrint(stdout, net_pc, 0);
    free(buf);
    netDel(net_pc);
    netDel(net_fc);

    return 0;
}

int ralgExtend(rset_t *alg, ralgname_t ext)
{
    switch (ext)
    {
    // estensioni di BApoint
    case RALG_POINT_1: rsetIns(alg, 439508); return 1;
    case RALG_POINT_2: rsetIns(alg, 524272); return 1;
    case RALG_POINT_3: rsetIns(alg, 261823); return 1;
    case RALG_POINT_4: rsetIns(alg, 261887); return 1;
    case RALG_POINT_5: rsetIns(alg, 524284); return 1;
    // estensioni di BAlin
    case RALG_LIN_1: rsetIns(alg, 524279); return 1;
    case RALG_LIN_2: rsetIns(alg, 524237); return 1;
    case RALG_LIN_3: rsetIns(alg, 524245); return 1;
    case RALG_LIN_4: rsetIns(alg, 524247); return 1;
    case RALG_LIN_5: rsetIns(alg, 524253); return 1;
    case RALG_LIN_6: rsetIns(alg, 524255); return 1;
    case RALG_LIN_2_3:
        rsetIns(alg, 524237);
        rsetIns(alg, 524245);
        return 1;
    case RALG_LIN_2_4:
        rsetIns(alg, 524237);
        rsetIns(alg, 524247);
        return 1;
    case RALG_LIN_4_5:
        rsetIns(alg, 524247);
        rsetIns(alg, 524253);
        return 1;

    default: return 0;
    }
}
