#include "lut.h"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <device_atomic_functions.h>
#include <device_launch_parameters.h>
#include <stdio.h>

#define N_STREAM 3
#define BLKSZ 32U

__constant__ static rel_t *dev_map;
static rel_t *cuComp_map;
static cudaStream_t stream[N_STREAM];

__device__ extern void __syncthreads();

__device__ static inline rel_t cuComp(rel_t r1, rel_t r2)
{
#ifdef BA
    rel_t l1{COMPLUT_GETLEFT(r1)}, l2{COMPLUT_GETLEFT(r2)}, r;

    r1 = COMPLUT_GETRIGHT(r1);
    r2 = COMPLUT_GETRIGHT(r2);
    r = dev_map[r1 * COMPLUT_NR + r2];
    r = RELADD(r, dev_map[COMPLUT_RL + r1 * COMPLUT_NL + l2]);
    r = RELADD(r, dev_map[COMPLUT_LR + l1 * COMPLUT_NR + r2]);
    r = RELADD(r, dev_map[COMPLUT_LL + l1 * COMPLUT_NL + l2]);

    return r;
#else
    return dev_map[r1 * (REL_NIA >> BABR_N) + (r2 >> BABR_N)];
#endif
}

template<closeflags_t flags>
__global__ static void cuRsetClose_ker(uint8_t *map, rel_t *fst, uint n_fst, rel_t *snd, uint n_snd)
{
    uint i = blockIdx.x * blockDim.x + threadIdx.x;
    uint j = blockIdx.y * blockDim.y + threadIdx.y;

    if (i >= n_fst || j >= n_snd)
        return;

    rel_t f = fst[i];
    [[maybe_unused]] rel_t s = snd[j];

    // per non fare O(n^3) passi, calcoliamo in due parti. Non serve sincronizzare, al massimo
    // otteniamo qualche relazione inutile, ma costa molto meno.
    if constexpr (flags & CLOSE_SCOMP)
        map[cuComp(f, s)] = 2;

    if constexpr (flags & CLOSE_CONV)
        map[RELCONV(f)] = 1;

    if constexpr (flags & CLOSE_SEC)
        map[RELSEC(f, s)] = 1;

    if constexpr (flags & CLOSE_UNION)
        map[RELADD(f, s)] = 1;

    if constexpr (flags & CLOSE_WCOMP)
        map[cuComp(f, s)] = 1;
}

extern "C" {
int cuLutInit(int rank, const rel_t *comp_map)
{
    int ndev = 1, idev;

    cudaGetDeviceCount(&ndev);
    idev = rank % ndev;
    cudaSetDevice(idev);

    cudaMalloc((void **)&cuComp_map, COMPLUT_N * sizeof(*cuComp_map));
    cudaMemcpy(cuComp_map, comp_map, COMPLUT_N * sizeof(*cuComp_map), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(dev_map, &cuComp_map, sizeof(cuComp_map), 0, cudaMemcpyHostToDevice);

    for (int i = 0; i < N_STREAM; ++i)
        cudaStreamCreate(stream + i);

    return idev;
}

void cuLutFinish(void)
{
    cudaFree(cuComp_map);

    for (int i = 0; i < N_STREAM; ++i)
        cudaStreamDestroy(stream[i]);
}

cudaStream_t cuGetStream(int i)
{
    return stream[i];
}

void cuRsetClose(uint8_t *set, rel_t *fst, uint n_fst, rel_t *snd, uint n_snd, cudaStream_t stream,
                 closeflags_t flags)
{
    // ogni blocco è 32x32, la griglia ha come dimensione il numero di blocchi
    static const dim3 blksz{BLKSZ, BLKSZ};
    dim3 grdsz{(n_fst / BLKSZ) + 1U, (n_snd / BLKSZ) + 1U};

#define closekercase(flag)                                                                         \
    case flag:                                                                                     \
        cuRsetClose_ker<flag><<<grdsz, blksz, 0, stream>>>(set, fst, n_fst, snd, n_snd);           \
        break

    switch (flags)
    {
        closekercase(CLOSE_CONV);

        closekercase(CLOSE_SEC);
        closekercase(CLOSE_CONV | CLOSE_SEC);

        closekercase(CLOSE_UNION);
        closekercase(CLOSE_UNION | CLOSE_CONV);
        closekercase(CLOSE_UNION | CLOSE_SEC);
        closekercase(CLOSE_UNION | CLOSE_SEC | CLOSE_CONV);

        closekercase(CLOSE_WCOMP);
        closekercase(CLOSE_WCOMP | CLOSE_CONV);
        closekercase(CLOSE_WCOMP | CLOSE_SEC);
        closekercase(CLOSE_WCOMP | CLOSE_SEC | CLOSE_CONV);
        closekercase(CLOSE_WCOMP | CLOSE_UNION);
        closekercase(CLOSE_WCOMP | CLOSE_UNION | CLOSE_CONV);
        closekercase(CLOSE_WCOMP | CLOSE_UNION | CLOSE_SEC);
        closekercase(CLOSE_WCOMP | CLOSE_UNION | CLOSE_SEC | CLOSE_CONV);

        closekercase(CLOSE_SCOMP);
        closekercase(CLOSE_SCOMP | CLOSE_CONV);
        closekercase(CLOSE_SCOMP | CLOSE_SEC);
        closekercase(CLOSE_SCOMP | CLOSE_SEC | CLOSE_CONV);
        closekercase(CLOSE_SCOMP | CLOSE_UNION);
        closekercase(CLOSE_SCOMP | CLOSE_UNION | CLOSE_CONV);
        closekercase(CLOSE_SCOMP | CLOSE_UNION | CLOSE_SEC);
        closekercase(CLOSE_SCOMP | CLOSE_UNION | CLOSE_SEC | CLOSE_CONV);
        // le altre combinazioni sono impedite da rsetClose

    default: break;
    }
}
}
