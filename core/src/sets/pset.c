#include "sets/pset.h"
#include "rels/prel.h"

#include <stdlib.h>

pset_t psetInit(void)
{
    return (pset_t){.data = calloc(REL_N, sizeof(uint8_t))};
}

void psetDel(pset_t pset)
{
    if (pset.data)
        free(pset.data);
}

pset_t psetInitByArr(prel_t *array, int n)
{
    pset_t pset = psetInit();

    for (int i = 0; i < n; ++i)
        psetIns(&pset, array[i]);

    return pset;
}

uint8_t psetGet(pset_t pset, prel_t pr)
{
    return pset.data[pr];
}

void psetPut(pset_t *pset, prel_t pr, uint8_t val)
{
    pset->num += !!val - !!pset->data[pr];
    pset->data[pr] = val;
}

void psetIns(pset_t *pset, prel_t pr)
{
    psetPut(pset, pr, 1);
}

void psetRem(pset_t *pset, prel_t pc)
{
    psetPut(pset, pc, 0);
}

void psetPrint(FILE *f, pset_t pset)
{
    for (prel_t pr = PREL0; pr < PREL_N; ++pr)
        if (psetGet(pset, pr))
            prelPrint(f, pr);
}

int psetToArray(pset_t pset, prel_t *array)
{
    int n = 0;

    for (prel_t pr = PREL0; pr < PREL_N; ++pr)
        if (psetGet(pset, pr))
            array[n++] = pr;

    return n;
}
