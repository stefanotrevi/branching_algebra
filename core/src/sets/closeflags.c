#include "sets/closeflags.h"

const char *closeflagToStr(closeflag_t flag)
{
    switch (flag)
    {
    case CLOSE_CONV: return "converso";
    case CLOSE_SEC: return "intersezione";
    case CLOSE_UNION: return "unione";
    case CLOSE_WCOMP: return "composizione debole";
    case CLOSE_SCOMP: return "composizione forte";
    case CLOSE_PRINT: return "info progresso";
    default: return "???";
    }
}
