#include "lits/cnf.h"
#include "lits/lit.h"
#include "rels/rel.h"
#include "rels/prel.h"

#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#include X86INTRIN_H

cls_t clsInit(void)
{
    return 0ULL;
}

cls_t clsShift(clsidx_t i)
{
    return i * (BLIT_N * COUPLES_N);
}

cls_t clToCls(clit_t cl, clsidx_t i)
{
    return (cls_t)cl << clsShift(i);
}

int clsHas(cls_t cls, clsidx_t cls_i, blit_t lit_i, pcouples_t pc)
{
    return clHas(cls >> clsShift(cls_i), lit_i, pc);
}

int clsCount(cls_t cls)
{
    return _popcnt64(cls);
}

int clsPosCount(cls_t cls)
{
    return clCount(clsGet(cls, L_POS)) + clCount(litToCl(clGet(clsGet(cls, L_NEG), U), U));
}

cls_t clsAdd(cls_t cls1, cls_t cls2)
{
    return cls1 | cls2;
}

cls_t clsPut(cls_t cls, clsidx_t i, clit_t cl)
{
    return clsAdd(cls, clToCls(cl, i));
}

cls_t clsIns(cls_t cls, clsidx_t i, blit_t bl, pcouples_t pc)
{
    return clsPut(cls, i, clIns(clInit(), bl, pc));
}

cls_t clsInsChar(cls_t cls, char c, clsidx_t idx)
{
    litcoord_t lc = litGetCoord(c);

    return clsIns(cls, idx, lc.bl, lc.pc);
}

clit_t clsGet(cls_t cls, clsidx_t i)
{
    return (clit_t)((cls >> clsShift(i)) & ALL_LIT);
}

rel_t clsToRel(cls_t cls)
{
    rel_t r = REL0;

    if (!cls)
        return REL_ALL;

    for (clsidx_t ci = (clsidx_t)0; ci < L_NUM; ++ci)
    {
        clit_t cl = clsGet(cls, ci);

        if (cl)
            r = relAdd(r, clToRel(cl, ci));
    }
    return r;
}

size_t clsToStr(char *buf, cls_t cls)
{
    clit_t cln = clsGet(cls, L_NEG), clp = clsGet(cls, L_POS);
    size_t n = 0;

    if (!cls)
        sprintf(buf, "()");
    if (cln)
        n += clToStr(buf, cln, L_NEG);
    if (clp)
    {
        if (cln)
            n += sprintf(buf + n, " v ");
        n += clToStr(buf + n, clp, L_POS);
    }

    return n;
}

cnf_t cnfInit(void)
{
    return (cnf_t){.r = REL_ALL};
}

int cnfIns(cnf_t *cnf, cls_t cls)
{
    rel_t r = relSec(cnf->r, clsToRel(cls));

    // se la relazione equivalente è la stessa, la clausola è insignificante
    if (r == cnf->r)
        return 0;

    cnf->r = r;
    cnf->cl[cnf->sz++] = cls;

    return 1;
}

ccls_t cclsInit(void)
{
    return 0U;
}

cls_t cclsToCls(ccls_t ccl)
{
    uint64_t msb = (ccl & ~CCLS_NEGMAX);

    // Uso i 4 MSB come shift per il letterale positivo, e semplicemente copio quelli negativi
    return _pdep_u64(msb ? 1ULL << ((msb >> CCLS_NEGLIT) - 1ULL) : 0, CCLS_MASKPOS) |
           _pdep_u64(ccl & CCLS_NEGMAX, CCLS_MASKNEG);
}

ccnf_t ccnfInit(void)
{
    return (ccnf_t){0};
}

int ccnfNext(ccnf_t *cnf, int max_depth)
{
    int depth, sz = cnf->sz;
    ccls_t *ccl = cnf->ccl;

    // proviamo ad incrementare le clausole a ritroso
    for (depth = sz - 1; depth >= 0; --depth)
    {
        ccls_t next = (ccls_t)(ccl[depth] + 1U);

        if (next < MAX_CCNF_SZ)
        {
            ccl[depth] = next;
            break;
        }
    }
    // se le clausole non sono tutte piene...
    if (depth >= 0)
    {
        // tutte le successive devono essere settate a prec + 1
        for (++depth; depth < sz; ++depth)
            ccl[depth] = (ccls_t)(ccl[depth - 1] + 1U);
        return 1;
    }

    // sono tutte piene, provo ad inserire una nuova clausola
    if (++cnf->sz > max_depth)
        return 0;

    ccl[0] = 1;
    for (depth = 1; depth < cnf->sz; ++depth)
        ccl[depth] = (ccls_t)(ccl[depth - 1] + 1U);

    return 1;
}

rel_t ccnfToRel(ccnf_t cnf)
{
    rel_t r = REL_ALL;

    // devo intersecare (CNF) le relazioni equivalenti di ogni clausola
    for (int i = 0; i < cnf.sz; ++i)
        r = relSec(r, clsToRel(cclsToCls(cnf.ccl[i])));

    return r;
}
