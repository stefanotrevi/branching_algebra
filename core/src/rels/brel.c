#include "rels/brel.h"
#include "rels/bprel.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"

#undef _NODISCARD
#define _NODISCARD

#include <ctype.h>
#include <string.h>

brel_t brInit(void)
{
    return BR0;
}

rel_t brelToRel(brel_t br)
{
    return BRTOR(br);
}

static rel_t brelAtoRelLoop(brel_t *br, int n)
{
    rel_t r = REL0;

    for (int i = 0; i < n; ++i)
        r = relPut(r, br[i]);

    return r;
}

rel_t brelAtoRel(brel_t *br, int n)
{
    switch (n)
    {
    case 0: return REL0;
    case 1: return BRTOR(br[0]);
    case 2: return BRADD2(br[0], br[1]);
    case 3: return BRADD3(br[0], br[1], br[2]);
    case 4: return BRADD4(br[0], br[1], br[2], br[3]);
    case 5: return BRADD5(br[0], br[1], br[2], br[3], br[4]);
    case 6: return BRADD6(br[0], br[1], br[2], br[3], br[4], br[5]);
    case 7: return BRADD7(br[0], br[1], br[2], br[3], br[4], br[5], br[6]);
    case 8: return BRADD8(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7]);
    case 9: return BRADD9(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8]);
    case 10: return BRADD10(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9]);

    case 11:
        return BRADD11(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                       br[10]);
    case 12:
        return BRADD12(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11]);
    case 13:
        return BRADD13(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12]);
    case 14:
        return BRADD14(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12], br[13]);
    case 15:
        return BRADD15(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12], br[13], br[14]);
    case 16:
        return BRADD16(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12], br[13], br[14], br[15]);
    case 17:
        return BRADD17(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12], br[13], br[14], br[15], br[16]);
    case 18:
        return BRADD18(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9], br[10],
                       br[11], br[12], br[13], br[14], br[15], br[16], br[17]);
    default: return brelAtoRelLoop(br, n);
    }
}

rel_t brelLesser(brel_t br)
{
    switch (br)
    {
    case B: return BRTOR(B);
    case M: return BRADD2(B, M);
    case O: return BRADD3(B, M, O);
    case FI: return BRADD4(B, M, O, FI);
    case DI: return BRADD5(B, M, O, FI, DI);
    case S: return BRADD4(B, M, O, S);
    case EQ: return BRADD6(B, M, O, S, FI, EQ);
    case SI: return BRADD8(B, M, O, S, FI, EQ, DI, SI);
    case D: return BRADD5(B, M, O, S, D);
    case F: return BRADD8(B, M, O, S, D, FI, EQ, F);
    case OI: return BRADD11(B, M, O, S, D, FI, EQ, F, DI, SI, OI);
    case MI: return BRADD12(B, M, O, S, D, FI, EQ, F, DI, SI, OI, MI);
    case BI: return BRADD13(B, M, O, S, D, FI, EQ, F, DI, SI, OI, MI, BI);
    case IM: return BRADD6(B, M, O, FI, DI, IM);
    case IB: return BRADD7(B, M, O, FI, DI, IM, IB);
    case IE: return BRADD10(B, M, O, FI, DI, IM, S, EQ, SI, IE);
    case IMI: return BRADD14(B, M, O, FI, DI, IM, S, EQ, SI, IE, D, F, OI, IMI);
    case IBI: return BRADD17(B, M, O, FI, DI, IM, S, EQ, SI, IE, D, F, OI, IMI, MI, BI, IBI);
    case UN: return REL_ALL;
    default: return REL0;
    }
}

rel_t brelBigger(brel_t br)
{
    return relPut(relInv(brelLesser(br)), br);
}

int brelDim(brel_t br)
{
    switch (br)
    {
    case UN: return 4;
    case IE: return 1;
    case IB: return 3;
    case IBI: return 3;
    case IM: return 3;
    case IMI: return 3;
    case EQ: return 0;
    case B: return 2;
    case BI: return 2;
    case D: return 2;
    case DI: return 2;
    case F: return 1;
    case FI: return 1;
    case M: return 1;
    case MI: return 1;
    case O: return 2;
    case OI: return 2;
    case S: return 1;
    case SI: return 1;
    default: return -1;
    }
}

brel_t brelConv(brel_t br)
{
    switch (br)
    {
    case UN: return UN;
    case IE: return IE;
    case IB: return IBI;
    case IBI: return IB;
    case IM: return IMI;
    case IMI: return IM;
    case EQ: return EQ;
    case B: return BI;
    case BI: return B;
    case D: return DI;
    case DI: return D;
    case F: return FI;
    case FI: return F;
    case M: return MI;
    case MI: return M;
    case O: return OI;
    case OI: return O;
    case S: return SI;
    case SI: return S;
    default: return BREL_N;
    }
}

brel_t brelPath(brel_t ij, brel_t jk, brel_t ik)
{
    return relHas(brelComp(ij, jk), ik) ? ik : BREL_N;
}

const char *brelToStr(brel_t br)
{
    switch (br)
    {
    case UN: return "u";
    case IE: return "ie";
    case IB: return "ib";
    case IBI: return "ibi";
    case IM: return "im";
    case IMI: return "imi";
    case EQ: return "e";
    case B: return "b";
    case BI: return "bi";
    case D: return "d";
    case DI: return "di";
    case F: return "f";
    case FI: return "fi";
    case M: return "m";
    case MI: return "mi";
    case O: return "o";
    case OI: return "oi";
    case S: return "s";
    case SI: return "si";
    default: return "?";
    }
}

brel_t brelFromStr(const char *str)
{
    const char *last = str;

    while (isalpha(*last))
        ++last;
    --last;
    if (*last == 'i' || *last == '\'')
        switch (str[0])
        {
        case 'b': return BI;
        case 'd': return DI;
        case 'f': return FI;
        case 'i':
            switch (str[1])
            {
            case 'b': return IBI;
            case 'm': return IMI;
            default: return BREL_N;
            }
        case 'm': return MI;
        case 'o': return OI;
        case 's': return SI;
        default: return BREL_N;
        }

    switch (str[0])
    {
    case 'b': return B;
    case 'd': return D;
    case 'e': return EQ;
    case 'f': return F;
    case 'i':
        switch (str[1])
        {
        case 'b': return IB;
        case 'e': return IE;
        case 'm': return IM;
        default: return BREL_N;
        }
    case 'm': return M;
    case 'o': return O;
    case 's': return S;
    case 'u': return UN;
    default: return BREL_N;
    }
}

cprel_t brelTocprel(brel_t br)
{
    /* Trasformiamo le relazioni tra intervalli in relazioni fra estremi.
     * Dobbiamo esplicitare le relazioni fra tutte le coppie di punti. */
    static const cprel_t mapping[] = {
        [UN] = BPREL4_TO_CPREL(PUN, PUN, PUN, PUN), //
        [IE] = BPREL4_TO_CPREL(PEQ, PB, PBI, PUN),  //
        [IB] = BPREL4_TO_CPREL(PB, PB, PUN, PUN),   [IBI] = BPREL4_TO_CPREL(PBI, PUN, PBI, PUN),
        [IM] = BPREL4_TO_CPREL(PB, PB, PBI, PUN),   [IMI] = BPREL4_TO_CPREL(PBI, PB, PBI, PUN),
        [EQ] = BPREL4_TO_CPREL(PEQ, PB, PBI, PEQ), //
        [B] = BPREL4_TO_CPREL(PB, PB, PB, PB),      [BI] = BPREL4_TO_CPREL(PBI, PBI, PBI, PBI),
        [D] = BPREL4_TO_CPREL(PBI, PB, PBI, PB),    [DI] = BPREL4_TO_CPREL(PB, PB, PBI, PBI),
        [F] = BPREL4_TO_CPREL(PBI, PB, PBI, PEQ),   [FI] = BPREL4_TO_CPREL(PB, PB, PBI, PEQ),
        [M] = BPREL4_TO_CPREL(PB, PB, PEQ, PB),     [MI] = BPREL4_TO_CPREL(PBI, PEQ, PBI, PBI),
        [O] = BPREL4_TO_CPREL(PB, PB, PBI, PB),     [OI] = BPREL4_TO_CPREL(PBI, PB, PBI, PBI),
        [S] = BPREL4_TO_CPREL(PEQ, PB, PBI, PB),    [SI] = BPREL4_TO_CPREL(PEQ, PB, PBI, PBI),
    };

    return mapping[br];
}

rel_t brelComp(brel_t br1, brel_t br2)
{
    static const rel_t brel_comptable[BREL_N][BREL_N] = // clang-format off
    { 
        [BI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRTOR(BI),
            [F] = BRTOR(BI),
            [D] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [IBI] = BRTOR(IBI),
            [IMI] = BRTOR(IBI),
            [SI] = BRTOR(BI),
            [EQ] = BRTOR(BI),
            [S] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [IE] = BRTOR(IBI),
            [DI] = BRTOR(BI),
            [FI] = BRTOR(BI),
            [O] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [IM] = BRTOR(IBI),
            [M] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [B] = REL_ALL,
            [UN] = BRTOR(UN),
            [IB] = BRTOR(UN),
        },
        [MI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRTOR(BI),
            [F] = BRTOR(MI),
            [D] = BRADD4(IMI, OI, D, F),
            [IBI] = BRTOR(IBI),
            [IMI] = BRTOR(IBI),
            [SI] = BRTOR(BI),
            [EQ] = BRTOR(MI),
            [S] = BRADD4(IMI, OI, D, F),
            [IE] = BRTOR(IBI),
            [DI] = BRTOR(BI),
            [FI] = BRTOR(MI),
            [O] = BRADD4(IMI, OI, D, F),
            [IM] = BRTOR(IBI),
            [M] = BRADD4(S, EQ, IE, SI),
            [B] = BRADD7(B, IM, DI, M, IB, O, FI),
            [UN] = BRTOR(UN),
            [IB] = BRTOR(UN),
        },
        [OI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRADD3(BI, OI, MI),
            [F] = BRTOR(OI),
            [D] = BRADD4(IMI, OI, D, F),
            [IBI] = BRTOR(IBI),
            [IMI] = BRADD2(IBI, IMI),
            [SI] = BRADD3(BI, OI, MI),
            [EQ] = BRTOR(OI),
            [S] = BRADD4(IMI, OI, D, F),
            [IE] = BRADD2(IBI, IMI),
            [DI] = BRADD5(MI, BI, DI, OI, SI),
            [FI] = BRADD3(DI, OI, SI),
            [O] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
            [IM] = BRADD4(IMI, IE, IBI, IM),
            [M] = BRADD4(IM, DI, O, FI),
            [B] = BRADD7(B, IM, DI, M, IB, O, FI),
            [UN] = BRTOR(UN),
            [IB] = BRADD2(IB, UN),
        },
        [F] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRADD3(BI, OI, MI),
            [F] = BRTOR(F),
            [D] = BRTOR(D),
            [IBI] = BRTOR(IBI),
            [IMI] = BRADD2(IBI, IMI),
            [SI] = BRADD3(BI, OI, MI),
            [EQ] = BRTOR(F),
            [S] = BRTOR(D),
            [IE] = BRADD2(IBI, IMI),
            [DI] = BRADD5(MI, BI, DI, OI, SI),
            [FI] = BRADD3(FI, EQ, F),
            [O] = BRADD3(S, D, O),
            [IM] = BRADD4(IMI, IE, IBI, IM),
            [M] = BRTOR(M),
            [B] = BRTOR(B),
            [UN] = BRTOR(UN),
            [IB] = BRADD2(IB, UN),
        },
        [D] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRADD5(D, MI, BI, F, OI),
            [F] = BRTOR(D),
            [D] = BRTOR(D),
            [IBI] = BRTOR(IBI),
            [IMI] = BRADD3(D, IBI, IMI),
            [SI] = BRADD5(D, MI, BI, F, OI),
            [EQ] = BRTOR(D),
            [S] = BRTOR(D),
            [IE] = BRADD3(D, IBI, IMI),
            [DI] = BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI),
            [FI] = BRADD5(D, B, S, M, O),
            [O] = BRADD5(D, B, S, M, O),
            [IM] = BRADD9(D, IBI, B, IE, IM, S, M, IMI, O),
            [M] = BRTOR(B),
            [B] = BRTOR(B),
            [UN] = BRTOR(UN),
            [IB] = BRADD3(IB, B, UN),
        },
        [SI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(MI),
            [OI] = BRTOR(OI),
            [F] = BRTOR(OI),
            [D] = BRADD4(IMI, OI, D, F),
            [IBI] = BRTOR(IBI),
            [IMI] = BRTOR(IMI),
            [SI] = BRTOR(SI),
            [EQ] = BRTOR(SI),
            [S] = BRADD4(S, EQ, IE, SI),
            [IE] = BRTOR(IE),
            [DI] = BRTOR(DI),
            [FI] = BRTOR(DI),
            [O] = BRADD4(IM, DI, O, FI),
            [IM] = BRTOR(IM),
            [M] = BRADD4(IM, DI, O, FI),
            [B] = BRADD7(B, IM, DI, M, IB, O, FI),
            [UN] = BRTOR(UN),
            [IB] = BRTOR(IB),
        },
        [EQ] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(MI),
            [OI] = BRTOR(OI),
            [F] = BRTOR(F),
            [D] = BRTOR(D),
            [IBI] = BRTOR(IBI),
            [IMI] = BRTOR(IMI),
            [SI] = BRTOR(SI),
            [EQ] = BRTOR(EQ),
            [S] = BRTOR(S),
            [IE] = BRTOR(IE),
            [DI] = BRTOR(DI),
            [FI] = BRTOR(FI),
            [O] = BRTOR(O),
            [IM] = BRTOR(IM),
            [M] = BRTOR(M),
            [B] = BRTOR(B),
            [UN] = BRTOR(UN),
            [IB] = BRTOR(IB),
        },
        [S] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(MI),
            [OI] = BRADD3(OI, D, F),
            [F] = BRTOR(D),
            [D] = BRTOR(D),
            [IBI] = BRTOR(IBI),
            [IMI] = BRADD2(D, IMI),
            [SI] = BRADD3(S, EQ, SI),
            [EQ] = BRTOR(S),
            [S] = BRTOR(S),
            [IE] = BRADD2(S, IE),
            [DI] = BRADD5(B, DI, M, O, FI),
            [FI] = BRADD3(M, B, O),
            [O] = BRADD3(M, B, O),
            [IM] = BRADD4(M, B, O, IM),
            [M] = BRTOR(B),
            [B] = BRTOR(B),
            [UN] = BRTOR(UN),
            [IB] = BRADD2(IB, B),
        },
        [DI] = {
            [BI] = BRADD5(MI, BI, DI, OI, SI),
            [MI] = BRADD3(DI, OI, SI),
            [OI] = BRADD3(DI, OI, SI),
            [F] = BRADD3(DI, OI, SI),
            [D] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
            [IBI] = BRADD4(IMI, IE, IBI, IM),
            [IMI] = BRADD3(IM, IE, IMI),
            [SI] = BRTOR(DI),
            [EQ] = BRTOR(DI),
            [S] = BRADD4(IM, DI, O, FI),
            [IE] = BRTOR(IM),
            [DI] = BRTOR(DI),
            [FI] = BRTOR(DI),
            [O] = BRADD4(IM, DI, O, FI),
            [IM] = BRTOR(IM),
            [M] = BRADD4(IM, DI, O, FI),
            [B] = BRADD7(B, IM, DI, M, IB, O, FI),
            [UN] = BRADD2(IB, UN),
            [IB] = BRTOR(IB),
        },
        [FI] = {
            [BI] = BRADD5(MI, BI, DI, OI, SI),
            [MI] = BRADD3(DI, OI, SI),
            [OI] = BRADD3(DI, OI, SI),
            [F] = BRADD3(FI, EQ, F),
            [D] = BRADD3(S, D, O),
            [IBI] = BRADD4(IMI, IE, IBI, IM),
            [IMI] = BRADD3(IM, IE, IMI),
            [SI] = BRTOR(DI),
            [EQ] = BRTOR(FI),
            [S] = BRTOR(O),
            [IE] = BRTOR(IM),
            [DI] = BRTOR(DI),
            [FI] = BRTOR(FI),
            [O] = BRTOR(O),
            [IM] = BRTOR(IM),
            [M] = BRTOR(M),
            [B] = BRTOR(B),
            [UN] = BRADD2(IB, UN),
            [IB] = BRTOR(IB),
        },
        [O] = {
            [BI] = BRADD5(MI, BI, DI, OI, SI),
            [MI] = BRADD3(DI, OI, SI),
            [OI] = BRADD9(D, F, S, DI, OI, SI, EQ, O, FI),
            [F] = BRADD3(S, D, O),
            [D] = BRADD3(S, D, O),
            [IBI] = BRADD4(IMI, IE, IBI, IM),
            [IMI] = BRADD6(D, IE, IM, S, IMI, O),
            [SI] = BRADD3(DI, O, FI),
            [EQ] = BRTOR(O),
            [S] = BRTOR(O),
            [IE] = BRADD2(O, IM),
            [DI] = BRADD5(B, DI, M, O, FI),
            [FI] = BRADD3(M, B, O),
            [O] = BRADD3(M, B, O),
            [IM] = BRADD4(M, B, O, IM),
            [M] = BRTOR(B),
            [B] = BRTOR(B),
            [UN] = BRADD2(IB, UN),
            [IB] = BRADD2(IB, B),
        },
        [M] = {
            [BI] = BRADD5(MI, BI, DI, OI, SI),
            [MI] = BRADD3(FI, EQ, F),
            [OI] = BRADD3(S, D, O),
            [F] = BRADD3(S, D, O),
            [D] = BRADD3(S, D, O),
            [IBI] = BRADD4(IMI, IE, IBI, IM),
            [IMI] = BRADD3(S, D, O),
            [SI] = BRTOR(M),
            [EQ] = BRTOR(M),
            [S] = BRTOR(M),
            [IE] = BRTOR(M),
            [DI] = BRTOR(B),
            [FI] = BRTOR(B),
            [O] = BRTOR(B),
            [IM] = BRTOR(B),
            [M] = BRTOR(B),
            [B] = BRTOR(B),
            [UN] = BRADD2(IB, UN),
            [IB] = BRTOR(B),
        },
        [B] = {
            [BI] = BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI),
            [MI] = BRADD5(D, B, S, M, O),
            [OI] = BRADD5(D, B, S, M, O),
            [F] = BRADD5(D, B, S, M, O),
            [D] = BRADD5(D, B, S, M, O),
            [IBI] = BRADD9(D, IBI, B, IE, IM, S, M, IMI, O),
            [IMI] = BRADD5(D, B, S, M, O),
            [SI] = BRTOR(B),
            [EQ] = BRTOR(B),
            [S] = BRTOR(B),
            [IE] = BRTOR(B),
            [DI] = BRTOR(B),
            [FI] = BRTOR(B),
            [O] = BRTOR(B),
            [IM] = BRTOR(B),
            [M] = BRTOR(B),
            [B] = BRTOR(B),
            [UN] = BRADD3(IB, B, UN),
            [IB] = BRTOR(B),
        },
        [IMI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [OI] = BRADD4(IMI, BI, OI, MI),
            [IMI] = BRADD5(D, IBI, F, OI, IMI),
            [F] = BRTOR(IMI),
            [D] = BRTOR(IMI),
            [IBI] = BRTOR(IBI),
            [SI] = BRADD4(IMI, BI, OI, MI),
            [IE] = BRADD5(D, IBI, F, OI, IMI),
            [EQ] = BRTOR(IMI),
            [S] = BRTOR(IMI),
            [DI] = BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB),
            [IM] = BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI),
            [FI] = BRADD4(IM, IB, IE, IMI),
            [O] = BRADD4(IM, IB, IE, IMI),
            [IB] = BRADD8(B, IM, DI, M, UN, IB, O, FI),
            [M] = BRTOR(IB),
            [B] = BRTOR(IB),
            [UN] = BRTOR(UN),
        },
        [IE] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(MI),
            [IMI] = BRADD4(IMI, OI, D, F),
            [OI] = BRADD2(OI, IMI),
            [F] = BRTOR(IMI),
            [D] = BRTOR(IMI),
            [IBI] = BRTOR(IBI),
            [IE] = BRADD4(S, EQ, IE, SI),
            [SI] = BRADD2(IE, SI),
            [EQ] = BRTOR(IE),
            [S] = BRTOR(IE),
            [IB] = BRADD7(B, IM, DI, M, IB, O, FI),
            [DI] = BRADD3(IB, DI, IM),
            [FI] = BRADD2(IB, IM),
            [O] = BRADD2(IB, IM),
            [IM] = BRADD5(IM, DI, IB, O, FI),
            [M] = BRTOR(IB),
            [B] = BRTOR(IB),
            [UN] = BRTOR(UN),
        },
        [UN] = {
            [BI] = BRADD3(BI, UN, IBI),
            [IBI] = BRADD8(D, IBI, MI, BI, F, OI, UN, IMI),
            [MI] = BRADD2(UN, IBI),
            [OI] = BRADD2(UN, IBI),
            [F] = BRADD2(UN, IBI),
            [D] = BRADD2(UN, IBI),
            [IMI] = BRADD2(UN, IBI),
            [UN] = REL_ALL,
            [SI] = BRTOR(UN),
            [EQ] = BRTOR(UN),
            [S] = BRTOR(UN),
            [IE] = BRTOR(UN),
            [DI] = BRTOR(UN),
            [FI] = BRTOR(UN),
            [O] = BRTOR(UN),
            [IM] = BRTOR(UN),
            [M] = BRTOR(UN),
            [B] = BRTOR(UN),
            [IB] = BRTOR(UN),
        },
        [IB] = {
            [BI] = BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB),
            [IBI] = BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI),
            [MI] = BRADD4(IM, IB, IE, IMI),
            [OI] = BRADD4(IM, IB, IE, IMI),
            [F] = BRADD4(IM, IB, IE, IMI),
            [D] = BRADD4(IM, IB, IE, IMI),
            [IMI] = BRADD4(IM, IB, IE, IMI),
            [UN] = BRADD8(B, IM, DI, M, UN, IB, O, FI),
            [SI] = BRTOR(IB),
            [EQ] = BRTOR(IB),
            [S] = BRTOR(IB),
            [IE] = BRTOR(IB),
            [DI] = BRTOR(IB),
            [FI] = BRTOR(IB),
            [O] = BRTOR(IB),
            [IM] = BRTOR(IB),
            [M] = BRTOR(IB),
            [B] = BRTOR(IB),
            [IB] = BRTOR(IB),
        },
        [IBI] = {
            [BI] = BRTOR(BI),
            [MI] = BRTOR(BI),
            [IMI] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [OI] = BRADD2(BI, IBI),
            [F] = BRTOR(IBI),
            [D] = BRTOR(IBI),
            [IBI] = BRTOR(IBI),
            [IE] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
            [SI] = BRADD2(BI, IBI),
            [EQ] = BRTOR(IBI),
            [S] = BRTOR(IBI),
            [IB] = REL_ALL,
            [DI] = BRADD3(BI, UN, IBI),
            [FI] = BRADD2(UN, IBI),
            [O] = BRADD2(UN, IBI),
            [IM] = BRADD8(D, IBI, MI, BI, F, OI, UN, IMI),
            [M] = BRTOR(UN),
            [B] = BRTOR(UN),
            [UN] = BRTOR(UN),
        },
        [IM] = {
            [BI] = BRADD5(MI, BI, DI, OI, SI),
            [MI] = BRADD3(DI, OI, SI),
            [IMI] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
            [OI] = BRADD6(IE, IM, DI, OI, SI, IMI),
            [F] = BRADD3(IM, IE, IMI),
            [D] = BRADD3(IM, IE, IMI),
            [IBI] = BRADD4(IMI, IE, IBI, IM),
            [IE] = BRADD4(IM, DI, O, FI),
            [SI] = BRADD2(DI, IM),
            [EQ] = BRTOR(IM),
            [S] = BRTOR(IM),
            [IB] = BRADD7(B, IM, DI, M, IB, O, FI),
            [DI] = BRADD3(IB, DI, IM),
            [FI] = BRADD2(IB, IM),
            [O] = BRADD2(IB, IM),
            [IM] = BRADD5(IM, DI, IB, O, FI),
            [M] = BRTOR(IB),
            [B] = BRTOR(IB),
            [UN] = BRADD2(IB, UN),
        },
    }; // clang-format on

    return brel_comptable[br1][br2];
}
