#include "rels/bprel.h"
#include "lits/lit.h"
#include "rels/brel.h"
#include "rels/prel.h"
#include "rels/rel.h"

#include <stdio.h>
#include <string.h>

#ifndef _WIN32
    #include <strings.h>
#else
    #define strcasecmp _stricmp
#endif

char *bprelToLitStr(char *s, bprel_t br, pcouples_t pc)
{
    // La traduzione si basa sull'uso degli assiomi ORD e dell'esplicitazione
    switch (br)
    {
    case PEQ: // (P = Q) -> (P = Q) & (P <= Q) & (Q <= P) & (P lin Q)
        sprintf(s, "!%c & %c & %c & %c", litChar(pc, U), litChar(pc, E), litChar(pc, LE), litChar(pc, LEI));
        break;
    case PB: // (P < Q) -> (P <= Q) & (P != Q) & (P </ Q) & (P lin Q)
        sprintf(s, "!%c & !%c & %c & %c", litChar(pc, U), litChar(pc, E), litChar(pc, LE), litChar(pc, LU));
        break;
    case PBI: // (P > Q) -> (Q <= P) & (P != Q) & (Q </ P) & (P lin Q)
        sprintf(s, "!%c & !%c & %c & %c", litChar(pc, U), litChar(pc, E), litChar(pc, LEI), litChar(pc, LUI));
        break;
    case PUN: // (P || Q) -> (P || Q) & (P != Q) & (P </ Q) & (Q </ P)
        sprintf(s, "%c & !%c & %c & %c", litChar(pc, U), litChar(pc, E), litChar(pc, LU), litChar(pc, LUI));
        break;
    default:
        break;
    }
    return s;
}

prel_t bprelToPrel(bprel_t bpr)
{
    return (prel_t)(1U << bpr);
}

prel_t bprelComp(bprel_t bp1, bprel_t bp2)
{
    switch (bp1)
    {
    case PUN:
        switch (bp2)
        {
        case PUN: return PREL_ALL;
        case PEQ: return BPRTOPR(PUN);
        case PB: return BPRTOPR(PUN);
        case PBI: return BPRADD2(PBI, PUN);
        default: return BPR0;
        }
    case PEQ:
        switch (bp2)
        {
        case PUN: return BPRTOPR(PUN);
        case PEQ: return BPRTOPR(PEQ);
        case PB: return BPRTOPR(PB);
        case PBI: return BPRTOPR(PBI);
        default: return BPR0;
        }
    case PB:
        switch (bp2)
        {
        case PUN: return BPRADD2(PB, PUN);
        case PEQ: return BPRTOPR(PB);
        case PB: return BPRTOPR(PB);
        case PBI: return BPRADD3(PEQ, PB, PBI);
        default: return BPR0;
        }
    case PBI:
        switch (bp2)
        {
        case PUN: return BPRTOPR(PUN);
        case PEQ: return BPRTOPR(PBI);
        case PB: return PREL_ALL;
        case PBI: return BPRTOPR(PBI);
        default: return BPR0;
        }
    default: return BPR0;
    }
}

char *bprelToStr(bprel_t br)
{
    switch (br)
    {
    case PUN: return "||";
    case PEQ: return "=";
    case PB: return "<";
    case PBI: return ">";
    default: return "?";
    }
}

bprel_t bprelConv(bprel_t br)
{
    switch (br)
    {
    case PUN: return PUN;
    case PEQ: return PEQ;
    case PB: return PBI;
    case PBI: return PB;
    default: return BPREL_N;
    }
}

bprel_t bprelPath(bprel_t ij, bprel_t jk, bprel_t ik)
{
    return prelHas(bprelComp(ij, jk), ik) ? ik : BPREL_N;
}

rel_t bprelToRel(bprel_t bp, pcouples_t pc)
{
    switch (bp)
    {
    case PEQ:
        switch (pc)
        {
        case AMBM: return RELSEC(BRADD4(IE, EQ, S, SI), REL_ALL);
        case AMBP: return BRTOR(MI);
        case APBM: return BRTOR(M);
        case APBP: return BRADD3(EQ, F, FI);
        default: return REL0;
        }
    case PB:
        switch (pc)
        {
        case AMBM: return RELSEC(BRADD7(IB, IM, B, DI, FI, M, O), REL_ALL);
        case AMBP: return RELSEC(BRADD15(IE, IB, IM, IMI, EQ, B, D, DI, F, FI, M, O, OI, S, SI), REL_ALL);
        case APBM: return BRTOR(B);
        case APBP: return BRADD5(B, D, M, O, S);
        default: return REL0;
        }
    case PBI: // Posso ritornare la conversa dopo aver scambiato le coppie e la relazione
        return relConv(bprelToRel(PB, pcFlip(pc)));
    case PUN:
        switch (pc)
        {
        case AMBM: return RELSEC(BRTOR(UN), REL_ALL);
        case AMBP: return RELSEC(BRADD2(UN, IBI), REL_ALL);
        case APBM: return RELSEC(BRADD2(UN, IB), REL_ALL);
        case APBP: return RELSEC(BRADD6(UN, IE, IB, IBI, IM, IMI), REL_ALL);
        default: return REL0;
        }
    default: return REL0;
    }
}

#define compare_and_ret(x)              \
    if (!strcasecmp(str, STRING(x))) \
    return x

pcouples_t pcFromStr(const char *str)
{
    compare_and_ret(AMBM);
    compare_and_ret(AMBP);
    compare_and_ret(APBM);
    compare_and_ret(APBP);

    return COUPLES_N;
}

char *pcToStr(pcouples_t pc)
{
    switch (pc)
    {
    case AMBM: return "A-B-";
    case AMBP: return "A-B+";
    case APBM: return "A+B-";
    case APBP: return "A+B+";
    default: return "?";
    }
}
