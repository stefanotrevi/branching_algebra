#include "rels/irel.hpp"

#include <iostream>

int main()
{
    Ibrel br{Ispecs::BI};
    Irel r{br};
    Irel s{Ispecs::BI};

    std::cout << r.size() << '\n';

    return 0;
}
