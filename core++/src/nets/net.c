#include "nets/net.h"
#include "lut.h"
#include "nets/bnet.h"
#include "nets/queue.h"
#include "rels/brel.h"
#include "rels/rel.h"
#include "sets/set.h"

#include <ctype.h>
#include X86INTRIN_H
#include <math.h>
#include <stdlib.h>
#include <string.h>

// Alloca una rete senza inizializzarla
static net_t netAlloc(int size)
{
    return (net_t){.size = size, .rels = malloc(size * size * sizeof(rel_t))};
}

net_t netInit(int size)
{
    net_t net = netAlloc(size);

    // di base sono tutte REL_ALL, tranne ovviamenete quelle per uno stesso intervallo
    for (int i = 0; i < net.size; ++i)
    {
        netSet(net, i, i, BRTOR(EQ));
        for (int j = i + 1; j < net.size; ++j)
            netPut(net, j, i, REL_ALL);
    }

    return net;
}

net_t netInitCopy(net_t src)
{
    net_t dst = netAlloc(src.size);

    memcpy(dst.rels, src.rels, src.size * src.size * sizeof *src.rels);

    return dst;
}

static int *intShuffle(int *v, int sz)
{
    for (int i = 0; i < sz; ++i)
    {
        int j = i + rand() / (RAND_MAX / (sz - i) + 1);
        int t = v[i];

        v[i] = v[j];
        v[j] = t;
    }

    return v;
}

net_t netInitRand(int size, int net_dens, int rel_dens, const rel_t *allow, int allow_n)
{
    net_t net = netAlloc(size);
    int sz = net.size * net.size;
    int *idx = malloc(sz * sizeof *idx);

    net_dens = net_dens >= 100 ? sz : sz * net_dens / 100;

    for (int i = 0; i < sz; ++i)
        idx[i] = i;

    intShuffle(idx, sz);

    for (int i = 0; i < net_dens; ++i)
        net.rels[idx[i]] = allow ? relSample(allow, allow_n) : relRand(rel_dens);

    for (int i = net_dens; i < sz; ++i)
        net.rels[idx[i]] = REL_ALL;

    for (int i = 0; i < net.size; ++i) // rendo coerente la rete
    {
        netSet(net, i, i, BRTOR(EQ));
        for (int j = i + 1; j < net.size; ++j)
            netSet(net, j, i, netGet(net, i, j));
    }

    free(idx);

    return net;
}

void netDel(net_t net)
{
    free(net.rels);
}

rel_t netGet(net_t net, int i, int j)
{
    return net.rels[i * net.size + j];
}

void netSet(net_t net, int i, int j, rel_t r)
{
    net.rels[i * net.size + j] = r;
}

void netPut(net_t net, int i, int j, rel_t r)
{
    netSet(net, i, j, r);
    netSet(net, j, i, relConv(r));
}

static void putRelated(queue_t *q, int size, int i, int j)
{
    for (int k = 0; k < size; ++k)
        if (k != i && k != j)
        {
            queuePut(q, i);
            queuePut(q, j);
            queuePut(q, k);

            queuePut(q, k);
            queuePut(q, i);
            queuePut(q, j);
        }
}

int netPC(net_t net)
{
    queue_t q = queueInit();

    for (int i = 0; i < net.size; ++i)
        for (int j = 0; j < net.size; ++j)
            if (i != j)
                putRelated(&q, net.size, i, j);

    while (!queueIsEmpty(q))
    {
        int i = queueGet(&q), j = queueGet(&q), k = queueGet(&q);
        rel_t path = relPath(netGet(net, i, j), netGet(net, j, k), netGet(net, i, k));

        if (relIsEmpty(path))
        {
            netSet(net, i, k, REL0);
            netSet(net, k, i, REL0);
            queueDel(q);

            return 0;
        }

        rel_t conv = relSec(relConv(path), netGet(net, k, i));
        if (path != netGet(net, i, k) || conv != netGet(net, k, i))
        {
            netSet(net, i, k, path);
            netSet(net, k, i, conv);
            putRelated(&q, net.size, i, k);
        }
    }
    queueDel(q);

    return netIsCons(net);
}

static uint64_t round_to_pow2(uint64_t x)
{
    unsigned long y = 0;

    _BitScanReverse64(&y, x);

    return 1U << (y + 1);
}

int netPC2(net_t net)
{
    /* 
     * assicuro una potenza di 2, così posso usare & al posto di % (molto più veloce!)
     * lo spazio usato quindi è di 2^ceil(log(6n^3)) anziché 6n^3 come per PC1, ma questo non 
     * dovrebbe essere un problema. 
     * In più usiamo un altro array di supporto, un po' più piccolo, per non riempire la coda 
     * inutilmente. 
     * Usiamo interi a 64bit per ridurre la possibilità di overflow (trattandosi di dimensioni 
     * cubiche, andremmo in overflow con net.size > ~1000)
    */
    int size = net.size;
    uint64_t csize = size * size * size;
    uint64_t q_dim = round_to_pow2(6 * csize);
    uint64_t q_rem = q_dim - 1;
    uint64_t top = 0, bot = 0;
    uint8_t *in_q = malloc(csize * sizeof *in_q);
    int *q = malloc(q_dim * sizeof *q);

    memset(in_q, 1, csize * sizeof *in_q);

    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            if (i != j)
                for (int k = 0; k < size; ++k)
                    if (k != i && k != j)
                    {
                        q[top++] = i;
                        q[top++] = j;
                        q[top++] = k;

                        q[top++] = k;
                        q[top++] = i;
                        q[top++] = j;
                    }

    while (bot != top)
    {
        int i = q[bot = (bot + 1) & q_rem];
        int j = q[bot = (bot + 1) & q_rem];
        int k = q[bot = (bot + 1) & q_rem];
        rel_t path = relPath(netGet(net, i, j), netGet(net, j, k), netGet(net, i, k));
        rel_t conv;
        if (relIsEmpty(path))
        {
            netSet(net, i, k, REL0);
            netSet(net, k, i, REL0);
            break;
        }

        in_q[i * size + j * size + k] = 0;
        conv = relSec(relConv(path), netGet(net, k, i));

        if (path != netGet(net, i, k) || conv != netGet(net, k, i))
        {
            netSet(net, i, k, path);
            netSet(net, k, i, conv);
            for (int l = 0, stride = i * size + k * size; l < size; ++l)
                if (l != i && l != k)
                {
                    if (!in_q[stride + l])
                    {
                        in_q[stride + l] = 1;
                        q[top = (top + 1) & q_rem] = i;
                        q[top = (top + 1) & q_rem] = k;
                        q[top = (top + 1) & q_rem] = l;
                    }
                    if (!in_q[l * size + i * size + k])
                    {
                        in_q[l * size + i * size + k] = 1;
                        q[top = (top + 1) & q_rem] = l;
                        q[top = (top + 1) & q_rem] = i;
                        q[top = (top + 1) & q_rem] = k;
                    }
                }
        }
    }

    free(q);
    free(in_q);

    return netIsCons(net);
}

static int netFCRec(net_t net, int i, int j, partflag_t eur)
{
    net_t nnet;
    int next_i = i, next_j = j + 1;
    int cons = 0;
    rel_t r_ij = netGet(net, i, j);
    const rel_t *part = relPartition(r_ij, eur);

    if (!netPC2(net))
        return 0;
    else if (i >= net.size || j >= net.size)
        return 1;

    if (next_j >= net.size)
        next_j = ++next_i + 1;


    nnet = netInit(net.size);

    if (part)
        for (int p = 0; part[p] && !cons; ++p)
        {
            // istanziamo, e vediamo se il modello funziona
            netCopy(nnet, net);
            netPut(nnet, i, j, part[p]);
            cons = netFCRec(nnet, next_i, next_j, eur);
        }
    else
        for (brel_t br = BR0; br < BREL_N && !cons; ++br)
            if (relHas(r_ij, br))
            {
                netCopy(nnet, net);
                netPut(nnet, i, j, brelToRel(br));
                cons = netFCRec(nnet, next_i, next_j, eur);
            }

    netCopy(net, nnet);
    netDel(nnet);

    return cons;
}

int netFC(net_t net, partflag_t eur)
{
    return netFCRec(net, 0, 1, eur);
}

int netAdd(net_t net, int i, int j, rel_t r)
{
    rel_t old = netGet(net, i, j);

    if (r != old)
    {
        queue_t q = queueInit();

        netPut(net, i, j, r);
        queuePut(&q, i);
        queuePut(&q, j);

        while (!queueIsEmpty(q))
        {
            i = queueGet(&q);
            j = queueGet(&q);
            for (int k = 0; k < net.size; ++k)
                if (k != i && k != j)
                {
                    rel_t path = relPath(netGet(net, i, j), netGet(net, j, k), netGet(net, j, k));

                    if (relIsEmpty(path))
                        return 0;
                    if (path != netGet(net, i, k))
                    {
                        queuePut(&q, i);
                        queuePut(&q, k);
                        netPut(net, i, k, path);
                    }

                    path = relPath(netGet(net, k, i), netGet(net, i, j), netGet(net, k, j));

                    if (relIsEmpty(path))
                        return 0;
                    if (netGet(net, k, j) != path)
                    {
                        queuePut(&q, k);
                        queuePut(&q, j);
                        netPut(net, k, j, path);
                    }
                }
        }
        queueDel(q);
    }

    return 1;
}

net_t netPrint(FILE *f, net_t net, int hex)
{

    for (int i = 0; i < net.size; ++i)
        fprintf(f, "\t%d", i);

    for (int i = 0; i < net.size; ++i)
    {
        fprintf(f, "\n%d", i);
        if (hex)
            for (int j = 0; j < net.size; ++j)
                fprintf(f, "\t%05X", netGet(net, i, j));
        else
            for (int j = 0; j < net.size; ++j)
            {
                char buf[64];

                relToStr(buf, netGet(net, i, j));
                fprintf(f, "\t%s", buf);
            }
    }
    putc('\n', f);
    putc('\n', f);

    return net;
}

int netEquals(net_t n1, net_t n2)
{
    return n1.rels == n2.rels ||
           (n1.size == n2.size && !memcmp(n1.rels, n2.rels, n1.size * n1.size * sizeof *n1.rels));
}

void netCopy(net_t dst, net_t src)
{
    memcpy(dst.rels, src.rels, src.size * src.size * sizeof *src.rels);
}

int netIsHorn(net_t net)
{
    int sz = net.size * net.size;
    rel_t *r = net.rels;

    for (int i = 0; i < sz; ++i)
        if (!relIsHorn(r[i]))
            return 0;

    return 1;
}

int netHas(net_t net, rel_t r)
{
    int sz = net.size * net.size;
    rel_t *rels = net.rels;

    for (int i = 0; i < sz; ++i)
        if (rels[i] == r)
            return 1;

    return 0;
}

int netIsCons(net_t net)
{
    return !netHas(net, REL0);
}

double netGetDens(net_t net)
{
    int sz = net.size * net.size, dens = 0;

    for (int i = 0; i < sz; ++i)
        if (!relIsAll(net.rels[i]))
            ++dens;

    return dens * 100. / sz;
}

bnet_t netModel(net_t net)
{
    bnet_t model = bnetInit(net.size);

    for (int i = 0; i < net.size; ++i)
        for (int j = i + 1; j < net.size; ++j)
        {
            brel_t max = BREL_N;

            for (brel_t br = BR0; br < BREL_N; ++br)
                if (relHas(netGet(net, i, j), br) && brelDim(br) > brelDim(max))
                    max = br;

            bnetPut(model, i, j, max);
        }

    return model;
}
