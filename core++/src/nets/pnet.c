#include "nets/pnet.h"
#include "nets/bpnet.h"
#include "nets/queue.h"
#include "rels/bprel.h"
#include "rels/prel.h"

#include X86INTRIN_H
#include <math.h>
#include <stdlib.h>
#include <string.h>

// Alloca una rete senza inizializzarla
static pnet_t pnetAlloc(int size)
{
    return (pnet_t){.size = size, .rels = malloc(size * size * sizeof(prel_t))};
}

pnet_t pnetInit(int size)
{
    pnet_t pnet = pnetAlloc(size);

    // di base sono tutte PREL_ALL, tranne ovviamepnete quelle per uno stesso intervallo
    for (int i = 0; i < pnet.size; ++i)
    {
        pnetSet(pnet, i, i, BPRTOPR(PEQ));
        for (int j = i + 1; j < pnet.size; ++j)
            pnetPut(pnet, j, i, PREL_ALL);
    }

    return pnet;
}

pnet_t pnetInitRand(int size, int dens)
{
    pnet_t pnet = pnetAlloc(size);
    int sz = pnet.size * pnet.size;
    int d = dens >= 100 ? sz : sz * dens / 100;
    int *idx = malloc(sz * sizeof *idx);

    for (int i = 0; i < sz; ++i)
        idx[i] = i;
    for (int i = 0; i < sz; ++i) // mescolo gli indici
    {
        int j = i + rand() / (RAND_MAX / (sz - i) + 1), t = idx[i];

        idx[i] = idx[j];
        idx[j] = t;
    }

    for (int i = 0; i < d; ++i) // riempio d elementi con relazioni a caso
        pnet.rels[idx[i]] = prelRand();
    for (int i = d; i < sz; ++i) // e gli altri con ALL
        pnet.rels[idx[i]] = PREL_ALL;

    free(idx);
    for (int i = 0; i < pnet.size; ++i) // rendo coerente la rete
    {
        pnetSet(pnet, i, i, BPRTOPR(PEQ));
        for (int j = i + 1; j < pnet.size; ++j)
            pnetSet(pnet, j, i, pnetGet(pnet, i, j));
    }

    return pnet;
}

void pnetDel(pnet_t pnet)
{
    free(pnet.rels);
}

prel_t pnetGet(pnet_t pnet, int i, int j)
{
    return pnet.rels[i * pnet.size + j];
}

pnet_t pnetSet(pnet_t pnet, int i, int j, prel_t pr)
{
    pnet.rels[pnet.size * i + j] = pr;

    return pnet;
}

pnet_t pnetPut(pnet_t pnet, int i, int j, prel_t pr)
{
    pnet.rels[pnet.size * i + j] = pr;
    pnet.rels[pnet.size * j + i] = prelConv(pr);

    return pnet;
}

static queue_t *putRelated(queue_t *q, int size, int i, int j)
{
    for (int k = 0; k < size; ++k)
        if (k != i && k != j)
        {
            queuePut(q, i);
            queuePut(q, j);
            queuePut(q, k);

            queuePut(q, k);
            queuePut(q, i);
            queuePut(q, j);
        }

    return q;
}

int pnetPC(pnet_t pnet)
{
    queue_t q = queueInit();

    for (int i = 0; i < pnet.size; ++i)
        for (int j = 0; j < pnet.size; ++j)
            if (i != j)
                putRelated(&q, pnet.size, i, j);

    while (!queueIsEmpty(q))
    {
        int i = queueGet(&q), j = queueGet(&q), k = queueGet(&q);
        prel_t path = prelPath(pnetGet(pnet, i, j), pnetGet(pnet, j, k), pnetGet(pnet, i, k));

        if (prelIsEmpty(path))
        {
            pnetSet(pnet, i, k, PREL0);
            pnetSet(pnet, k, i, PREL0);
            queueDel(q);

            return 0;
        }

        prel_t conv = prelSec(prelConv(path), pnetGet(pnet, k, i));
        if (path != pnetGet(pnet, i, k) || conv != pnetGet(pnet, k, i))
        {
            pnetSet(pnet, i, k, path);
            pnetSet(pnet, k, i, conv);
            putRelated(&q, pnet.size, i, k);
        }
    }
    queueDel(q);

    return pnetIsCons(pnet);
}

int pnetPC2(pnet_t pnet)
{
    int size = pnet.size;
    int64_t csize = size * size * size;
    int64_t q_dim = 1U << (_bit_scan_reverse(6 * csize) + 1);
    int *q = malloc(q_dim * sizeof *q);
    int64_t q_rem = q_dim - 1;
    int64_t top = -1, bot = -1;
    uint8_t *in_q = malloc(csize * sizeof *in_q);

    memset(in_q, 1, csize * sizeof *in_q);

    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            if (i != j)
                for (int k = 0; k < size; ++k)
                    if (k != i && k != j)
                    {
                        q[++top] = i;
                        q[++top] = j;
                        q[++top] = k;

                        q[++top] = k;
                        q[++top] = i;
                        q[++top] = j;
                    }

    while (bot != top)
    {
        int i = q[bot = (bot + 1) & q_rem];
        int j = q[bot = (bot + 1) & q_rem];
        int k = q[bot = (bot + 1) & q_rem];
        prel_t path = prelPath(pnetGet(pnet, i, j), pnetGet(pnet, j, k), pnetGet(pnet, i, k));
        prel_t conv;

        if (prelIsEmpty(path))
        {
            pnetSet(pnet, i, k, PREL0);
            pnetSet(pnet, k, i, PREL0);
            free(q);
            free(in_q);

            return 0;
        }

        in_q[i * size + j * size + k] = 0;
        conv = prelSec(prelConv(path), pnetGet(pnet, k, i));

        if (path != pnetGet(pnet, i, k) || conv != pnetGet(pnet, k, i))
        {
            pnetSet(pnet, i, k, path);
            pnetSet(pnet, k, i, conv);
            for (int l = 0, stride = i * size + k * size; l < size; ++l)
                if (l != i && l != k)
                {
                    if (!in_q[stride + l])
                    {
                        in_q[stride + l] = 1;
                        q[top = (top + 1) & q_rem] = i;
                        q[top = (top + 1) & q_rem] = k;
                        q[top = (top + 1) & q_rem] = l;
                    }
                    if (!in_q[l * size + i * size + k])
                    {
                        in_q[l * size + i * size + k] = 1;
                        q[top = (top + 1) & q_rem] = l;
                        q[top = (top + 1) & q_rem] = i;
                        q[top = (top + 1) & q_rem] = k;
                    }
                }
        }
    }

    free(q);
    free(in_q);

    return pnetIsCons(pnet);
}

int pnetAdd(pnet_t pnet, int i, int j, prel_t r)
{
    prel_t old = pnetGet(pnet, i, j);

    if (r != old)
    {
        queue_t q = queueInit();

        pnetPut(pnet, i, j, r);
        queuePut(&q, i);
        queuePut(&q, j);

        while (!queueIsEmpty(q))
        {
            i = queueGet(&q);
            j = queueGet(&q);
            for (int k = 0; k < pnet.size; ++k)
                if (k != i && k != j)
                {
                    prel_t path = prelPath(pnetGet(pnet, i, j), pnetGet(pnet, j, k), pnetGet(pnet, j, k));

                    if (prelIsEmpty(path))
                        return 0;
                    if (path != pnetGet(pnet, i, k))
                    {
                        queuePut(&q, i);
                        queuePut(&q, k);
                        pnetPut(pnet, i, k, path);
                    }

                    path = prelPath(pnetGet(pnet, k, i), pnetGet(pnet, i, j), pnetGet(pnet, k, j));

                    if (prelIsEmpty(path))
                        return 0;
                    if (pnetGet(pnet, k, j) != path)
                    {
                        queuePut(&q, k);
                        queuePut(&q, j);
                        pnetPut(pnet, k, j, path);
                    }
                }
        }
        queueDel(q);
    }

    return 1;
}

pnet_t pnetPrint(FILE *f, pnet_t pnet, int hex)
{

    putc('\n', f);

    for (int i = 0; i < pnet.size; ++i)
        fprintf(f, "\t%d", i);

    for (int i = 0; i < pnet.size; ++i)
    {
        fprintf(f, "\n%d", i);
        for (int j = 0; j < pnet.size; ++j)
            if (hex)
                fprintf(f, "\t%05X", pnetGet(pnet, i, j));
            else
                fprintf(f, "\t%s", prelToChar(pnetGet(pnet, i, j)));
    }
    putc('\n', f);
    putc('\n', f);

    return pnet;
}

int pnetEquals(pnet_t n1, pnet_t n2)
{
    return n1.rels == n2.rels || // confronto per riferimento, poi per valore
           (n1.size == n2.size && !memcmp(n1.rels, n2.rels, n1.size * n1.size * sizeof *n1.rels));
}

pnet_t pnetCopy(pnet_t dst, pnet_t src)
{
    if (!dst.rels)
        dst = pnetAlloc(src.size);

    memcpy(dst.rels, src.rels, src.size * src.size * sizeof *src.rels);

    return dst;
}

int pnetHas(pnet_t pnet, prel_t r)
{
    int sz = pnet.size * pnet.size;
    prel_t *rels = pnet.rels;

    for (int i = 0; i < sz; ++i)
        if (rels[i] == r)
            return 1;

    return 0;
}

int pnetIsCons(pnet_t pnet)
{
    return !pnetHas(pnet, REL0);
}

double pnetDens(pnet_t pnet)
{
    int sz = pnet.size * pnet.size, dens = 0;

    for (int i = 0; i < sz; ++i)
        if (!prelIsAll(pnet.rels[i]))
            ++dens;

    return dens * 100. / sz;
}

bpnet_t pnetModel(pnet_t pnet, const int *idx)
{
    int n = pnet.size;
    bpnet_t model = bpnetInit(n);

    for (int i = 0, m = n - 1; i < n; ++i)
    {
        int k0 = i * m - 1 - ((i * i + i) >> 1);

        for (int j = i + 1; j < n; ++j)
        {
            bprel_t chose = BPREL_N;
            int k = k0 + j;
            int rel_i = idx[k];

            for (bprel_t br = BPR0; br < BPREL_N && rel_i >= 0; ++br)
                if (prelHas(pnetGet(pnet, i, j), br))
                {
                    chose = br;
                    --rel_i;
                }

            bpnetPut(model, i, j, chose);
        }
    }

    return model;
}

/*
pnet_t *pnetDecompose(pnet_t pnet)
{
    pnet_t *parts = malloc(pnet.size * sizeof *parts);
    int n = 0;

    parts[0] = pnetInit(pnet.size);

    for (int i = 0, j = 0; i < pnet.size && pnetGet(pnet, i, j); )
        parts[0];

    free(parts);
}

int pnetBranchCons(pnet_t pnet)
{
    pnet_t pnet_g = pnetInit(pnet.size);

    for (int i = 0; i < pnet.size; ++i)
        for (int j = 0; j < pnet.size; ++j)
            if (!prelHas(pnetGet(pnet, i, j), PUN))
                pnetSet(pnet_g, i, j, pnetGet(pnet, i, j));
}
*/
