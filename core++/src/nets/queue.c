#include "nets/queue.h"

#include <stdlib.h>

queue_t queueInit(void)
{
    queue_t q = {.tail = calloc(1, sizeof *q.tail)};

    q.head = q.tail;

    return q;
}

void queueDel(queue_t q)
{
    while (q.head)
    {
        qnode_t *t = q.head;

        q.head = q.head->next;
        free(t);
    }
}

queue_t *queuePut(queue_t *q, int i)
{
    qnode_t *t = q->tail;

    t->i = i;
    t->next = calloc(1, sizeof *t);
    q->tail = t->next;

    return q;
}

int queueGet(queue_t *q)
{
    qnode_t *h = q->head;
    int i = h->i;

    q->head = h->next;
    free(h);

    return i;
}

int queueIsEmpty(queue_t q)
{
    return q.head == q.tail;
}
