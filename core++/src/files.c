#include "files.h"

#ifdef _WIN32
    #include <Windows.h>
    #include <direct.h>
    #include <io.h>
#else
    #include <sys/mman.h>
    #include <sys/sendfile.h>
    #include <sys/stat.h>
    #include <unistd.h>
#endif

#ifdef MPI_ENABLE
    #include <mpi.h>
#endif

#include <fcntl.h>
#include <stdlib.h>

int fileExists(char *fname)
{
    return access(fname, 0) == 0;
}

void *fileMap(char *fname, size_t size)
{
    void *map = NULL;

#ifdef _WIN32
    HANDLE f_map;
    HANDLE f = CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                          FILE_ATTRIBUTE_READONLY, NULL);

    if (f == INVALID_HANDLE_VALUE)
        return NULL;

    f_map = CreateFileMapping(f, NULL, PAGE_READONLY, 0, 0, NULL);
    map = MapViewOfFile(f_map, FILE_MAP_READ, 0, 0, size);

    CloseHandle(f);
    CloseHandle(f_map);
#else
    int fd = open(fname, O_RDONLY);

    if (fd < 0)
        return NULL;

    map = mmap(map, size, PROT_READ, MAP_PRIVATE, fd, 0);
    close(fd);
#endif

    return map;
}

void fileUnmap(void *map, size_t size)
{
#ifdef _WIN32
    (void)size;
    UnmapViewOfFile(map);
#else
    munmap(map, size);
#endif
}

long fileCopy(FILE *src, FILE *dst)
{
#ifdef _WIN32
    int n = 0;

    while (!feof(src))
    {
        char buf[BUFSIZ];

        n += fread(buf, sizeof *buf, sizeof buf, src);
        fwrite(buf, sizeof *buf, n, dst);
    }

    return n;
#else // sendfile è più efficente, esegue la copia in kernel-space
    int in = fileno(src), out = fileno(dst);
    off_t len = lseek(in, 0, SEEK_END);

    lseek(in, 0, SEEK_SET);

    return sendfile(out, in, NULL, len);
#endif
}

int fileMkdir(char *dname)
{
#ifdef _WIN32
    return mkdir(dname);
#else
    return mkdir(dname, 0755);
#endif
}
