#include "generator.h"
#include "algs/palg.h"
#include "algs/ralg.h"
#include "files.h"
#include "lits/cnf.h"
#include "rels/bprel.h"
#include "rels/brel.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"
#include "sets/cuRset.cuh"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <stdlib.h>
#include <string.h>

#ifdef MPI_ENABLE
    #include <mpi.h>
#endif

#define CHECK_MAXIMALITY

static void genHornWorker(rset_t *set)
{
    for (ccls_t ccl = cclsInit(); ccl < CCLS_MAX; ++ccl)
        rsetIns(set, clsToRel(cclsToCls(ccl)));
}

void genHorn(int rank, int size)
{
    rset_t horns = rsetInit();
    FILE *out, *out_bin;
    int n, is_maximal = 0;
    static const rel_t p_c0 = BRADD5(B, M, O, FI, DI);
    static const rel_t p_c1 = BRADD5(B, M, O, S, D);

    if (!rank)
        puts("Genero relazioni ORD-Horn...");

    genHornWorker(&horns);
    n = rsetClose(&horns, CLOSE_SEC);
    // stampo i risultati in ordine, e salvo l'insieme in un file
    if (!rank)
    {
        out = fopen(FILE_OUT, "wt");
        out_bin = fopen(BFILE_OUT, "wb");

        rsetPrint(out, horns);
        rsetWrite(out_bin, horns);
        fprintf(out, "Relazioni generate: %d\n", n);
        printf("%d\nCerco le sovra-algebre contenenti le relazioni corner...\n", n);

        fclose(out_bin);
    }
#ifdef CHECK_MAXIMALITY
    if (rsetGet(horns, p_c0) && rsetGet(horns, p_c1))
        is_maximal = ralgIsMaximal(horns, rank, size);
    else if (!rank)
        printf("L'insieme non contiene le relazioni corner necessarie\n");
#endif

    if (!rank)
    {
        printf("L'algebra %s massimale\n", is_maximal ? "è" : "non è");
        fprintf(out, "L'algebra %s massimale\n", is_maximal ? "è" : "non è");
        fclose(out);
    }

    rsetDel(horns);
}

void genNear(void)
{
    rset_t found = rsetInit();
    FILE *out = fopen(FILE_OUT, "wt");

    if (!out)
    {
        perror(FILE_OUT);
        exit(EXIT_FAILURE);
    }

    for (brel_t b1 = BR0; b1 < BREL_N; ++b1)     // estremo inferiore
        for (brel_t b2 = BR0; b2 < BREL_N; ++b2) // estremo superiore
        {
            rel_t r = relSec(brelBigger(b1), brelLesser(b2));
            // Se non ciclo, trovo esattamente CONVEX
            rsetIns(&found, r);
            // Inserisco l'intervallo intero o con i "buchi neri"
            /*  for (uint i = 0; i < MAX_BLACK; ++i)
                set[relSub(r, blackGet(i))] = 1U;*/
        }

    rsetPrint(out, found);
    fprintf(out, "Totale relazioni: %d\n", found.num);
    printf("Totale relazioni: %d\n", found.num);

    rsetDel(found);
    fclose(out);
}

void genPoint(void)
{
    FILE *out = fopen(FILE_OUT, "wt");
    rset_t set = rsetInit();

    printf("Genero le relazioni puntuali...\n");

    for (cprel_t cr = CPREL0; cr < CPREL_N; ++cr)
        rsetIns(&set, cprelToRel(cr));

    rsetPrint(out, set);

    fprintf(out, "Numero di relazioni: %d\n", set.num);
    printf("Numero di relazioni: %d\n", set.num);

    rsetDel(set);
    fclose(out);
}

void genTHornClauses(void)
{
    FILE *out = fopen(FILE_OUT, "wt");
    rset_t set = rsetInit();

    printf("Generating Horn clauses...\n");
    for (ccls_t ccl = cclsInit(); ccl < CCLS_MAX; ++ccl)
    {
        cls_t cls = cclsToCls(ccl);
        rel_t r = clsToRel(cls);

        if (!rsetGet(set, r))
        {
            char buf[128];

            rsetIns(&set, r);
            clsToStr(buf, cls);
            fprintf(out, "%s = \n", buf);
            relPrint(out, r);
        }
    }

    printf("Generated %d clauses\n", set.num);

    rsetDel(set);
    fclose(out);
}

void genDisPoint(palgname_t fst, palgname_t snd, int rank, int size)
{
    FILE *out;
    rset_t set = ralgInitByDisj(fst, snd);
    int ret = 0;
    int check = 7734;
    char fname[FILE_NAMELEN];

    sprintf(fname, FILE_OUT "_%d", check);

    if (!rank)
    {
        out = fopen(fname, "wt");
        printf("Generata l'algebra %s ⨯ %s^* (%d relazioni)\n", palgName(fst), palgName(snd),
               set.num);

        printf("Calcolo della chiusura:\n");
    }

    if (!rank)
        rsetClose(&set, CLOSE_PRINT | CLOSE_FULL);
    else
        rsetClose(&set, CLOSE_FULL);

    if (!rank)
    {
        rsetPrint(out, set);
        fprintf(out, "Totale relazioni: %d\n", set.num);
        printf("Totale relazioni: %d\n", set.num);
    }

    ret = ralgIsMaximal(set, rank, size);

    if (!rank)
    {
        fflush(out);
        // rimetto insieme i file
        for (int i = 0; i < size; ++i)
        {
            char pname[FILE_NAMELEN];
            FILE *part;

            sprintf(pname, "%s_%d", fname, i);
            part = fopen(fname, "rt");
            fileCopy(part, out);

            fclose(part);
        }

        if (ret > 0)
            puts("L'algebra è massimale (se è trattabile)");
        else if (ret == 0)
            puts("L'algebra non è massimale");
        else
            puts("L'algebra non è trattabile (stando agli insiemi corner)");

        fclose(out);
    }

    rsetDel(set);
}

void genRalg(ralgname_t alg, int rank, int size)
{
    FILE *out;
    rset_t set = ralgInit(alg);
    int ret = 0;
    char fname[FILE_NAMELEN] = {0};

    sprintf(fname, FILE_OUT "_%d", set.num);

    if (!rank)
    {
        out = fopen(fname, "wt");

        rsetPrint(out, set);
        fprintf(out, "Totale relazioni: %d\n", set.num);
        printf("Generata l'algebra %s (%d relazioni)\n", ralgName(alg), set.num);
    }

    ret = ralgIsMaximal(set, rank, size);

    if (!rank)
    {
        fflush(out);
        // rimetto insieme i file
        for (int i = 0; i < size; ++i)
        {
            char partname[FILE_NAMELEN] = {0};
            FILE *part;

            sprintf(partname, "%s_%d", fname, i);
            if ((part = fopen(partname, "rt")))
            {
                fileCopy(part, out);

                fclose(part);
            }
        }

        if (ret > 0)
            puts("L'algebra è massimale (se è trattabile)");
        else if (ret == 0)
            puts("L'algebra non è massimale");
        else
            puts("L'algebra non è trattabile (stando agli insiemi corner)");

        fclose(out);
    }

    rsetDel(set);
}

void genDisjRelations(void)
{
    rset_t set = rsetInit();
    rset_t set_noconv = rsetInit();
    FILE *out = fopen(FILE_OUT, "wt");

    // non serve considerare la relazione vuota né la relazione tutto
    for (prel_t pr1 = PREL0 + 1; pr1 < PREL_N - 1; ++pr1)
        for (prel_t pr2 = PREL0 + 1; pr2 < PREL_N - 1; ++pr2)
        {
            int buf_n = 0;
            char buf[4096] = {0};

            for (pcouples_t pc1 = AMBM; pc1 < COUPLES_N; ++pc1)
                for (pcouples_t pc2 = AMBM; pc2 < COUPLES_N; ++pc2)
                {
                    rel_t r = relAdd(prelToRel(pr1, pc1), prelToRel(pr2, pc2));

                    if (!relIsPoint(r) && !rsetGet(set, r))
                    {
                        rsetIns(&set, r);
                        rsetIns(&set_noconv, r);
                        rsetIns(&set, relConv(r));

                        buf_n += sprintf(buf + buf_n, "(I%c ",
                                         pc1 == AMBM || pc1 == AMBP ? '-' : '+');
                        buf_n += prelToStr(buf + buf_n, pr1);
                        buf_n += sprintf(buf + buf_n, " J%c) v (I%c ",
                                         pc1 == AMBM || pc1 == APBM ? '-' : '+',
                                         pc2 == AMBM || pc2 == AMBP ? '-' : '+');

                        buf_n += prelToStr(buf + buf_n, pr2);
                        buf_n += sprintf(buf + buf_n,
                                         " J%c): %u: ", pc2 == AMBM || pc2 == APBM ? '-' : '+', r);
                        buf_n += relToStr(buf + buf_n, r);
                        buf[buf_n++] = '\n';
                        buf[buf_n++] = '\n';
                    }
                }
            if (buf_n)
                fputs(buf, out);
        }

    rsetPrint(out, set_noconv);
    fprintf(out, "Relazioni generate: %d\n", set_noconv.num);
    printf("Relazioni generate: %d\n", set_noconv.num);

    rsetDel(set);
    rsetDel(set_noconv);
}
