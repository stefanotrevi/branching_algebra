#include "lut.h"
#include "algs/ralg.h"
#include "files.h"
#include "rels/brel.h"
#include "rels/prel.h"
#include "rels/rel.h"
#include "sets/cuRset.cuh"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static rel_t *comp_map;   // mappa per la composizione di relazioni
static prel_t *pcomp_map; // mappa per la composizione di relazioni puntuali

static uint8_t *horn_map;       // mappa relazioni Horn
static rel_t horn_rels[HORN_N]; // lista relazioni horn

static uint8_t *point_map;        // mappa relazioni puntizzabili
static rel_t point_rels[POINT_N]; // lista relazioni puntizzabili

static rel_t *horn_part_map; // mappa per la partitione in relazioni Horn

#define SIZE_COMPMAP (COMPLUT_N * sizeof *comp_map)
#define SIZE_PCOMPMAP (PREL_N * PREL_N * sizeof *pcomp_map)

#define SIZE_HORNMAP (REL_N * sizeof *horn_map)
#define SIZE_POINTMAP (REL_N * sizeof *point_map)

#define SIZE_HORNPART (REL_N * BREL_N * sizeof *horn_part_map)

int lutInit(int rank)
{
    int grank; // rank GPU

    // Directory di risorse e di output
    if (!fileExists(FILE_DIR_RES))
        if (fileMkdir(FILE_DIR_RES))
        {
            perror(FILE_DIR_RES);
            exit(EXIT_FAILURE);
        }
    if (!fileExists(FILE_DIR_OUT))
        if (fileMkdir(FILE_DIR_OUT))
        {
            perror(FILE_DIR_OUT);
            exit(EXIT_FAILURE);
        }

    // E' importante caricare prima le tabelle di composizione!
    if (!(comp_map = fileMap(FILE_COMPMAP, SIZE_COMPMAP)))
    {
        puts("Missing interval composition table, building it now...");
        lutBuildComp();
        comp_map = fileMap(FILE_COMPMAP, SIZE_COMPMAP);
    }
    grank = cuLutInit(rank, comp_map);

    if (!(pcomp_map = fileMap(FILE_PCOMPMAP, SIZE_PCOMPMAP)))
    {
        puts("Missing point composition table, building it now...");
        lutBuildPcomp();
        pcomp_map = fileMap(FILE_PCOMPMAP, SIZE_PCOMPMAP);
    }

    if (!(horn_map = fileMap(FILE_HORNMAP, SIZE_HORNMAP)))
    {
        puts("Missing horn relations table, building it now...");
        lutBuildHorn();
        horn_map = fileMap(FILE_HORNMAP, SIZE_HORNMAP);
    }
    int n = 0;

    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (horn_map[r])
            horn_rels[n++] = r;

    if (!(point_map = fileMap(FILE_POINTMAP, SIZE_POINTMAP)))
    {
        puts("Missing pointisable relations table, building it now...");
        lutBuildPoint();
        point_map = fileMap(FILE_POINTMAP, SIZE_POINTMAP);
    }
    n = 0;

    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (point_map[r])
            point_rels[n++] = r;


    if (!(horn_part_map = fileMap(FILE_HORNPART, SIZE_HORNPART)))
    {
        puts("Missing relation partitioning table, building it now...");
        lutBuildHornPartition();
        horn_part_map = fileMap(FILE_HORNPART, SIZE_HORNPART);
    }

    return grank;
}

void lutFinish(void)
{
    fileUnmap(horn_map, SIZE_HORNMAP);
    fileUnmap(point_map, SIZE_POINTMAP);
    fileUnmap(comp_map, SIZE_COMPMAP);
    fileUnmap(pcomp_map, SIZE_PCOMPMAP);
    fileUnmap(horn_part_map, SIZE_HORNPART);

    cuLutFinish();
}

rel_t lutHornGet(unsigned i)
{
    return i < HORN_N ? horn_rels[i] : REL_ALL;
}

int lutHornMapGet(rel_t r)
{
    return horn_map[r];
}

rel_t lutPointGet(unsigned i)
{
    return i < POINT_N ? point_rels[i] : REL_ALL;
}

int lutPointMapGet(rel_t r)
{
    return point_map[r];
}

#ifdef BA
rel_t compGet(rel_t r1, rel_t r2, lutidx_t lidx, uint ld)
{
    // ld è la leading dimension, cioè COMPLUT_L o COMPLUT_R
    return comp_map[lidx + r1 * ld + r2];
}
#else
// in IA ho abbastanza memoria per farci stare tutto
rel_t compGet(rel_t r1, rel_t r2)
{
    // Sono contigui in memoria, devo normalizzare!
    return comp_map[r1 * (REL_NIA >> BABR_N) + (r2 >> BABR_N)];
}
#endif
prel_t pCompGet(prel_t r1, prel_t r2)
{
    return pcomp_map[r1 * PREL_N + r2];
}

const rel_t *hornPartGet(rel_t r)
{
    return horn_part_map + r * BREL_N;
}

int lutBuildHorn(void)
{
    FILE *out = fopen(FILE_HORNMAP, "wb");
    rset_t set;

    if (!out)
    {
        perror(FILE_HORNMAP);
        return -1;
    }

    // gamma_e x delta_e* = Horn
    set = ralgInitByDisj(PALG_GAMMA_E, PALG_DELTA_E);
    rsetWrite(out, set);

    rsetDel(set);
    fclose(out);

    return 0;
}

int lutBuildPoint(void)
{
    FILE *out = fopen(FILE_POINTMAP, "wb");
    rset_t set;

    if (!out)
    {
        perror(FILE_POINTMAP);
        return -1;
    }

    // gamma_a = BPA
    set = ralgInitByPalg(PALG_GAMMA_A);
    rsetWrite(out, set);

    rsetDel(set);
    fclose(out);

    return 0;
}

int lutBuildPcomp(void)
{
    FILE *out = fopen(FILE_PCOMPMAP, "wb");

    if (!out)
    {
        perror(FILE_PCOMPMAP);
        return -1;
    }

    for (prel_t r = PREL0; r < PREL_N; ++r)
        for (prel_t s = PREL0; s < PREL_N; ++s)
        {
            prel_t comp = prelComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    fclose(out);

    return 0;
}

int lutBuildComp(void)
{
    FILE *out = fopen(FILE_COMPMAP, "wb");

    if (!out)
    {
        perror(FILE_COMPMAP);
        return -1;
    }

#ifdef BA // Per BA dobbiamo partizionare la LUT in quattro pezzi

    for (rel_t r = REL0; r < COMPLUT_NR; ++r)
        for (rel_t s = REL0; s < COMPLUT_NR; ++s)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    for (rel_t r = REL0; r < COMPLUT_NR; ++r)
        for (rel_t s = REL0; s < REL_N; s += COMPLUT_NR)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    for (rel_t r = REL0; r < REL_N; r += COMPLUT_NR)
        for (rel_t s = REL0; s < COMPLUT_NR; ++s)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    for (rel_t r = REL0; r < REL_N; r += COMPLUT_NR)
        for (rel_t s = REL0; s < REL_N; s += COMPLUT_NR)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

#else // Per IA costruiamo tutta la LUT
    fwriteComp(out, REL0, REL_N, REL0, REL_N);
#endif

    fclose(out);

    return 0;
}

int lutBuildHornPartition(void)
{
    /* 
     * Questa funzione sostanzialmente calcola la chiusura per unione, solo che deve anche
     * tenere traccia di chi si è unito con chi, quindi non può essere fatto dalla GPU che è
     * imprevedibile come ordine di esecuzione. Otteniamo il partizionamento ottimo perché:
     * 1) Le relazioni Horn sono partizionate con sé stesse
     * 2) Attraverso un "peso" si garantisce che solo unioni di peso minore possono sovrascrivere
     *  un partizionamento già fatto.
    */
    rel_t *pset = calloc(REL_N * BREL_N, sizeof *pset); // insieme delle partizioni
    rel_t *todo = malloc(REL_N * sizeof *todo);         // relazioni da processare
    rel_t *done = malloc(REL_N * sizeof *done);         // relazioni processate
    rel_t *todo_new = malloc(REL_N * sizeof *todo_new); // nuove relazioni processate
    uint *wset = calloc(REL_N, sizeof *wset);           // insieme dei pesi
    uint todo_n = HORN_N, done_n = 0;
    FILE *out = fopen(FILE_HORNPART, "wb");

    memcpy(todo, horn_rels, HORN_N * sizeof *horn_rels);

    for (uint i = 0; i < HORN_N; ++i) // le relazioni Horn hanno come partizione sé stesse
    {
        pset[horn_rels[i] * BREL_N] = horn_rels[i];
        wset[horn_rels[i]] = 1;
    }

    while (todo_n) // ripeto finché non raggiungo un punto fisso
    {
        uint new_n = 0;

        for (uint i = 0; i < done_n; ++i)
            for (uint j = 0; j < todo_n; ++j)
            {
                rel_t rs = relAdd(done[i], todo[j]);
                uint wsum = wset[done[i]] + wset[todo[j]];

                // se non ho un partizionamento o se il peso è magggiore
                if (!wset[rs] || wset[rs] > wsum)
                {
                    uint k = 0;

                    // Mettiamo dentro gli elementi delle due relazioni
                    for (uint l = 0; pset[done[i] * BREL_N + l]; ++l, ++k)
                        pset[rs * BREL_N + k] = pset[done[i] * BREL_N + l];
                    for (uint l = 0; pset[todo[j] * BREL_N + l]; ++l, ++k)
                        pset[rs * BREL_N + k] = pset[todo[j] * BREL_N + l];

                    if (!wset[rs])
                        todo_new[new_n++] = rs;
                    wset[rs] = wsum;
                }
            }

        memcpy(done + done_n, todo, todo_n * sizeof *todo);
        done_n += todo_n;
        memcpy(todo, todo_new, new_n * sizeof *todo_new);
        todo_n = new_n;
    }

    fwrite(pset, sizeof *pset, REL_N * BREL_N, out);

    free(pset);
    free(wset);
    free(todo);
    free(done);
    free(todo_new);

    return 0;
}
