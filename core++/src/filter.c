#include "filter.h"
#include "files.h"
#include "lits/cnf.h"
#include "lits/dnf.h"
#include "rels/rel.h"
#include "sets/rset.h"

#ifndef _WIN32
    #include <pthread.h>
    #include <unistd.h>
    #include <wait.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char *eqRead(FILE *f, char *buff)
{
    int i = 0, c;

    if (!buff || (c = fgetc(f)) == EOF)
        return NULL;

    do
        buff[i++] = (char)c;
    while ((c = fgetc(f)) != EOF && c != ';');

    if (c != EOF)
        buff[i++] = (char)c;

    buff[i] = '\0';
    if (fgetc(f) == '\r') // salto il 1° a capo
        fgetc(f);
    if (fgetc(f) == '\r') // salto il 2° a capo
        fgetc(f);

    return buff;
}

static int skipws(char *str)
{
    if (!str)
        return 0;

    int i = 0;

    while (str[i] && (str[i] == '\n' || str[i] == ' ' || str[i] == '\r'))
        ++i;

    return i;
}

static int skipchar(char *str, char c)
{
    if (!str)
        return 0;

    int i = 0;

    while (str[i] && str[i] != c)
        ++i;

    if (!str[i])
        return i;
    ++i;
    return i + skipws(str + i);
}

// Verifica se l'equazione ORD in input è di Horn o meno
static int eqIsHorn(char *eq)
{
    /* Una formula ORD è di Horn se il numero di disgiunti positivi, in ogni
     * clausola, è al massimo 1. La formula in input è "invertita": gli &
     * e gli | sono scambiati, così come i letterali positivi/negativi. */
    cnf_t cnf = cnfInit();
    rel_t additional = REL_ALL;

    eq += skipchar(eq, '=');
    while (*eq != ';') // ciclo sulle clausole
    {
        cls_t cls = clsInit();

        eq += skipchar(eq, '(');
        while (*eq != ')') // ciclo sui letterali nella clausola
        {
            // Inserisco il letterale
            if (*eq == '!')
                cls = clsInsChar(cls, *(eq += skipchar(eq, '!')), L_POS);
            else
                cls = clsInsChar(cls, *eq, L_NEG);
            // passo al prosimo letterale
            while (*eq != '&' && *eq != ')')
                ++eq;
            if (*eq == '&')
                ++eq;
            eq += skipws(eq);
        }
        // Se la clausola ha più di un letterale positivo, la "salvo per più tardi"
        if (clsPosCount(cls) > 1)
            additional = relSec(additional, clsToRel(cls));
        else
            cnfIns(&cnf, cls);
        eq += skipchar(eq, ')');
    }

    return relSec(cnf.r, additional) == cnf.r;
}

#ifndef _WIN32
// Esegue la minimizzazione usando espresso ed eqntott
static size_t espressify(char *str, size_t sz)
{
    int esp_in[2], esp_out[2];
    pid_t esp_pid;

    // creo la prima pipe per espresso
    if (pipe(esp_in) || pipe(esp_out))
    {
        perror("Errore creazione pipe per espresso");
        exit(EXIT_FAILURE);
    }

    if ((esp_pid = fork()) < 0)
        perror("Errore fork espresso");
    else if (!esp_pid) // ESPRESSO
    {
        // uso la prima pipe come stdin
        close(esp_in[W_END]);
        dup2(esp_in[R_END], STDIN_FILENO);
        close(esp_in[R_END]);
        // uso la seconda pipe come stdout
        close(esp_out[R_END]);
        dup2(esp_out[W_END], STDOUT_FILENO);
        close(esp_out[W_END]);
        // eseguo espresso
        execlp("espresso", "espresso", "-epos", "-oeqntott", "-Dso", "-S1", (char *)NULL);
        perror("Errore");
        exit(EXIT_FAILURE);
    }

    int eqn_fd[2];
    pid_t eqn_pid;

    close(esp_in[R_END]);
    close(esp_out[W_END]);
    if (pipe(eqn_fd)) // creo la pipe per eqntott
    {
        perror("Errore creazione pipe");
        exit(EXIT_FAILURE);
    }

    if ((eqn_pid = fork()) < 0)
        perror("Errore fork eqntott");
    else if (!eqn_pid) // EQNTOTT
    {
        close(esp_out[R_END]);
        // leggo le equazioni da stdin (cioè la pipe con il parent)
        close(eqn_fd[W_END]);
        dup2(eqn_fd[R_END], STDIN_FILENO);
        close(eqn_fd[R_END]);
        // scrivo le PLA in stdout (cioè la pipe con espresso)
        dup2(esp_in[W_END], STDOUT_FILENO);
        close(esp_in[W_END]);
        // eseguo eqntott
        execlp("eqntott", "eqntott", "-l", (char *)NULL);
        perror("Errore");
        exit(EXIT_FAILURE);
    }
    // parent
    int rem;

    close(eqn_fd[R_END]);
    close(esp_in[W_END]);
    // scrivo le equazioni sulla pipe con eqntott
    if (write(eqn_fd[W_END], str, sz) != sz)
    {
        perror("Errore scrittura su pipe");
        exit(EXIT_FAILURE);
    }
    close(eqn_fd[W_END]);

    sz = 0;
    while ((rem = read(esp_out[R_END], str + sz, 4096)) > 0)
        sz += rem;

    if (rem < 0)
    {
        perror("Errore lettura da pipe");
        exit(EXIT_FAILURE);
    }
    close(esp_out[R_END]);
    str[sz] = '\0';
    // i figli hanno già finito, ma per pulizia eseguiamo il wait
    waitpid(eqn_pid, NULL, 0);
    waitpid(esp_pid, NULL, 0);

    return sz; // ritorno il numero di byte letti
}

// Funzione worker per multi-threading di OrdGenAndMin()
static void *genAndMinWorker(void *args)
{
    int id = *(int *)args;
    rel_t start = REL_PER_TH * id, end = start + REL_PER_TH;
    char *store = malloc(sizeof *store * MAX_EQN * LINELEN);
    char dnfname[FILE_NAMELEN];
    char cnfname[FILE_NAMELEN];
    char tabs[THREAD_N << 1] = {0};
    size_t sz = 0;

    for (int i = 0; i < id << 1; i += 2)
        tabs[i + 1] = tabs[i] = '\t';

    sprintf(dnfname, FILE_DNF "_%d", id);
    sprintf(cnfname, FILE_CNF "_%d", id);
    // puliamo i file
    fclose(fopen(dnfname, "wt"));
    fclose(fopen(cnfname, "wt"));
    // prendiamo tutte le possibili relazioni
    for (rel_t r = start; r < end; r += REL_STEP)
    {
        int i = ((r - start) >> REL_IASHIFT) % MAX_EQN;
        char eq[LINELEN];
        size_t temp = DNFToStr(eq, relToDnf(r), r); // creiamo una mappatura (DNF)

        strcpy(store + sz, eq);
        sz += temp;
        if (i == MAX_EQN - 1)
        {
            FILE *f_dnf = fopen(dnfname, "at"), *f_cnf = fopen(cnfname, "at");
            // salviamo sul file la dnf e minimizziamo
            fwrite(store, sizeof *store, sz, f_dnf);
            fclose(f_dnf);
            fwrite(store, sizeof *store, espressify(store, sz), f_cnf);
            fclose(f_cnf);
            // stampiamo info sul progresso
            printf("%s%d: %.2lf%%\r", tabs, id,
                   (double)((r - start + REL_STEP) * 100U) / REL_PER_TH);
            fflush(stdout);
            sz = 0;
        }
    }
    free(store);
    pthread_exit(NULL);
}

// Genera le relazioni salvandole in un file come clausole ORD
static void genAndMin(void)
{
    pthread_t th[THREAD_N];
    int ids[THREAD_N];

    for (int i = 0; i < THREAD_N; ++i)
    {
        ids[i] = i;
        pthread_create(&th[i], NULL, &genAndMinWorker, ids + i);
    }
    // resetto i file per sicurezza
    fclose(fopen(FILE_DNF, "wt"));
    fclose(fopen(FILE_CNF, "wt"));

    for (int i = 0; i < THREAD_N; ++i)
        pthread_join(th[i], NULL);

    putchar('\n');
    for (int i = 0; i < THREAD_N; ++i)
    {
        char script[FILE_NAMELEN];

        sprintf(script,
                "cat " FILE_DNF "_%d >>" FILE_DNF ";"
                "cat " FILE_CNF "_%d >>" FILE_CNF ";"
                "rm " FILE_DNF "_%d " FILE_CNF "_%d;",
                i, i, i, i);

        if (system(script))
            perror("Errore esecuzione script");
    }
}
#endif

// Controlla se le relazioni nel file F sono di Horn.
static int checkAll(FILE *f_in, FILE *f_out)
{
    int found = 0;
    char *line = (char *)malloc(LINELEN);
    rset_t horns = rsetInit();

    for (rel_t r = 0; eqRead(f_in, line); r += REL_STEP)
        if (eqIsHorn(line))
        {
            rsetIns(&horns, r);
            printf("Relazioni di Horn: %d\r", ++found);
            fflush(stdout);
        }
    free(line);

    rsetPrint(f_out, horns);
    rsetDel(horns);

    fprintf(f_out, "Totale relazioni di Horn: %d\n", found);
    printf("\nTotale relazioni di Horn: %d\n", found);

    return found;
}

void filterHorn(void)
{
#ifndef _WIN32
    char ans = 'n';

    // genero tutte le relazioni
    puts("Rigenerare le relazioni? (y/n)");
    if (scanf(" %c", &ans) != 1)
        perror("Errore lettura da stdin");
    if (ans == 'y')
        genAndMin();
#endif
    FILE *in, *out;
    // cerco le relazioni ORD-Horn
    checkAll(in = fopen(FILE_CNF, "rt"), out = fopen(FILE_OUT, "wt"));
    fclose(in);
    fclose(out);
}
