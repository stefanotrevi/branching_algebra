#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"
 
cprel_t cprelInit(void)
{
    return CPREL0;
}

cprel_t cprelShift(pcouples_t pc)
{
    return pc * BPREL_N;
}

int cprelHas(cprel_t cr, bprel_t br, pcouples_t pc)
{
    return prelHas((prel_t)(cr >> cprelShift(pc)), br);
}

cprel_t cprelSec(cprel_t cr1, cprel_t cr2)
{
    return cr1 & cr2;
}

cprel_t cprelAdd(cprel_t cr1, cprel_t cr2)
{
    return cr1 | cr2;
}

cprel_t cprelPut(cprel_t cr, pcouples_t pc, prel_t pr)
{
    return cprelAdd(cr, prelToCprel(pr, pc));
}

prel_t cprelGet(cprel_t cr, pcouples_t pc)
{
    return (prel_t)((cr >> cprelShift(pc)) & PREL_ALL);
}

cprel_t cprelComp(cprel_t cr1, cprel_t cr2)
{
    // Per ogni coppia I*K* = (r[I*,J-] ° r[J-, K*]) & (r[I*,J+] ° r[J+, K*])
    prel_t p1, p2;
    cprel_t cpr = cprelInit();

    // AMBM
    p1 = prelComp(cprelGet(cr1, AMBM), cprelGet(cr2, AMBM));
    p2 = prelComp(cprelGet(cr1, AMBP), cprelGet(cr2, APBM));
    cpr = cprelPut(cpr, AMBM, prelSec(p1, p2));

    // AMBP
    p1 = prelComp(cprelGet(cr1, AMBM), cprelGet(cr2, AMBP));
    p2 = prelComp(cprelGet(cr1, AMBP), cprelGet(cr2, APBP));
    cpr = cprelPut(cpr, AMBP, prelSec(p1, p2));

    // APBM
    p1 = prelComp(cprelGet(cr1, APBM), cprelGet(cr2, AMBM));
    p2 = prelComp(cprelGet(cr1, APBP), cprelGet(cr2, APBM));
    cpr = cprelPut(cpr, APBM, prelSec(p1, p2));

    // APBP
    p1 = prelComp(cprelGet(cr1, APBM), cprelGet(cr2, AMBP));
    p2 = prelComp(cprelGet(cr1, APBP), cprelGet(cr2, APBP));
    cpr = cprelPut(cpr, APBP, prelSec(p1, p2));

    return cpr;
}

rel_t cprelToRel(cprel_t cr)
{
    rel_t r = REL_ALL;

    for (pcouples_t pc = AMBM; pc < COUPLES_N; ++pc)
        r = relSec(r, prelToRel(cprelGet(cr, pc), pc));

    return r;
}
