#include "rels/rel.h"
#include "generator.h"
#include "lut.h"
#include "rels/brel.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "sets/cuRset.cuh"
#include "sets/set.h"

#include <ctype.h>
#include <cuda_runtime.h>
#include X86INTRIN_H
#include <stdlib.h>
#include <string.h>

rel_t blackGet(uint i)
{
    return _pdep_u32(i, BRADD7(EQ, F, FI, M, MI, S, SI));
}

rel_t relInit(void)
{
    return REL0;
}

int relIsEmpty(rel_t r)
{
    return RELISEMPTY(r);
}

int relIsAll(rel_t r)
{
    return RELISALL(r);
}

int relIsIA(rel_t r)
{
    return RELISIA(r);
}

int relIsHorn(rel_t r)
{
    return lutHornMapGet(r);
}

int relIsPoint(rel_t r)
{
    return lutPointMapGet(r);
}

int relHas(rel_t r, brel_t br)
{
    return RELHAS(r, br);
}

int relSubSet(rel_t r1, rel_t r2)
{
    return RELISSUBSET(r1, r2);
}

int relPropSet(rel_t r1, rel_t r2)
{
    return relSubSet(r1, r2) && r1 != r2;
}

rel_t relSec(rel_t r1, rel_t r2)
{
    return RELSEC(r1, r2);
}

rel_t relAdd(rel_t r1, rel_t r2)
{
    return RELADD(r1, r2);
}

rel_t relSub(rel_t r1, rel_t r2)
{
    return RELSUB(r1, r2);
}

rel_t relPut(rel_t r, brel_t br)
{
    return RELPUT(r, br);
}

rel_t relRem(rel_t r, brel_t br)
{
    return RELSUB(r, br);
}

rel_t relInv(rel_t r)
{
    return RELINV(r);
}

rel_t relConv(rel_t r)
{
    return RELCONV(r);
}

rel_t relLeft(rel_t r)
{
    return COMPLUT_GETLEFT(r);
}

rel_t relRight(rel_t r)
{
    return COMPLUT_GETRIGHT(r);
}

rel_t relComp(rel_t r1, rel_t r2)
{
#ifdef BA
    rel_t l1 = relLeft(r1), l2 = relLeft(r2), r;

    r1 = relRight(r1);
    r2 = relRight(r2);
    r = compGet(r1, r2, COMPLUT_RR, COMPLUT_NR);
    r = relAdd(r, compGet(r1, l2, COMPLUT_RL, COMPLUT_NL));
    r = relAdd(r, compGet(l1, r2, COMPLUT_LR, COMPLUT_NR));
    r = relAdd(r, compGet(l1, l2, COMPLUT_LL, COMPLUT_NL));

    return relSec(r, REL_ALL);
#else
    return compGet(r1, r2);
#endif
}

rel_t relComp2(rel_t r1, rel_t r2)
{
    rel_t r = REL0;
    brel_t brs[BREL_N];
    int n = 0;

    // "metto in cache" le relazioni attive in r2
    for (brel_t br = BR0; br < BREL_N; ++br)
        if (relHas(r2, br))
            brs[n++] = br;

    for (brel_t br1 = BR0; br1 < BREL_N; ++br1)
        if (relHas(r1, br1))
            for (int i = 0; i < n; ++i)
                r = relAdd(r, brelComp(br1, brs[i]));

    return relSec(r, REL_ALL);
}

rel_t relComp3(rel_t r1, rel_t r2)
{
    rel_t r = REL0;
    cprel_t cp2[BREL_N];
    int n = 0;

    // precalcolo gli elementi di r2
    for (brel_t br = BR0; br < BREL_N; ++br)
        if (relHas(r2, br))
            cp2[n++] = brelTocprel(br);

    for (brel_t br1 = BR0; br1 < BREL_N; ++br1)
        if (relHas(r1, br1))
            for (int i = 0; i < n; ++i)
                r = relAdd(r, cprelToRel(cprelComp(brelTocprel(br1), cp2[i])));

    return relSec(r, REL_ALL);
}

rel_t relPath(rel_t ij, rel_t jk, rel_t ik)
{
    return relSec(relComp(ij, jk), ik);
}

rel_t relRand(int rel_dens)
{
    rel_t r;

    for (brel_t br = BR0; br < BREL_N; ++br)
        if (rand() % 100 < rel_dens)
            r = relPut(r, br);

    return relIsEmpty(r) ? REL_ALL : r; // la relazione nulla non è valida
}

rel_t relSample(const rel_t *allow, int size)
{
    rel_t r = allow[rand() % size];

    return relIsEmpty(r) ? REL_ALL : r; // la relazione nulla non è valida
}

brel_t relFirstBr(rel_t r)
{
    return (brel_t)_tzcnt_u32(r);
}

int relSize(rel_t r)
{
    return _popcnt32(r);
}

rel_t relNext(rel_t r)
{
    return r << 1U;
}

FILE *relPrint(FILE *f, rel_t r)
{
    fprintf(f, "%u: ( ", r);
    for (brel_t br = BR0; br < BREL_N; ++br)
        if (relHas(r, br))
            fprintf(f, "%s ", brelToStr(br));

    fputs(")\n", f);

    return f;
}

rel_t relFromStr(char *str)
{
    rel_t r = REL0;

    while (*str && *str != '\n')
    {
        while (*str && !isalpha(*str))
            ++str;

        if (isalpha(*str))
            r = relPut(r, brelFromStr(str));

        while (*str && (isalpha(*str) || *str == '\''))
            ++str;
    }

    return r;
}

int relToStr(char *buf, rel_t r)
{
    int first = 1, n = 0;

    buf[n++] = '(';

    if (relIsAll(r))
    {
        buf[n++] = 'A';
        buf[n++] = 'L';
        buf[n++] = 'L';
    }
    else
        for (brel_t br = BR0; br < BREL_N; ++br)
            if (relHas(r, br))
            {
                n += sprintf(buf + n, "%s%s", first ? "" : " ", brelToStr(br));
                first = 0;
            }

    buf[n++] = ')';
    buf[n] = '\0';

    return n;
}

const rel_t *relPartition(rel_t r, partflag_t flag)
{
    switch (flag)
    {
    case PART_HORN: return hornPartGet(r);
    default: return NULL;
    }
}
