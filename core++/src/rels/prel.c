#include "rels/prel.h"
#include "sets/set.h"
#include "lits/lit.h"
#include "lut.h"
#include "rels/bprel.h"
#include "rels/cprel.h"
#include "rels/rel.h"

#include <stdlib.h>
#include <string.h>

/* Una relazione tra punti è rappresentata con una bitmap del tipo "0000 aaXX". Per ottenere
il converso, dobbiamo scambiare le coppie, ignorando le relazioni "X" (simmetriche). Shiftiamo a 
sinistra e a destra di una posizione la bitmap, per poi rimuovere i bit superflui. */
#define PCONV_LMASK 0b1000U // tiene solo i bit per lo shift a sinistra
#define PCONV_RMASK 0b0100U // tiene solo i bit per lo shift a destra
#define PCONV_MASK 0b11U    // tiene solo i bit delle relazioni simmetriche

prel_t prelInit(void)
{
    return PREL0;
}

prel_t prelAdd(prel_t pr1, prel_t pr2)
{
    return PRELADD(pr1, pr2);
}

prel_t prelIns(prel_t pr, bprel_t bpr)
{
    return prelAdd(pr, BPRTOPR(bpr));
}

prel_t prelSec(prel_t pr1, prel_t pr2)
{
    return PRELSEC(pr1, pr2);
}

prel_t prelConv(prel_t pr)
{
    return (prel_t)(((pr << 1U) & PCONV_LMASK) | ((pr >> 1U) & PCONV_RMASK) | (pr & PCONV_MASK));
}

prel_t prelComp(prel_t pr1, prel_t pr2)
{
    return pCompGet(pr1, pr2);
}

prel_t prelComp2(prel_t pr1, prel_t pr2)
{
    prel_t pr = PREL0;

    for (bprel_t bp1 = BPR0; bp1 < BPREL_N; ++bp1)
        if (prelHas(pr1, bp1))
            for (bprel_t bp2 = BPR0; bp2 < BPREL_N; ++bp2)
                if (prelHas(pr2, bp2))
                    pr = prelAdd(pr, bprelComp(bp1, bp2));

    return prelSec(pr, PREL_ALL);
}

prel_t prelPath(prel_t ij, prel_t jk, prel_t ik)
{
    return prelSec(prelComp(ij, jk), ik);
}

prel_t prelRand(void)
{
    prel_t pr = (prel_t)(rand() & PREL_ALL);

    return (prel_t)(prelIsEmpty(pr) ? PREL_ALL : pr); // la relazione nulla non è valida
}

int prelHas(prel_t pr, bprel_t bpr)
{
    return pr & BPRTOPR(bpr);
}

int prelIsEmpty(prel_t pr)
{
    return !pr;
}

int prelIsAll(prel_t pr)
{
    return pr >= PREL_ALL; // uso >= per ignorare eventuali "overflow"...
}

pcouples_t pcFlip(pcouples_t pc)
{
    return pc == AMBP ? APBM : pc == APBM ? AMBP : pc;
}

int prelToStr(char *buf, prel_t pr)
{
    const char* str = prelToChar(pr);
    int len = strlen(str);

    strcpy(buf, str);

    return len;
}

const char *prelToChar(prel_t pr)
{
    switch (pr)
    {
    case BPRTOPR(PEQ): return "=";
    case BPRTOPR(PB): return "<";
    case BPRTOPR(PBI): return ">";
    case BPRTOPR(PUN): return "||";
    case BPRADD2(PEQ, PB): return "<=";
    case BPRADD2(PEQ, PBI): return ">=";
    case BPRADD2(PEQ, PUN): return "||=";
    case BPRADD2(PB, PBI): return "<>";
    case BPRADD2(PB, PUN): return "<||";
    case BPRADD2(PBI, PUN): return ">||";
    case BPRADD3(PEQ, PB, PBI): return "~";
    case BPRADD3(PEQ, PB, PUN): return "<=||";
    case BPRADD3(PEQ, PBI, PUN): return ">=||";
    case BPRADD3(PB, PBI, PUN): return "≠";
    case PREL_ALL: return "⊤";
    default: return "⊥";
    }
}

rel_t prelToRel(prel_t pr, pcouples_t pc)
{
    rel_t r = relInit();

    for (bprel_t bpr = BPR0; bpr < BPREL_N; ++bpr)
        if (prelHas(pr, bpr))
            r = relAdd(r, bprelToRel(bpr, pc));

    return r;
}

cprel_t prelToCprel(prel_t pr, pcouples_t pc)
{
    return (cprel_t)pr << cprelShift(pc);
}

FILE *prelPrint(FILE *f, prel_t pr)
{
    fprintf(f, "%d: ( ", pr);
    for (bprel_t bpr = BPR0; bpr < BPREL_N; ++bpr)
        if (prelHas(pr, bpr))
            fprintf(f, "%s ", bprelToStr(bpr));

    fputs(")\n", f);

    return f;
}
