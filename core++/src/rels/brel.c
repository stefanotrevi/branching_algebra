#include "rels/brel.h"
#include "rels/bprel.h"
#include "rels/cprel.h"
#include "rels/prel.h"
#include "rels/rel.h"

#undef _NODISCARD
#define _NODISCARD

#include <string.h>

static const rel_t brel_comptable[BREL_N][BREL_N] = {
    [BI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRTOR(BI),
        [F] = BRTOR(BI),
        [D] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [IBI] = BRTOR(IBI),
        [IMI] = BRTOR(IBI),
        [SI] = BRTOR(BI),
        [EQ] = BRTOR(BI),
        [S] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [IE] = BRTOR(IBI),
        [DI] = BRTOR(BI),
        [FI] = BRTOR(BI),
        [O] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [IM] = BRTOR(IBI),
        [M] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [B] = REL_ALL,
        [UN] = BRTOR(UN),
        [IB] = BRTOR(UN),
    },
    [MI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRTOR(BI),
        [F] = BRTOR(MI),
        [D] = BRADD4(IMI, OI, D, F),
        [IBI] = BRTOR(IBI),
        [IMI] = BRTOR(IBI),
        [SI] = BRTOR(BI),
        [EQ] = BRTOR(MI),
        [S] = BRADD4(IMI, OI, D, F),
        [IE] = BRTOR(IBI),
        [DI] = BRTOR(BI),
        [FI] = BRTOR(MI),
        [O] = BRADD4(IMI, OI, D, F),
        [IM] = BRTOR(IBI),
        [M] = BRADD4(S, EQ, IE, SI),
        [B] = BRADD7(B, IM, DI, M, IB, O, FI),
        [UN] = BRTOR(UN),
        [IB] = BRTOR(UN),
    },
    [OI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRADD3(BI, OI, MI),
        [F] = BRTOR(OI),
        [D] = BRADD4(IMI, OI, D, F),
        [IBI] = BRTOR(IBI),
        [IMI] = BRADD2(IBI, IMI),
        [SI] = BRADD3(BI, OI, MI),
        [EQ] = BRTOR(OI),
        [S] = BRADD4(IMI, OI, D, F),
        [IE] = BRADD2(IBI, IMI),
        [DI] = BRADD5(MI, BI, DI, OI, SI),
        [FI] = BRADD3(DI, OI, SI),
        [O] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
        [IM] = BRADD4(IMI, IE, IBI, IM),
        [M] = BRADD4(IM, DI, O, FI),
        [B] = BRADD7(B, IM, DI, M, IB, O, FI),
        [UN] = BRTOR(UN),
        [IB] = BRADD2(IB, UN),
    },
    [F] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRADD3(BI, OI, MI),
        [F] = BRTOR(F),
        [D] = BRTOR(D),
        [IBI] = BRTOR(IBI),
        [IMI] = BRADD2(IBI, IMI),
        [SI] = BRADD3(BI, OI, MI),
        [EQ] = BRTOR(F),
        [S] = BRTOR(D),
        [IE] = BRADD2(IBI, IMI),
        [DI] = BRADD5(MI, BI, DI, OI, SI),
        [FI] = BRADD3(FI, EQ, F),
        [O] = BRADD3(S, D, O),
        [IM] = BRADD4(IMI, IE, IBI, IM),
        [M] = BRTOR(M),
        [B] = BRTOR(B),
        [UN] = BRTOR(UN),
        [IB] = BRADD2(IB, UN),
    },
    [D] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRADD5(D, MI, BI, F, OI),
        [F] = BRTOR(D),
        [D] = BRTOR(D),
        [IBI] = BRTOR(IBI),
        [IMI] = BRADD3(D, IBI, IMI),
        [SI] = BRADD5(D, MI, BI, F, OI),
        [EQ] = BRTOR(D),
        [S] = BRTOR(D),
        [IE] = BRADD3(D, IBI, IMI),
        [DI] = BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI),
        [FI] = BRADD5(D, B, S, M, O),
        [O] = BRADD5(D, B, S, M, O),
        [IM] = BRADD9(D, IBI, B, IE, IM, S, M, IMI, O),
        [M] = BRTOR(B),
        [B] = BRTOR(B),
        [UN] = BRTOR(UN),
        [IB] = BRADD3(IB, B, UN),
    },
    [SI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(MI),
        [OI] = BRTOR(OI),
        [F] = BRTOR(OI),
        [D] = BRADD4(IMI, OI, D, F),
        [IBI] = BRTOR(IBI),
        [IMI] = BRTOR(IMI),
        [SI] = BRTOR(SI),
        [EQ] = BRTOR(SI),
        [S] = BRADD4(S, EQ, IE, SI),
        [IE] = BRTOR(IE),
        [DI] = BRTOR(DI),
        [FI] = BRTOR(DI),
        [O] = BRADD4(IM, DI, O, FI),
        [IM] = BRTOR(IM),
        [M] = BRADD4(IM, DI, O, FI),
        [B] = BRADD7(B, IM, DI, M, IB, O, FI),
        [UN] = BRTOR(UN),
        [IB] = BRTOR(IB),
    },
    [EQ] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(MI),
        [OI] = BRTOR(OI),
        [F] = BRTOR(F),
        [D] = BRTOR(D),
        [IBI] = BRTOR(IBI),
        [IMI] = BRTOR(IMI),
        [SI] = BRTOR(SI),
        [EQ] = BRTOR(EQ),
        [S] = BRTOR(S),
        [IE] = BRTOR(IE),
        [DI] = BRTOR(DI),
        [FI] = BRTOR(FI),
        [O] = BRTOR(O),
        [IM] = BRTOR(IM),
        [M] = BRTOR(M),
        [B] = BRTOR(B),
        [UN] = BRTOR(UN),
        [IB] = BRTOR(IB),
    },
    [S] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(MI),
        [OI] = BRADD3(OI, D, F),
        [F] = BRTOR(D),
        [D] = BRTOR(D),
        [IBI] = BRTOR(IBI),
        [IMI] = BRADD2(D, IMI),
        [SI] = BRADD3(S, EQ, SI),
        [EQ] = BRTOR(S),
        [S] = BRTOR(S),
        [IE] = BRADD2(S, IE),
        [DI] = BRADD5(B, DI, M, O, FI),
        [FI] = BRADD3(M, B, O),
        [O] = BRADD3(M, B, O),
        [IM] = BRADD4(M, B, O, IM),
        [M] = BRTOR(B),
        [B] = BRTOR(B),
        [UN] = BRTOR(UN),
        [IB] = BRADD2(IB, B),
    },
    [DI] = {
        [BI] = BRADD5(MI, BI, DI, OI, SI),
        [MI] = BRADD3(DI, OI, SI),
        [OI] = BRADD3(DI, OI, SI),
        [F] = BRADD3(DI, OI, SI),
        [D] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
        [IBI] = BRADD4(IMI, IE, IBI, IM),
        [IMI] = BRADD3(IM, IE, IMI),
        [SI] = BRTOR(DI),
        [EQ] = BRTOR(DI),
        [S] = BRADD4(IM, DI, O, FI),
        [IE] = BRTOR(IM),
        [DI] = BRTOR(DI),
        [FI] = BRTOR(DI),
        [O] = BRADD4(IM, DI, O, FI),
        [IM] = BRTOR(IM),
        [M] = BRADD4(IM, DI, O, FI),
        [B] = BRADD7(B, IM, DI, M, IB, O, FI),
        [UN] = BRADD2(IB, UN),
        [IB] = BRTOR(IB),
    },
    [FI] = {
        [BI] = BRADD5(MI, BI, DI, OI, SI),
        [MI] = BRADD3(DI, OI, SI),
        [OI] = BRADD3(DI, OI, SI),
        [F] = BRADD3(FI, EQ, F),
        [D] = BRADD3(S, D, O),
        [IBI] = BRADD4(IMI, IE, IBI, IM),
        [IMI] = BRADD3(IM, IE, IMI),
        [SI] = BRTOR(DI),
        [EQ] = BRTOR(FI),
        [S] = BRTOR(O),
        [IE] = BRTOR(IM),
        [DI] = BRTOR(DI),
        [FI] = BRTOR(FI),
        [O] = BRTOR(O),
        [IM] = BRTOR(IM),
        [M] = BRTOR(M),
        [B] = BRTOR(B),
        [UN] = BRADD2(IB, UN),
        [IB] = BRTOR(IB),
    },
    [O] = {
        [BI] = BRADD5(MI, BI, DI, OI, SI),
        [MI] = BRADD3(DI, OI, SI),
        [OI] = BRADD9(D, F, S, DI, OI, SI, EQ, O, FI),
        [F] = BRADD3(S, D, O),
        [D] = BRADD3(S, D, O),
        [IBI] = BRADD4(IMI, IE, IBI, IM),
        [IMI] = BRADD6(D, IE, IM, S, IMI, O),
        [SI] = BRADD3(DI, O, FI),
        [EQ] = BRTOR(O),
        [S] = BRTOR(O),
        [IE] = BRADD2(O, IM),
        [DI] = BRADD5(B, DI, M, O, FI),
        [FI] = BRADD3(M, B, O),
        [O] = BRADD3(M, B, O),
        [IM] = BRADD4(M, B, O, IM),
        [M] = BRTOR(B),
        [B] = BRTOR(B),
        [UN] = BRADD2(IB, UN),
        [IB] = BRADD2(IB, B),
    },
    [M] = {
        [BI] = BRADD5(MI, BI, DI, OI, SI),
        [MI] = BRADD3(FI, EQ, F),
        [OI] = BRADD3(S, D, O),
        [F] = BRADD3(S, D, O),
        [D] = BRADD3(S, D, O),
        [IBI] = BRADD4(IMI, IE, IBI, IM),
        [IMI] = BRADD3(S, D, O),
        [SI] = BRTOR(M),
        [EQ] = BRTOR(M),
        [S] = BRTOR(M),
        [IE] = BRTOR(M),
        [DI] = BRTOR(B),
        [FI] = BRTOR(B),
        [O] = BRTOR(B),
        [IM] = BRTOR(B),
        [M] = BRTOR(B),
        [B] = BRTOR(B),
        [UN] = BRADD2(IB, UN),
        [IB] = BRTOR(B),
    },
    [B] = {
        [BI] = BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI),
        [MI] = BRADD5(D, B, S, M, O),
        [OI] = BRADD5(D, B, S, M, O),
        [F] = BRADD5(D, B, S, M, O),
        [D] = BRADD5(D, B, S, M, O),
        [IBI] = BRADD9(D, IBI, B, IE, IM, S, M, IMI, O),
        [IMI] = BRADD5(D, B, S, M, O),
        [SI] = BRTOR(B),
        [EQ] = BRTOR(B),
        [S] = BRTOR(B),
        [IE] = BRTOR(B),
        [DI] = BRTOR(B),
        [FI] = BRTOR(B),
        [O] = BRTOR(B),
        [IM] = BRTOR(B),
        [M] = BRTOR(B),
        [B] = BRTOR(B),
        [UN] = BRADD3(IB, B, UN),
        [IB] = BRTOR(B),
    },
    [IMI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [OI] = BRADD4(IMI, BI, OI, MI),
        [IMI] = BRADD5(D, IBI, F, OI, IMI),
        [F] = BRTOR(IMI),
        [D] = BRTOR(IMI),
        [IBI] = BRTOR(IBI),
        [SI] = BRADD4(IMI, BI, OI, MI),
        [IE] = BRADD5(D, IBI, F, OI, IMI),
        [EQ] = BRTOR(IMI),
        [S] = BRTOR(IMI),
        [DI] = BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB),
        [IM] = BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI),
        [FI] = BRADD4(IM, IB, IE, IMI),
        [O] = BRADD4(IM, IB, IE, IMI),
        [IB] = BRADD8(B, IM, DI, M, UN, IB, O, FI),
        [M] = BRTOR(IB),
        [B] = BRTOR(IB),
        [UN] = BRTOR(UN),
    },
    [IE] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(MI),
        [IMI] = BRADD4(IMI, OI, D, F),
        [OI] = BRADD2(OI, IMI),
        [F] = BRTOR(IMI),
        [D] = BRTOR(IMI),
        [IBI] = BRTOR(IBI),
        [IE] = BRADD4(S, EQ, IE, SI),
        [SI] = BRADD2(IE, SI),
        [EQ] = BRTOR(IE),
        [S] = BRTOR(IE),
        [IB] = BRADD7(B, IM, DI, M, IB, O, FI),
        [DI] = BRADD3(IB, DI, IM),
        [FI] = BRADD2(IB, IM),
        [O] = BRADD2(IB, IM),
        [IM] = BRADD5(IM, DI, IB, O, FI),
        [M] = BRTOR(IB),
        [B] = BRTOR(IB),
        [UN] = BRTOR(UN),
    },
    [UN] = {
        [BI] = BRADD3(BI, UN, IBI),
        [IBI] = BRADD8(D, IBI, MI, BI, F, OI, UN, IMI),
        [MI] = BRADD2(UN, IBI),
        [OI] = BRADD2(UN, IBI),
        [F] = BRADD2(UN, IBI),
        [D] = BRADD2(UN, IBI),
        [IMI] = BRADD2(UN, IBI),
        [UN] = REL_ALL,
        [SI] = BRTOR(UN),
        [EQ] = BRTOR(UN),
        [S] = BRTOR(UN),
        [IE] = BRTOR(UN),
        [DI] = BRTOR(UN),
        [FI] = BRTOR(UN),
        [O] = BRTOR(UN),
        [IM] = BRTOR(UN),
        [M] = BRTOR(UN),
        [B] = BRTOR(UN),
        [IB] = BRTOR(UN),
    },
    [IB] = {
        [BI] = BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB),
        [IBI] = BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI),
        [MI] = BRADD4(IM, IB, IE, IMI),
        [OI] = BRADD4(IM, IB, IE, IMI),
        [F] = BRADD4(IM, IB, IE, IMI),
        [D] = BRADD4(IM, IB, IE, IMI),
        [IMI] = BRADD4(IM, IB, IE, IMI),
        [UN] = BRADD8(B, IM, DI, M, UN, IB, O, FI),
        [SI] = BRTOR(IB),
        [EQ] = BRTOR(IB),
        [S] = BRTOR(IB),
        [IE] = BRTOR(IB),
        [DI] = BRTOR(IB),
        [FI] = BRTOR(IB),
        [O] = BRTOR(IB),
        [IM] = BRTOR(IB),
        [M] = BRTOR(IB),
        [B] = BRTOR(IB),
        [IB] = BRTOR(IB),
    },
    [IBI] = {
        [BI] = BRTOR(BI),
        [MI] = BRTOR(BI),
        [IMI] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [OI] = BRADD2(BI, IBI),
        [F] = BRTOR(IBI),
        [D] = BRTOR(IBI),
        [IBI] = BRTOR(IBI),
        [IE] = BRADD7(D, IBI, MI, BI, F, OI, IMI),
        [SI] = BRADD2(BI, IBI),
        [EQ] = BRTOR(IBI),
        [S] = BRTOR(IBI),
        [IB] = REL_ALL,
        [DI] = BRADD3(BI, UN, IBI),
        [FI] = BRADD2(UN, IBI),
        [O] = BRADD2(UN, IBI),
        [IM] = BRADD8(D, IBI, MI, BI, F, OI, UN, IMI),
        [M] = BRTOR(UN),
        [B] = BRTOR(UN),
        [UN] = BRTOR(UN),
    },
    [IM] = {
        [BI] = BRADD5(MI, BI, DI, OI, SI),
        [MI] = BRADD3(DI, OI, SI),
        [IMI] = BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI),
        [OI] = BRADD6(IE, IM, DI, OI, SI, IMI),
        [F] = BRADD3(IM, IE, IMI),
        [D] = BRADD3(IM, IE, IMI),
        [IBI] = BRADD4(IMI, IE, IBI, IM),
        [IE] = BRADD4(IM, DI, O, FI),
        [SI] = BRADD2(DI, IM),
        [EQ] = BRTOR(IM),
        [S] = BRTOR(IM),
        [IB] = BRADD7(B, IM, DI, M, IB, O, FI),
        [DI] = BRADD3(IB, DI, IM),
        [FI] = BRADD2(IB, IM),
        [O] = BRADD2(IB, IM),
        [IM] = BRADD5(IM, DI, IB, O, FI),
        [M] = BRTOR(IB),
        [B] = BRTOR(IB),
        [UN] = BRADD2(IB, UN),
    },
};

brel_t brInit(void)
{
    return BR0;
}

rel_t brelToRel(brel_t br)
{
    return BRTOR(br);
}

rel_t brelAtoRel(brel_t *br, int n)
{
    switch (n)
    {
    case 0: return REL0;
    case 1: return BRTOR(br[0]);
    case 2: return BRADD2(br[0], br[1]);
    case 3: return BRADD3(br[0], br[1], br[2]);
    case 4: return BRADD4(br[0], br[1], br[2], br[3]);
    case 5: return BRADD5(br[0], br[1], br[2], br[3], br[4]);
    case 6: return BRADD6(br[0], br[1], br[2], br[3], br[4], br[5]);
    case 7: return BRADD7(br[0], br[1], br[2], br[3], br[4], br[5], br[6]);
    case 8: return BRADD8(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7]);
    case 9: return BRADD9(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8]);
    case 10: return BRADD10(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9]);

    case 11: return BRADD11(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10]);
    case 12: return BRADD12(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11]);
    case 13: return BRADD13(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12]);
    case 14: return BRADD14(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12], br[13]);
    case 15: return BRADD15(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12], br[13], br[14]);
    case 16: return BRADD16(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12], br[13], br[14], br[15]);
    case 17: return BRADD17(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12], br[13], br[14], br[15], br[16]);
    case 18: return BRADD18(br[0], br[1], br[2], br[3], br[4], br[5], br[6], br[7], br[8], br[9],
                            br[10], br[11], br[12], br[13], br[14], br[15], br[16], br[17]);
    default: return REL_ALL;
    }
}

rel_t brelLesser(brel_t br)
{
    switch (br)
    {
    case B: return BRTOR(B);
    case M: return BRADD2(B, M);
    case O: return BRADD3(B, M, O);
    case FI: return BRADD4(B, M, O, FI);
    case DI: return BRADD5(B, M, O, FI, DI);
    case S: return BRADD4(B, M, O, S);
    case EQ: return BRADD6(B, M, O, S, FI, EQ);
    case SI: return BRADD8(B, M, O, S, FI, EQ, DI, SI);
    case D: return BRADD5(B, M, O, S, D);
    case F: return BRADD8(B, M, O, S, D, FI, EQ, F);
    case OI: return BRADD11(B, M, O, S, D, FI, EQ, F, DI, SI, OI);
    case MI: return BRADD12(B, M, O, S, D, FI, EQ, F, DI, SI, OI, MI);
    case BI: return BRADD13(B, M, O, S, D, FI, EQ, F, DI, SI, OI, MI, BI);
    case IM: return BRADD6(B, M, O, FI, DI, IM);
    case IB: return BRADD7(B, M, O, FI, DI, IM, IB);
    case IE: return BRADD10(B, M, O, FI, DI, IM, S, EQ, SI, IE);
    case IMI: return BRADD14(B, M, O, FI, DI, IM, S, EQ, SI, IE, D, F, OI, IMI);
    case IBI: return BRADD17(B, M, O, FI, DI, IM, S, EQ, SI, IE, D, F, OI, IMI, MI, BI, IBI);
    case UN: return REL_ALL;
    default: return REL0;
    }
}

rel_t brelBigger(brel_t br)
{
    return relPut(relInv(brelLesser(br)), br);
}

int brelDim(brel_t br)
{
    switch (br)
    {
    case UN: return 4;
    case IE: return 1;
    case IB: return 3;
    case IBI: return 3;
    case IM: return 3;
    case IMI: return 3;
    case EQ: return 0;
    case B: return 2;
    case BI: return 2;
    case D: return 2;
    case DI: return 2;
    case F: return 1;
    case FI: return 1;
    case M: return 1;
    case MI: return 1;
    case O: return 2;
    case OI: return 2;
    case S: return 1;
    case SI: return 1;
    default: return -1;
    }
}

brel_t brelConv(brel_t br)
{
    switch (br)
    {
    case UN: return UN;
    case IE: return IE;
    case IB: return IBI;
    case IBI: return IB;
    case IM: return IMI;
    case IMI: return IM;
    case EQ: return EQ;
    case B: return BI;
    case BI: return B;
    case D: return DI;
    case DI: return D;
    case F: return FI;
    case FI: return F;
    case M: return MI;
    case MI: return M;
    case O: return OI;
    case OI: return O;
    case S: return SI;
    case SI: return S;
    default: return BREL_N;
    }
}

brel_t brelPath(brel_t ij, brel_t jk, brel_t ik)
{
    return relHas(brelComp(ij, jk), ik) ? ik : BREL_N;
}

const char *brelToStr(brel_t br)
{
    switch (br)
    {
    case UN: return "u";
    case IE: return "ie";
    case IB: return "ib";
    case IBI: return "ibi";
    case IM: return "im";
    case IMI: return "imi";
    case EQ: return "e";
    case B: return "b";
    case BI: return "bi";
    case D: return "d";
    case DI: return "di";
    case F: return "f";
    case FI: return "fi";
    case M: return "m";
    case MI: return "mi";
    case O: return "o";
    case OI: return "oi";
    case S: return "s";
    case SI: return "si";
    default: return "?";
    }
}

brel_t brelFromStr(const char *str)
{
    // devo considerare s
    if (str[1] == 'i' || str[1] == '\'' || (strlen(str) > 2 && (str[2] == 'i' || str[2] == '\'')))
        switch (str[0])
        {
        case 'b': return BI;
        case 'd': return DI;
        case 'f': return FI;
        case 'i':
            switch (str[1])
            {
            case 'b': return IBI;
            case 'm': return IMI;
            default: return BR0;
            }
        case 'm': return MI;
        case 'o': return OI;
        case 's': return SI;
        default: return BR0;
        }

    switch (str[0])
    {
    case 'b': return B;
    case 'd': return D;
    case 'e': return EQ;
    case 'f': return F;
    case 'i':
        switch (str[1])
        {
        case 'b': return IB;
        case 'e': return IE;
        case 'm': return IM;
        default: return BR0;
        }
    case 'm': return M;
    case 'o': return O;
    case 's': return S;
    case 'u': return UN;
    default: return BR0;
    }
}

cprel_t brelTocprel(brel_t br)
{
    /* Trasformiamo le relazioni tra intervalli in relazioni fra estremi.
     * Dobbiamo esplicitare le relazioni fra tutte le coppie di punti. */
    cprel_t cpr = cprelInit();

    switch (br)
    {
    case UN:
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PUN));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PUN));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PUN));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        return cpr;
    case IE:
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PEQ));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        return cpr;
    case IB:
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PUN));
        return cpr;
    case IBI:
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PUN));
        return cpr;
    case IM:
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        return cpr;
    case IMI:
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PUN));
        return cpr;
    case EQ:
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PEQ));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PEQ));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        return cpr;
    case B:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        return cpr;
    case BI:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        return cpr;
    case D:
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        return cpr;
    case DI:
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        return cpr;
    case F:
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PEQ));
        return cpr;
    case FI:
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PEQ));
        return cpr;
    case M:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PEQ));
        return cpr;
    case MI:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PEQ));
        return cpr;
    case O:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        return cpr;
    case OI:
        cpr = cprelPut(cpr, APBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PBI));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        return cpr;
    case S:
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PEQ));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        return cpr;
    case SI:
        cpr = cprelPut(cpr, AMBM, BPRTOPR(PEQ));
        cpr = cprelPut(cpr, AMBP, BPRTOPR(PB));
        cpr = cprelPut(cpr, APBP, BPRTOPR(PBI));
        cpr = cprelPut(cpr, APBM, BPRTOPR(PBI));
        return cpr;
    default:
        return cpr;
    }
}

rel_t brelComp(brel_t br1, brel_t br2)
{
    return brel_comptable[br1][br2];
}

/*
rel_t brelComp(brel_t br1, brel_t br2)
{
    switch (br1)
    {
    case BI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRTOR(BI);
        case F: return BRTOR(BI);
        case D: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case IBI: return BRTOR(IBI);
        case IMI: return BRTOR(IBI);
        case SI: return BRTOR(BI);
        case EQ: return BRTOR(BI);
        case S: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case IE: return BRTOR(IBI);
        case DI: return BRTOR(BI);
        case FI: return BRTOR(BI);
        case O: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case IM: return BRTOR(IBI);
        case M: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case B: return REL_ALL;
        case UN: return BRTOR(UN);
        case IB: return BRTOR(UN);
        default: return REL0;
        }
    case MI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRTOR(BI);
        case F: return BRTOR(MI);
        case D: return BRADD4(IMI, OI, D, F);
        case IBI: return BRTOR(IBI);
        case IMI: return BRTOR(IBI);
        case SI: return BRTOR(BI);
        case EQ: return BRTOR(MI);
        case S: return BRADD4(IMI, OI, D, F);
        case IE: return BRTOR(IBI);
        case DI: return BRTOR(BI);
        case FI: return BRTOR(MI);
        case O: return BRADD4(IMI, OI, D, F);
        case IM: return BRTOR(IBI);
        case M: return BRADD4(S, EQ, IE, SI);
        case B: return BRADD7(B, IM, DI, M, IB, O, FI);
        case UN: return BRTOR(UN);
        case IB: return BRTOR(UN);
        default: return REL0;
        }
    case OI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRADD3(BI, OI, MI);
        case F: return BRTOR(OI);
        case D: return BRADD4(IMI, OI, D, F);
        case IBI: return BRTOR(IBI);
        case IMI: return BRADD2(IBI, IMI);
        case SI: return BRADD3(BI, OI, MI);
        case EQ: return BRTOR(OI);
        case S: return BRADD4(IMI, OI, D, F);
        case IE: return BRADD2(IBI, IMI);
        case DI: return BRADD5(MI, BI, DI, OI, SI);
        case FI: return BRADD3(DI, OI, SI);
        case O: return BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI);
        case IM: return BRADD4(IMI, IE, IBI, IM);
        case M: return BRADD4(IM, DI, O, FI);
        case B: return BRADD7(B, IM, DI, M, IB, O, FI);
        case UN: return BRTOR(UN);
        case IB: return BRADD2(IB, UN);
        default: return REL0;
        }
    case F:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRADD3(BI, OI, MI);
        case F: return BRTOR(F);
        case D: return BRTOR(D);
        case IBI: return BRTOR(IBI);
        case IMI: return BRADD2(IBI, IMI);
        case SI: return BRADD3(BI, OI, MI);
        case EQ: return BRTOR(F);
        case S: return BRTOR(D);
        case IE: return BRADD2(IBI, IMI);
        case DI: return BRADD5(MI, BI, DI, OI, SI);
        case FI: return BRADD3(FI, EQ, F);
        case O: return BRADD3(S, D, O);
        case IM: return BRADD4(IMI, IE, IBI, IM);
        case M: return BRTOR(M);
        case B: return BRTOR(B);
        case UN: return BRTOR(UN);
        case IB: return BRADD2(IB, UN);
        default: return REL0;
        }
    case D:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRADD5(D, MI, BI, F, OI);
        case F: return BRTOR(D);
        case D: return BRTOR(D);
        case IBI: return BRTOR(IBI);
        case IMI: return BRADD3(D, IBI, IMI);
        case SI: return BRADD5(D, MI, BI, F, OI);
        case EQ: return BRTOR(D);
        case S: return BRTOR(D);
        case IE: return BRADD3(D, IBI, IMI);
        case DI: return BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI);
        case FI: return BRADD5(D, B, S, M, O);
        case O: return BRADD5(D, B, S, M, O);
        case IM: return BRADD9(D, IBI, B, IE, IM, S, M, IMI, O);
        case M: return BRTOR(B);
        case B: return BRTOR(B);
        case UN: return BRTOR(UN);
        case IB: return BRADD3(IB, B, UN);
        default: return REL0;
        }
    case SI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(MI);
        case OI: return BRTOR(OI);
        case F: return BRTOR(OI);
        case D: return BRADD4(IMI, OI, D, F);
        case IBI: return BRTOR(IBI);
        case IMI: return BRTOR(IMI);
        case SI: return BRTOR(SI);
        case EQ: return BRTOR(SI);
        case S: return BRADD4(S, EQ, IE, SI);
        case IE: return BRTOR(IE);
        case DI: return BRTOR(DI);
        case FI: return BRTOR(DI);
        case O: return BRADD4(IM, DI, O, FI);
        case IM: return BRTOR(IM);
        case M: return BRADD4(IM, DI, O, FI);
        case B: return BRADD7(B, IM, DI, M, IB, O, FI);
        case UN: return BRTOR(UN);
        case IB: return BRTOR(IB);
        default: return REL0;
        }
    case EQ:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(MI);
        case OI: return BRTOR(OI);
        case F: return BRTOR(F);
        case D: return BRTOR(D);
        case IBI: return BRTOR(IBI);
        case IMI: return BRTOR(IMI);
        case SI: return BRTOR(SI);
        case EQ: return BRTOR(EQ);
        case S: return BRTOR(S);
        case IE: return BRTOR(IE);
        case DI: return BRTOR(DI);
        case FI: return BRTOR(FI);
        case O: return BRTOR(O);
        case IM: return BRTOR(IM);
        case M: return BRTOR(M);
        case B: return BRTOR(B);
        case UN: return BRTOR(UN);
        case IB: return BRTOR(IB);
        default: return REL0;
        }
    case S:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(MI);
        case OI: return BRADD3(OI, D, F);
        case F: return BRTOR(D);
        case D: return BRTOR(D);
        case IBI: return BRTOR(IBI);
        case IMI: return BRADD2(D, IMI);
        case SI: return BRADD3(S, EQ, SI);
        case EQ: return BRTOR(S);
        case S: return BRTOR(S);
        case IE: return BRADD2(S, IE);
        case DI: return BRADD5(B, DI, M, O, FI);
        case FI: return BRADD3(M, B, O);
        case O: return BRADD3(M, B, O);
        case IM: return BRADD4(M, B, O, IM);
        case M: return BRTOR(B);
        case B: return BRTOR(B);
        case UN: return BRTOR(UN);
        case IB: return BRADD2(IB, B);
        default: return REL0;
        }
    case DI:
        switch (br2)
        {
        case BI: return BRADD5(MI, BI, DI, OI, SI);
        case MI: return BRADD3(DI, OI, SI);
        case OI: return BRADD3(DI, OI, SI);
        case F: return BRADD3(DI, OI, SI);
        case D: return BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI);
        case IBI: return BRADD4(IMI, IE, IBI, IM);
        case IMI: return BRADD3(IM, IE, IMI);
        case SI: return BRTOR(DI);
        case EQ: return BRTOR(DI);
        case S: return BRADD4(IM, DI, O, FI);
        case IE: return BRTOR(IM);
        case DI: return BRTOR(DI);
        case FI: return BRTOR(DI);
        case O: return BRADD4(IM, DI, O, FI);
        case IM: return BRTOR(IM);
        case M: return BRADD4(IM, DI, O, FI);
        case B: return BRADD7(B, IM, DI, M, IB, O, FI);
        case UN: return BRADD2(IB, UN);
        case IB: return BRTOR(IB);
        default: return REL0;
        }
    case FI:
        switch (br2)
        {
        case BI: return BRADD5(MI, BI, DI, OI, SI);
        case MI: return BRADD3(DI, OI, SI);
        case OI: return BRADD3(DI, OI, SI);
        case F: return BRADD3(FI, EQ, F);
        case D: return BRADD3(S, D, O);
        case IBI: return BRADD4(IMI, IE, IBI, IM);
        case IMI: return BRADD3(IM, IE, IMI);
        case SI: return BRTOR(DI);
        case EQ: return BRTOR(FI);
        case S: return BRTOR(O);
        case IE: return BRTOR(IM);
        case DI: return BRTOR(DI);
        case FI: return BRTOR(FI);
        case O: return BRTOR(O);
        case IM: return BRTOR(IM);
        case M: return BRTOR(M);
        case B: return BRTOR(B);
        case UN: return BRADD2(IB, UN);
        case IB: return BRTOR(IB);
        default: return REL0;
        }
    case O:
        switch (br2)
        {
        case BI: return BRADD5(MI, BI, DI, OI, SI);
        case MI: return BRADD3(DI, OI, SI);
        case OI: return BRADD9(D, F, S, DI, OI, SI, EQ, O, FI);
        case F: return BRADD3(S, D, O);
        case D: return BRADD3(S, D, O);
        case IBI: return BRADD4(IMI, IE, IBI, IM);
        case IMI: return BRADD6(D, IE, IM, S, IMI, O);
        case SI: return BRADD3(DI, O, FI);
        case EQ: return BRTOR(O);
        case S: return BRTOR(O);
        case IE: return BRADD2(O, IM);
        case DI: return BRADD5(B, DI, M, O, FI);
        case FI: return BRADD3(M, B, O);
        case O: return BRADD3(M, B, O);
        case IM: return BRADD4(M, B, O, IM);
        case M: return BRTOR(B);
        case B: return BRTOR(B);
        case UN: return BRADD2(IB, UN);
        case IB: return BRADD2(IB, B);
        default: return REL0;
        }
    case M:
        switch (br2)
        {
        case BI: return BRADD5(MI, BI, DI, OI, SI);
        case MI: return BRADD3(FI, EQ, F);
        case OI: return BRADD3(S, D, O);
        case F: return BRADD3(S, D, O);
        case D: return BRADD3(S, D, O);
        case IBI: return BRADD4(IMI, IE, IBI, IM);
        case IMI: return BRADD3(S, D, O);
        case SI: return BRTOR(M);
        case EQ: return BRTOR(M);
        case S: return BRTOR(M);
        case IE: return BRTOR(M);
        case DI: return BRTOR(B);
        case FI: return BRTOR(B);
        case O: return BRTOR(B);
        case IM: return BRTOR(B);
        case M: return BRTOR(B);
        case B: return BRTOR(B);
        case UN: return BRADD2(IB, UN);
        case IB: return BRTOR(B);
        default: return REL0;
        }
    case B:
        switch (br2)
        {
        case BI: return BRADD13(D, MI, BI, B, F, S, DI, OI, M, SI, EQ, O, FI);
        case MI: return BRADD5(D, B, S, M, O);
        case OI: return BRADD5(D, B, S, M, O);
        case F: return BRADD5(D, B, S, M, O);
        case D: return BRADD5(D, B, S, M, O);
        case IBI: return BRADD9(D, IBI, B, IE, IM, S, M, IMI, O);
        case IMI: return BRADD5(D, B, S, M, O);
        case SI: return BRTOR(B);
        case EQ: return BRTOR(B);
        case S: return BRTOR(B);
        case IE: return BRTOR(B);
        case DI: return BRTOR(B);
        case FI: return BRTOR(B);
        case O: return BRTOR(B);
        case IM: return BRTOR(B);
        case M: return BRTOR(B);
        case B: return BRTOR(B);
        case UN: return BRADD3(IB, B, UN);
        case IB: return BRTOR(B);
        default: return REL0;
        }
    case IMI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case OI: return BRADD4(IMI, BI, OI, MI);
        case IMI: return BRADD5(D, IBI, F, OI, IMI);
        case F: return BRTOR(IMI);
        case D: return BRTOR(IMI);
        case IBI: return BRTOR(IBI);
        case SI: return BRADD4(IMI, BI, OI, MI);
        case IE: return BRADD5(D, IBI, F, OI, IMI);
        case EQ: return BRTOR(IMI);
        case S: return BRTOR(IMI);
        case DI: return BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB);
        case IM: return BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI);
        case FI: return BRADD4(IM, IB, IE, IMI);
        case O: return BRADD4(IM, IB, IE, IMI);
        case IB: return BRADD8(B, IM, DI, M, UN, IB, O, FI);
        case M: return BRTOR(IB);
        case B: return BRTOR(IB);
        case UN: return BRTOR(UN);
        default: return REL0;
        }
    case IE:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(MI);
        case IMI: return BRADD4(IMI, OI, D, F);
        case OI: return BRADD2(OI, IMI);
        case F: return BRTOR(IMI);
        case D: return BRTOR(IMI);
        case IBI: return BRTOR(IBI);
        case IE: return BRADD4(S, EQ, IE, SI);
        case SI: return BRADD2(IE, SI);
        case EQ: return BRTOR(IE);
        case S: return BRTOR(IE);
        case IB: return BRADD7(B, IM, DI, M, IB, O, FI);
        case DI: return BRADD3(IB, DI, IM);
        case FI: return BRADD2(IB, IM);
        case O: return BRADD2(IB, IM);
        case IM: return BRADD5(IM, DI, IB, O, FI);
        case M: return BRTOR(IB);
        case B: return BRTOR(IB);
        case UN: return BRTOR(UN);
        default: return REL0;
        }
    case UN:
        switch (br2)
        {
        case BI: return BRADD3(BI, UN, IBI);
        case IBI: return BRADD8(D, IBI, MI, BI, F, OI, UN, IMI);
        case MI: return BRADD2(UN, IBI);
        case OI: return BRADD2(UN, IBI);
        case F: return BRADD2(UN, IBI);
        case D: return BRADD2(UN, IBI);
        case IMI: return BRADD2(UN, IBI);
        case UN: return REL_ALL;
        case SI: return BRTOR(UN);
        case EQ: return BRTOR(UN);
        case S: return BRTOR(UN);
        case IE: return BRTOR(UN);
        case DI: return BRTOR(UN);
        case FI: return BRTOR(UN);
        case O: return BRTOR(UN);
        case IM: return BRTOR(UN);
        case M: return BRTOR(UN);
        case B: return BRTOR(UN);
        case IB: return BRTOR(UN);
        default: return REL0;
        }
    case IB:
        switch (br2)
        {
        case BI: return BRADD9(MI, BI, IE, IM, DI, OI, SI, IMI, IB);
        case IBI: return BRADD14(D, IBI, IE, F, IM, S, DI, OI, SI, IMI, IB, EQ, O, FI);
        case MI: return BRADD4(IM, IB, IE, IMI);
        case OI: return BRADD4(IM, IB, IE, IMI);
        case F: return BRADD4(IM, IB, IE, IMI);
        case D: return BRADD4(IM, IB, IE, IMI);
        case IMI: return BRADD4(IM, IB, IE, IMI);
        case UN: return BRADD8(B, IM, DI, M, UN, IB, O, FI);
        case SI: return BRTOR(IB);
        case EQ: return BRTOR(IB);
        case S: return BRTOR(IB);
        case IE: return BRTOR(IB);
        case DI: return BRTOR(IB);
        case FI: return BRTOR(IB);
        case O: return BRTOR(IB);
        case IM: return BRTOR(IB);
        case M: return BRTOR(IB);
        case B: return BRTOR(IB);
        case IB: return BRTOR(IB);
        default: return REL0;
        }
    case IBI:
        switch (br2)
        {
        case BI: return BRTOR(BI);
        case MI: return BRTOR(BI);
        case IMI: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case OI: return BRADD2(BI, IBI);
        case F: return BRTOR(IBI);
        case D: return BRTOR(IBI);
        case IBI: return BRTOR(IBI);
        case IE: return BRADD7(D, IBI, MI, BI, F, OI, IMI);
        case SI: return BRADD2(BI, IBI);
        case EQ: return BRTOR(IBI);
        case S: return BRTOR(IBI);
        case IB: return REL_ALL;
        case DI: return BRADD3(BI, UN, IBI);
        case FI: return BRADD2(UN, IBI);
        case O: return BRADD2(UN, IBI);
        case IM: return BRADD8(D, IBI, MI, BI, F, OI, UN, IMI);
        case M: return BRTOR(UN);
        case B: return BRTOR(UN);
        case UN: return BRTOR(UN);
        default: return REL0;
        }
    case IM:
        switch (br2)
        {
        case BI: return BRADD5(MI, BI, DI, OI, SI);
        case MI: return BRADD3(DI, OI, SI);
        case IMI: return BRADD12(D, IE, F, IM, S, DI, OI, SI, IMI, EQ, O, FI);
        case OI: return BRADD6(IE, IM, DI, OI, SI, IMI);
        case F: return BRADD3(IM, IE, IMI);
        case D: return BRADD3(IM, IE, IMI);
        case IBI: return BRADD4(IMI, IE, IBI, IM);
        case IE: return BRADD4(IM, DI, O, FI);
        case SI: return BRADD2(DI, IM);
        case EQ: return BRTOR(IM);
        case S: return BRTOR(IM);
        case IB: return BRADD7(B, IM, DI, M, IB, O, FI);
        case DI: return BRADD3(IB, DI, IM);
        case FI: return BRADD2(IB, IM);
        case O: return BRADD2(IB, IM);
        case IM: return BRADD5(IM, DI, IB, O, FI);
        case M: return BRTOR(IB);
        case B: return BRTOR(IB);
        case UN: return BRADD2(IB, UN);
        default: return REL0;
        }
    default: return REL0;
    }
}
*/
