#include "rels/irel.hpp"

Ispecs::CompLut::CompLut() : table{nullptr}
{
    namespace fs = std::filesystem;
    fs::path path;

    if (!fs::exists(path))
        build();
    table = files::mmap();
}

int Ispecs::CompLut::build() const
{
    FILE *out = fopen(MAP_PATH, "wb");

    if (!out)
    {
        perror(MAP_PATH);
        return -1;
    }

    for (Irel r; r < LOW_N; ++r)
        for (Irel s; s < LOW_N; ++s)
        {
            Irel comp = r.compose(s);

            fwrite(&comp, sizeof(comp), 1, out);
        }

    for (Irel r; r < LOW_N; ++r)
        for (Irel s; s < Irel::REL_N; s += LOW_N)
        {
            Irel comp = r.compose(s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    for (rel_t r = 0; r < LOW_N; r += LOW_N)
        for (rel_t s = 0; s < COMPLUT_NR; ++s)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    for (rel_t r = 0; r < REL_N; r += LOW_N)
        for (rel_t s = 0; s < REL_N; s += LOW_N)
        {
            rel_t comp = relComp2(r, s);

            fwrite(&comp, sizeof comp, 1, out);
        }

    fclose(out);

    return 0;
}