#include "algs/palg.h"
#include "sets/pset.h"
#include "rels/bprel.h"
#include "rels/prel.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _WIN32
    #include <strings.h>
#else
    #define strcasecmp _stricmp
#endif

static const prel_t point_alg[PALG_N][PREL_N] = {
    [PALG_GAMMA_A] = {
        PREL_ALL,
        BPRTOPR(PB),
        BPRTOPR(PUN),
        BPRTOPR(PEQ),
        BPRADD2(PB, PEQ),
        BPRADD2(PB, PBI),
        BPRADD2(PB, PUN),
        BPRADD2(PEQ, PUN),
        BPRADD3(PB, PEQ, PBI),
        BPRADD3(PB, PUN, PBI),
        BPRADD3(PB, PEQ, PUN),
    },
    [PALG_GAMMA_B] = {
        PREL_ALL,
        BPRTOPR(PB),
        BPRTOPR(PEQ),
        BPRADD2(PB, PEQ),
        BPRADD2(PB, PBI),
        BPRADD3(PB, PEQ, PBI),
        BPRADD3(PB, PUN, PBI),
    },
    [PALG_GAMMA_D] = {
        PREL_ALL,
        BPRTOPR(PUN),
        BPRTOPR(PEQ),
        BPRADD2(PB, PUN),
        BPRADD2(PEQ, PUN),
        BPRADD3(PB, PUN, PBI),
        BPRADD3(PB, PEQ, PUN),
    },
    [PALG_GAMMA_E] = {
        PREL_ALL,
        BPRTOPR(PB),
        BPRTOPR(PUN),
        BPRTOPR(PEQ),
        BPRADD2(PB, PEQ),
        BPRADD2(PB, PBI),
        BPRADD2(PB, PUN),
        BPRADD3(PB, PEQ, PBI),
        BPRADD3(PB, PUN, PBI),
    },
    [PALG_DELTA_B] = {PREL_ALL, BPRADD2(PB, PBI), BPRADD3(PB, PEQ, PBI), BPRADD3(PB, PUN, PBI)},
    [PALG_DELTA_C] = {
        PREL_ALL,
        BPRTOPR(PEQ),
        BPRADD2(PB, PEQ),
        BPRADD2(PEQ, PUN),
        BPRADD3(PB, PEQ, PBI),
        BPRADD3(PB, PEQ, PUN),
    },
    [PALG_DELTA_D] = {
        PREL_ALL,
        BPRTOPR(PUN),
        BPRADD2(PB, PUN),
        BPRADD2(PEQ, PUN),
        BPRADD3(PB, PUN, PBI),
        BPRADD3(PB, PEQ, PUN),
    },
    [PALG_DELTA_E] = {PREL_ALL, BPRADD3(PB, PUN, PBI)},
    [PALG_OMEGA_A1] = {PREL_ALL, BPRADD3(PEQ, PB, PBI)},
    [PALG_OMEGA_A2] = {PREL_ALL, BPRTOPR(PB)},
    [PALG_OMEGA_B1] = {PREL_ALL, BPRADD2(PEQ, PUN), BPRADD2(PB, PBI)},
    [PALG_OMEGA_B2] = {PREL_ALL, BPRADD2(PEQ, PUN), BPRADD3(PB, PBI, PEQ)},
    [PALG_OMEGA_C] = {PREL_ALL, BPRTOPR(PUN)},
    [PALG_OMEGA_D] = {PREL_ALL, BPRTOPR(PB), BPRADD3(PB, PEQ, PBI)},
    [PALG_OMEGA_E] = {PREL_ALL, BPRTOPR(PEQ)},
    [PALG_ZETA_A] = {PREL_ALL, BPRTOPR(PUN)},
    [PALG_ZETA_B] = {PREL_ALL, BPRADD3(PUN, PB, PBI)},
    [PALG_ZETA_C1] = {PREL_ALL, BPRADD2(PB, PBI)},
    [PALG_ZETA_C2] = {PREL_ALL, BPRADD3(PB, PEQ, PBI)},
    [PALG_ZETA_C3] = {PREL_ALL, BPRADD2(PB, PBI), BPRADD3(PB, PEQ, PBI)},
    [PALG_ZETA_D] = {PREL_ALL, BPRADD2(PEQ, PUN)},
};

pset_t palgInit(palgname_t name)
{
    pset_t palg = psetInit();

    if (name < PALG_N)
        for (int i = 0; point_alg[name][i]; ++i)
            psetIns(&palg, point_alg[name][i]);

    return palg;
}

#define case_name(x)     \
    case GLUE(PALG_, x): \
        return STRING(x)

#define compare_and_ret(x)            \
    if (!strcasecmp(name, STRING(x))) \
    return GLUE(PALG_, x)

const char *palgName(palgname_t name)
{
    switch (name)
    {
        case_name(GAMMA_A);
        case_name(GAMMA_B);
        case_name(GAMMA_D);
        case_name(GAMMA_E);
        case_name(DELTA_B);
        case_name(DELTA_C);
        case_name(DELTA_D);
        case_name(DELTA_E);
        case_name(OMEGA_A1);
        case_name(OMEGA_A2);
        case_name(OMEGA_B1);
        case_name(OMEGA_B2);
        case_name(OMEGA_C);
        case_name(OMEGA_D);
        case_name(OMEGA_E);
        case_name(ZETA_A);
        case_name(ZETA_B);
        case_name(ZETA_C1);
        case_name(ZETA_C2);
        case_name(ZETA_C3);
        case_name(ZETA_D);
    default: return "Ø";
    }
}

palgname_t palgFromStr(const char *name)
{
    compare_and_ret(GAMMA_A);
    compare_and_ret(GAMMA_B);
    compare_and_ret(GAMMA_D);
    compare_and_ret(GAMMA_E);
    compare_and_ret(DELTA_B);
    compare_and_ret(DELTA_C);
    compare_and_ret(DELTA_D);
    compare_and_ret(DELTA_E);
    compare_and_ret(OMEGA_A1);
    compare_and_ret(OMEGA_A2);
    compare_and_ret(OMEGA_B1);
    compare_and_ret(OMEGA_B2);
    compare_and_ret(OMEGA_C);
    compare_and_ret(OMEGA_D);
    compare_and_ret(OMEGA_E);
    compare_and_ret(ZETA_A);
    compare_and_ret(ZETA_B);
    compare_and_ret(ZETA_C1);
    compare_and_ret(ZETA_C2);
    compare_and_ret(ZETA_C3);
    compare_and_ret(ZETA_D);

    return PALG_N;
}

void palgNotFound(void)
{
    fprintf(stderr, "Algebra non trovata! Algebre disponibili:\n");

    for (palgname_t i = (palgname_t)0; i < PALG_N; ++i)
        fprintf(stderr, "%s ", palgName(i));

    putc('\n', stderr);
}
