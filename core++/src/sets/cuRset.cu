#include "lut.h"
#include "sets/rset.h"

#include <cuda_runtime.h>
#include <device_atomic_functions.h>
#include <device_launch_parameters.h>
#include <stdio.h>

#define N_STREAM 3
#define BLKSZ 32U

__constant__ static rel_t *dev_map;
static rel_t *cuComp_map;
static cudaStream_t stream[N_STREAM];

__device__ extern void __syncthreads();

__device__ static inline rel_t cuComp(rel_t r1, rel_t r2)
{
#ifdef BA
    rel_t l1{COMPLUT_GETLEFT(r1)}, l2{COMPLUT_GETLEFT(r2)}, r;

    r1 = COMPLUT_GETRIGHT(r1);
    r2 = COMPLUT_GETRIGHT(r2);
    r = RELADD(dev_map[r1 * COMPLUT_NR + r2], dev_map[COMPLUT_RL + r1 * COMPLUT_NL + l2]);
    r = RELADD(r, dev_map[COMPLUT_LR + l1 * COMPLUT_NR + r2]);

    return RELADD(r, dev_map[COMPLUT_LL + l1 * COMPLUT_NL + l2]);
#else
    return dev_map[r1 * (REL_NIA >> BABR_N) + (r2 >> BABR_N)];
#endif
}

__device__ static inline rel_t cuPath(rel_t r1, rel_t r2, rel_t r3)
{
    return RELSEC(cuComp(r1, r2), r3);
}

template<closeflags_t flags>
__global__ static void cuRsetClose_ker(uint8_t *map, rel_t *fst, uint n_fst, rel_t *snd, uint n_snd)
{
    uint i = blockIdx.x * blockDim.x + threadIdx.x;
    uint j = blockIdx.y * blockDim.y + threadIdx.y;

    if (i >= n_fst || j >= n_snd)
        return;

    rel_t f = fst[i];
    rel_t s = snd[j];

    if (flags & CLOSE_CONV)
        if (!s)
            map[RELCONV(f)] = 1;

    if (flags & CLOSE_SEC)
        map[RELSEC(f, s)] = 1;

    if (flags & CLOSE_UNION)
        map[RELADD(f, s)] = 1;

    if (flags & CLOSE_WCOMP)
        map[cuComp(f, s)] = 1;

    if (flags & CLOSE_SCOMP)
        for (int k = 0; k < n_snd; ++k)
            map[cuPath(f, s, snd[k])] = 1;
}

extern "C" int cuLutInit(int rank, const rel_t *comp_map)
{
    int ndev = 1, idev;

    cudaGetDeviceCount(&ndev);
    idev = rank % ndev;
    cudaSetDevice(idev);

    cudaMalloc((void **)&cuComp_map, COMPLUT_N * sizeof(*cuComp_map));
    cudaMemcpy(cuComp_map, comp_map, COMPLUT_N * sizeof(*cuComp_map), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(dev_map, &cuComp_map, sizeof(cuComp_map), 0, cudaMemcpyHostToDevice);

    for (int i = 0; i < N_STREAM; ++i)
        cudaStreamCreate(stream + i);

    return idev;
}

extern "C" void cuLutFinish(void)
{
    cudaFree(cuComp_map);

    for (int i = 0; i < N_STREAM; ++i)
        cudaStreamDestroy(stream[i]);
}

extern "C" cudaStream_t cuGetStream(int i)
{
    return stream[i];
}

#define closekercase(flag)                                                               \
    case flag:                                                                           \
        cuRsetClose_ker<flag><<<grdsz, blksz, 0, stream>>>(map, fst, n_fst, snd, n_snd); \
        break

extern "C" void cuRsetClose(uint8_t *map, rel_t *fst, uint n_fst, rel_t *snd, uint n_snd,
                            cudaStream_t stream, closeflags_t flags)
{
    // ogni blocco è 32x32, la griglia ha come dimensione il numero di blocchi
    const dim3 blksz{BLKSZ, BLKSZ};
    dim3 grdsz{(n_fst / BLKSZ) + 1U, (n_snd / BLKSZ) + 1U};

    switch (flags)
    {
        // VS Code: errori di Intellisense che non riconosce <<<>>>
        closekercase(CLOSE_CONV);
        closekercase(CLOSE_SEC);
        closekercase(CLOSE_WCOMP);
        closekercase(CLOSE_UNION);
        closekercase(CLOSE_CONV | CLOSE_SEC);
        closekercase(CLOSE_CONV | CLOSE_WCOMP);
        closekercase(CLOSE_CONV | CLOSE_UNION);
        closekercase(CLOSE_SEC | CLOSE_WCOMP);
        closekercase(CLOSE_SEC | CLOSE_UNION);
        closekercase(CLOSE_WCOMP | CLOSE_UNION);
        closekercase(CLOSE_FULL);
        closekercase(CLOSE_CONV | CLOSE_SEC | CLOSE_UNION);
        closekercase(CLOSE_SEC | CLOSE_WCOMP | CLOSE_UNION);
        closekercase(CLOSE_FULL | CLOSE_UNION);
        closekercase(CLOSE_SCOMP);
    default: break;
    }
}
