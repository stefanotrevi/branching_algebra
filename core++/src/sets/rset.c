#include "sets/rset.h"
#include "rels/cprel.h"
#include "rels/rel.h"
#include "sets/cuRset.cuh"
#include "sets/pset.h"

#include <cuda_runtime.h>
#include <stdlib.h>
#include <string.h>
#include X86INTRIN_H

static const rel_t corner_sets[][16] = {
    // Insiemi di Drakengen&Jonsson
    {BRADD5(B, DI, FI, M, O), BRADD5(B, D, M, O, S), BRADD5(D, DI, F, OI, SI)},
    {BRADD5(B, DI, FI, M, O), BRADD5(B, D, M, O, S), BRADD5(DI, FI, O, OI, SI)},
    {BRADD2(B, BI), BRADD2(O, OI)},
    {BRADD2(B, BI), BRADD4(M, MI, O, OI)},
    {BRADD2(M, MI), BRADD6(B, BI, F, FI, S, SI)},
    {BRADD2(O, OI), BRADD4(B, BI, M, MI)},
};

rset_t rsetInit(void)
{
    return (rset_t){.data = calloc(REL_N, sizeof(uint8_t))};
}

void rsetDel(rset_t rset)
{
    if (rset.data)
        free(rset.data);
}

rset_t rsetInitCopy(rset_t set)
{
    rset_t set_new = rsetInit();

    memcpy(set_new.data, set.data, REL_N * sizeof *set.data);
    set_new.num = set.num;

    return set_new;
}

rset_t rsetInitByRaw(uint8_t *data, uint dim)
{
    rset_t set = rsetInit();

    for (uint i = 0; i < dim; ++i)
        rsetPut(&set, i, data[i]);

    return set;
}

rset_t rsetInitByArr(const rel_t *array, int n)
{
    rset_t set = rsetInit();

    for (int i = 0; i < n; ++i)
        rsetIns(&set, array[i]);

    return set;
}

rset_t rsetInitByPset(pset_t pset)
{
    rset_t rset = rsetInit();
    prel_t red[PREL_N];
    int n = psetToArray(pset, red);

    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                for (int l = 0; l < n; ++l)
                {
                    cprel_t cpr = cprelInit();

                    cpr = cprelPut(cpr, AMBM, red[i]);
                    cpr = cprelPut(cpr, AMBP, red[j]);
                    cpr = cprelPut(cpr, APBM, red[k]);
                    cpr = cprelPut(cpr, APBP, red[l]);

                    rsetIns(&rset, cprelToRel(cpr));
                }

    return rset;
}

static void rsetCloseUnion(rset_t *rset)
{
    rel_t *done = malloc(REL_N * sizeof *done);
    rel_t *todo = malloc(REL_N * sizeof *todo);
    rel_t *todo_new = malloc(REL_N * sizeof *todo_new);
    uint8_t *set = rset->data;
    int done_n = 0;
    int todo_n = rsetToArr(*rset, todo);

    while (todo_n)
    {
        int new_n = 0;

        for (int i = 0; i < todo_n; ++i)
            for (int j = 0; j < todo_n; ++j)
            {
                rel_t uni = relAdd(todo[i], todo[j]);

                if (!set[uni])
                {
                    int val_i = (set[todo[i]] == 2);
                    int val_j = (set[todo[j]] == 2);

                    if (!(val_i && val_j))
                    {
                        set[uni] = (uint8_t)(1 + (val_i | val_j));
                        todo_new[new_n++] = uni;
                    }
                }
            }
        for (int i = 0; i < todo_n; ++i)
            for (int j = 0; j < done_n; ++j)
            {
                rel_t uni = relAdd(todo[i], done[j]);

                if (!set[uni])
                {
                    int val_i = (set[todo[i]] == 2);
                    int val_j = (set[done[j]] == 2);

                    if (!(val_i && val_j))
                    {

                        set[uni] = (uint8_t)(1 + (val_i | val_j));
                        todo_new[new_n++] = uni;
                    }
                }
            }

        memcpy(done + done_n, todo, todo_n * sizeof *todo);
        memcpy(todo, todo_new, new_n * sizeof *todo_new);
        done_n += todo_n;
        todo_n = new_n;
    }

    rset->num = done_n;
    free(done);
    free(todo);
    free(todo_new);
}

rset_t rsetInitByPdisj(pset_t pset1, pset_t pset2)
{
    rset_t rset1 = rsetInitByPset(pset1);
    rset_t rset2 = rsetInitByPset(pset2);

    // Marchiamo gli elementi del primo insieme
    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (rsetGet(rset1, r))
            rsetPut(&rset1, r, 2);

    rsetMerge(&rset1, rset2);
    // Uso una versione modificata della chiusura per unione
    rsetCloseUnion(&rset1);

    // Rimettiamo i 2 a 1
    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (rsetGet(rset1, r) == 2)
            rsetIns(&rset1, r);

    rsetDel(rset2);

    return rset1;
}

rset_t rsetInitByFile(FILE *f)
{
    rset_t set = rsetInit();

    while (!feof(f))
    {
        rel_t r;

        if (fscanf(f, "%u", &r) == 1)
            rsetIns(&set, r);
        while (fgetc(f) != '\n' && !feof(f))
            ;
    }

    return set;
}

rset_t rsetInitByFname(const char *fname)
{
    FILE *f = fopen(fname, "rt");
    rset_t set = {0};

    if (!f)
        return set;

    set = rsetInitByFile(f);
    fclose(f);

    return set;
}

uint8_t rsetGet(rset_t rset, rel_t r)
{
    return rset.data[r];
}

void rsetPut(rset_t *rset, rel_t r, uint8_t val)
{
    rset->num += !!val - !!rset->data[r];
    rset->data[r] = val;
}

void rsetIns(rset_t *rset, rel_t r)
{
    rsetPut(rset, r, 1);
}

void rsetRem(rset_t *rset, rel_t r)
{
    rsetPut(rset, r, 0);
}

void rsetPrint(FILE *f, rset_t rset)
{
    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (rsetGet(rset, r))
            relPrint(f, r);
}

void rsetWrite(FILE *f, rset_t rset)
{
    fwrite(rset.data, sizeof *rset.data, REL_N, f);
}

int rsetToArr(rset_t rset, rel_t *array)
{
    int n = 0;

    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (rsetGet(rset, r))
            array[n++] = r;

    return n;
}

int rsetMerge(rset_t *dst, rset_t src)
{
    for (uint i = 0; i < REL_N; ++i)
        if (rsetGet(src, i))
            rsetPut(dst, i, rsetGet(src, i));

    return 0;
}

int rsetSec(rset_t *dst, rset_t src)
{
    for (uint i = 0; i < REL_N; ++i)
        if (!(rsetGet(src, i) && rsetGet(*dst, i)))
            rsetRem(dst, i);

    return 0;
}

int rsetIsSuper(rset_t set1, rset_t set2)
{
    for (uint i = 0; i < REL_N; ++i)
        if (rsetGet(set2, i) > rsetGet(set1, i))
            return 0;

    return 1;
}

// Calcola un passo per la chiusura di un insieme di relazioni
static uint rsetClosePart(uint8_t *set, uint8_t *new_set, rel_t *todo, uint nto,
                          rel_t *done, uint ndo, rel_t *todon, closeflags_t flags)
{
    uint new_n = 0;

    cudaMemset(new_set, 0, REL_N * sizeof *new_set);
    cuRsetClose(new_set, todo, nto, todo, nto, cudaStreamDefault, flags);
    cuRsetClose(new_set, todo, nto, done, ndo, cudaStreamDefault, flags);
    cuRsetClose(new_set, done, ndo, todo, nto, cudaStreamDefault, flags);
    cudaDeviceSynchronize();

    for (rel_t r = REL0; r < REL_N; r += REL_STEP)
        if (new_set[r] > set[r])
        {
            set[r] = 1;
            todon[new_n++] = r;
        }

    return new_n;
}

int rsetClose(rset_t *set, closeflags_t flags)
{
    /* 
     * Il calcolo della chiusura si basa su 3 array ed una procedura iterativa su di essi:
     * done: contiene le relazioni "fossilizzate", cioè quelle che appartengono all'insieme
     * e che sono già state processate (inizialmente, nessuna).
     * to_do: contiene le relazioni che appartengono all'insieme, ma che non sono ancora state
     * processate (inizialmente, tutte quelle dell'insieme base).
     * todo_new: contiene le relazioni che vengono scoperte fra un'iterazione e l'altra.
    */
    uint n_done = 0, n_todo;
    rel_t *todo, *done, *todo_new;
    uint8_t *new_set, *set_data = set->data;
    int print = flags & CLOSE_PRINT, finish = 0;

    flags &= ~CLOSE_PRINT; // non serve più questa flag

    cudaMallocManaged((void **)&todo, REL_N * sizeof *todo, cudaMemAttachGlobal);
    cudaMallocManaged((void **)&done, REL_N * sizeof *done, cudaMemAttachGlobal);
    cudaMallocManaged((void **)&todo_new, REL_N * sizeof *todo_new, cudaMemAttachGlobal);
    cudaMallocManaged((void **)&new_set, REL_N * sizeof *new_set, cudaMemAttachGlobal);

    // ricaviamo le relazioni
    n_todo = rsetToArr(*set, todo);

    // Se degenera, non c'è bisogno di altri passi
    for (int i = 0; n_todo && (n_done + n_todo) < REL_N; ++i)
    {
        uint n_new = rsetClosePart(set_data, new_set, todo, n_todo, done, n_done, todo_new, flags);

        if (print)
            printf("Relazioni: %u (passo %d)\n", n_done + n_todo, i);

        cudaMemcpy(done + n_done, todo, n_todo * sizeof *todo, cudaMemcpyDefault);
        cudaMemcpy(todo, todo_new, n_new * sizeof *todo_new, cudaMemcpyDefault);

        n_done += n_todo;
        n_todo = n_new;
    }

    n_done += n_todo;
    set->num = n_done;

    if (print)
        printf("Relazioni: %u (%s)\n", n_done, n_done == REL_N ? "degenere" : "chiuso");

    cudaFree(todo);
    cudaFree(done);
    cudaFree(todo_new);
    cudaFree(new_set);

    return finish ? -1 : (int)n_done;
}

// Funzione per simulare compressstore (+popcount) su AVX2
static inline int AVX2_COMPSTORE(int *dst, __m256i src, uint64_t msk)
{
    int cnt;

    // trasporta ogni bit del primo byte in un byte (maschera "gratis" per i byte successivi!)
    msk = _pdep_u64(msk, 0x0101010101010101U);
    cnt = _popcnt64(msk);
    // riempiamo i byte attivi
    msk *= 0xFFULL;
    // creiamo la nostra maschera di permutazione
    msk = _pext_u64(0x0706050403020100U, msk);
    // permutiamo
    src = _mm256_permutevar8x32_epi32(src, _mm256_cvtepu8_epi32(_mm_cvtsi64_si128(msk)));
    // non possiamo garantire l'allineamento, usiamo store*u*
    _mm256_storeu_si256((__m256i *)dst, src);

    return cnt;
}

static uint rsetClosePartQuick(uint8_t *sup, uint8_t *hset, uint8_t *dset, rel_t *done, uint n_done,
                               rel_t *todo, uint n_todo, rel_t *todo_new)
{
    int *todon_p = (int *)todo_new;
    __m256i v_base = _mm256_set_epi32(7 * REL_STEP, 6 * REL_STEP, 5 * REL_STEP, 4 * REL_STEP,
                                      3 * REL_STEP, 2 * REL_STEP, 1 * REL_STEP, 0 * REL_STEP);

    cudaMemsetAsync(dset, 0, REL_N * sizeof *dset, cuGetStream(0));
    cuRsetClose(dset, todo, n_todo, todo, n_todo, cuGetStream(0), CLOSE_FULL);
    cuRsetClose(dset, todo, n_todo, done, n_done, cuGetStream(1), CLOSE_FULL);
    cuRsetClose(dset, done, n_done, todo, n_todo, cuGetStream(2), CLOSE_FULL);
    cudaMemcpy(hset, dset, REL_N * sizeof *dset, cudaMemcpyDeviceToHost);

    /* 
     * Rendiamo il seguente codice circa 7-8 volte più veloce su AVX2. Su AVX512 si potrebbe
     * fare ancora meglio con l'istruzione VPCOMPRESSD, ma non abbiamo accesso a CPU AVX512.
    for (rel_t r = 0; r < REL_N; r += REL_STEP)
        if (hset[r] > sup[r])
        {
            sup[r] = 1;
            todo_new[new_n++] = r;
        }
    */
    for (rel_t r = REL0; r < REL_N; r += REL_STEP << 5) // confronto 32 elementi per volta
    {
        // carichiamo 8 elementi dalle due mappe
        __m256i set_v = _mm256_load_si256((const __m256i *)(sup + r));
        __m256i newset_v = _mm256_load_si256((const __m256i *)(hset + r));
        // confrontiamoli e ricaviamo la maschera di confronto
        __m256i cmp_v = _mm256_cmpgt_epi8(newset_v, set_v);
        uint64_t msk = (uint)_mm256_movemask_epi8(cmp_v); // cast per forzare zero-extend

        // Trovo gli elementi impostati sia in set che in new_set, e aggiorno set
        set_v = _mm256_or_si256(set_v, newset_v);
        _mm256_store_epi32(sup + r, set_v);

        // sizeof(rel_t) = 4, sizeof(uint8_t) = 1 ==> non ci stanno tutti e 32 in una volta sola
        // elementi 0-8
        todon_p += AVX2_COMPSTORE(todon_p, v_base, msk);
        v_base = _mm256_add_epi32(v_base, _mm256_set1_epi32(REL_STEP << 3));
        msk >>= 8;

        // elementi 8-16
        todon_p += AVX2_COMPSTORE(todon_p, v_base, msk);
        v_base = _mm256_add_epi32(v_base, _mm256_set1_epi32(REL_STEP << 3));
        msk >>= 8;

        // elementi 16-24
        todon_p += AVX2_COMPSTORE(todon_p, v_base, msk);
        v_base = _mm256_add_epi32(v_base, _mm256_set1_epi32(REL_STEP << 3));
        msk >>= 8;

        // elementi 24-32
        todon_p += AVX2_COMPSTORE(todon_p, v_base, msk);
        v_base = _mm256_add_epi32(v_base, _mm256_set1_epi32(REL_STEP << 3));
    }

    return (uint)(todon_p - (int *)todo_new);
}

int rsetCloseAndCheck(uint8_t *sup, uint8_t *hset, uint8_t *dset, rel_t *done,
                      rel_t *todo, uint n_todo, rel_t *todo_new)
{
    uint n_done = 0;
    int np = 0;

    while (n_todo && !np)
    {
        uint n_new = rsetClosePartQuick(sup, hset, dset, done, n_done, todo, n_todo, todo_new);

        cudaMemcpyAsync(done + n_done, todo, n_todo * sizeof *todo, cudaMemcpyDeviceToDevice, cuGetStream(0));
        cudaMemcpyAsync(todo, todo_new, n_new * sizeof *todo, cudaMemcpyHostToDevice, cuGetStream(0));
        n_done += n_todo;
        n_todo = n_new;

        // se contiene tutte le relazioni di almeno un insieme corner, è NP-completo
        for (int i = 0; !np && i < (int)lengthof(corner_sets); ++i)
        {
            int j;

            for (j = 0; corner_sets[i][j]; ++j)
                if (sup[corner_sets[i][j]])
                    ++np;
            np = (np == j);
        }
    }

    return (int)(np ? -n_done : n_done);
}


int rsetCheckCorner(rset_t set)
{
    int np = 0;

    for (int i = 0; !np && i < (int)lengthof(corner_sets); ++i)
    {
        int j;

        for (j = 0; corner_sets[i][j]; ++j)
            if (rsetGet(set, corner_sets[i][j]))
                ++np;

        np = (np == j);
    }

    return !np;
}
