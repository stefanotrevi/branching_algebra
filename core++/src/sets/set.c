#include "sets/set.h"

#include <stdlib.h>
#include <string.h>

set_t setInit(uint dim)
{
    return (set_t){.data = calloc(dim, sizeof(uint8_t)), .dim = dim};
}

set_t setInitCopy(set_t set)
{
    set_t set_new = setInit(set.dim);

    setCopy(&set_new, set);

    return set_new;
}

set_t setInitRaw(uint8_t *data, uint dim)
{
    set_t set = setInit(dim);

    for (uint i = 0; i < dim; ++i)
        setPut(&set, i, data[i]);

    return set;
}

set_t setEmbedRaw(uint8_t *data, uint dim)
{
    return (set_t){.data = data, .dim = dim};
}

void setCopy(set_t *dst, set_t src)
{
    memcpy(dst->data, src.data, src.dim * sizeof *src.data);
    dst->nset = src.nset;
}

uint8_t setGet(set_t set, uint idx)
{
    return set.data[idx];
}

void setPut(set_t *set, uint idx, uint8_t val)
{
    set->nset += !!val - !!set->data[idx];
    set->data[idx] = val;
}

void setIns(set_t *set, uint idx)
{
    setPut(set, idx, 1);
}

void setRem(set_t *set, uint idx)
{
    setPut(set, idx, 0);
}

void setPrint(FILE *f, set_t set, int bin)
{
    if (bin)
        fwrite(set.data, sizeof *set.data, set.dim, f);
    else
        for (uint i = 0; i < set.dim; ++i)
            fprintf(f, "%u: %d\n", i, setGet(set, i));
}

void setEmpty(set_t set)
{
    memset(set.data, 0, set.dim * sizeof *set.data);
}

void setDel(set_t set)
{
    if (set.data)
        free(set.data);
}

int setMerge(set_t *dst, set_t src)
{
    if (dst->dim < src.dim)
        return -1;

    for (uint i = 0; i < src.dim; ++i)
        if (setGet(src, i))
            setPut(dst, i, setGet(src, i));

    return 0;
}

int setSec(set_t *dst, set_t src)
{
    uint small = src.dim <= dst->dim ? src.dim : dst->dim;

    for (uint i = 0; i < small; ++i)
        if (!(setGet(src, i) && setGet(*dst, i)))
            setRem(dst, i);

    for (uint i = small; i < dst->dim; ++i)
        setRem(dst, i);

    return 0;
}

int setIsSuper(set_t set1, set_t set2)
{
    if (set1.dim < set2.dim)
        return 0;

    for (uint i = 0; i < set1.dim; ++i)
        if (setGet(set2, i) > setGet(set1, i))
            return 0;

    return 1;
}
