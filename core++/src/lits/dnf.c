#include "lits/dnf.h"
#include "rels/bprel.h"
#include "rels/prel.h"
#include "rels/brel.h"
#include "rels/rel.h"
#include "rels/cprel.h"

dnf_t dnfInit(void)
{
    return (dnf_t){{0}};
}

dnf_t relToDnf(rel_t r)
{
    dnf_t dnf = dnfInit();

    return *dnfInsert(&dnf, r);
}

dnf_t *dnfInsert(dnf_t *dnf, rel_t r)
{
    for (brel_t br = UN; br <= SI; ++br)
        if (relHas(r, br))
            dnfInsertbr(dnf, br);

    return dnf;
}

dnf_t *dnfInsertbr(dnf_t *dnf, brel_t br)
{
    dnf->f[dnf->sz++] = brelTocprel(br);

    return dnf;
}

size_t DNFToStr(char *s, dnf_t dnf, int iter)
{
    char buff[128];
    size_t n = sprintf(s, "eq%d = ", iter), part;

    s += n;
    if (!dnf.sz)
    {
        s += part = sprintf(s, "(1");
        n += part;
    }
    // trasformiamo in letterali la mappatura
    for (int i = 0; i < dnf.sz; ++i)
    {
        int first = 1;

        *(s++) = '(';
        ++n;
        for (pcouples_t j = (pcouples_t)0; j < COUPLES_N; ++j)
            for (bprel_t br = (bprel_t)0; br < BPREL_N; ++br)
                if (cprelHas(dnf.f[i], br, j))
                {
                    if (!first) // primo letterale?
                        s += part = sprintf(s, " & "), n += part;
                    else
                        first = 0;
                    // salvo la clausola sul file
                    s += part = sprintf(s, "%s", bprelToLitStr(buff, br, j));
                    n += part;
                }
        if (i != dnf.sz - 1)
            s += part = sprintf(s, ") | "), n += part;
    }

    return n + sprintf(s, ");\n");
}
