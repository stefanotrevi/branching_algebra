#include "lits/lit.h"
#include "rels/brel.h"
#include "rels/prel.h"
#include "rels/rel.h"

#include <ctype.h>
#include X86INTRIN_H

#undef _NODISCARD
#define _NODISCARD

#include <string.h>

lit_t pcToLit(pcouples_t pc)
{
    return (lit_t)(1U << pc);
}

int litHas(lit_t lit, pcouples_t pc)
{
    return lit & pcToLit(pc);
}

lit_t litAdd(lit_t lit1, lit_t lit2)
{
    return (lit_t)(lit1 | lit2);
}

lit_t litIns(lit_t lit, pcouples_t pc)
{
    return litAdd(lit, pcToLit(pc));
}

lit_t litMask(lit_t lit, lit_t mask)
{
    return (lit_t)(lit & (lit_t)~mask);
}

lit_t litSub(lit_t lit, pcouples_t pc)
{
    return litMask(lit, pcToLit(pc));
}

rel_t litToRel(lit_t lit, blit_t li, clsidx_t ci)
{
    rel_t r = REL0;

    if (!lit)
        return REL_ALL;

    for (pcouples_t pc = (pcouples_t)0; pc < COUPLES_N; ++pc)
        if (litHas(lit, pc))
            r = relAdd(r, blitToRel(li, pc, ci));

    return r;
}

clit_t clInit(void)
{
    return 0U;
}

int clCount(clit_t cl)
{
    return _popcnt32(cl);
}

clit_t clShift(blit_t i)
{
    return i * COUPLES_N;
}

clit_t litToCl(lit_t l, blit_t i)
{
    return (clit_t)l << clShift(i);
}

lit_t clGet(clit_t cl, blit_t i)
{
    return (lit_t)((cl >> clShift(i)) & ALL_COUP);
}

int clHas(clit_t cl, blit_t i, pcouples_t pc)
{
    return litHas(clGet(cl, i), pc);
}

clit_t clAdd(clit_t cl1, clit_t cl2)
{
    return cl1 | cl2;
}

clit_t clPut(clit_t cl, blit_t i, lit_t l)
{
    return clAdd(cl, litToCl(l, i));
}

clit_t clIns(clit_t cl, blit_t i, pcouples_t pc)
{
    return clPut(cl, i, litIns(clGet(cl, i), pc));
}

clit_t clSub(clit_t cl1, clit_t cl2)
{
    return cl1 & ~cl2;
}

clit_t clMask(clit_t cl, blit_t i, lit_t l)
{
    return clSub(cl, litToCl(l, i));
}

clit_t clRem(clit_t cl, blit_t i, pcouples_t pc)
{
    return clMask(cl, i, litSub(clGet(cl, i), pc));
}

clit_t clMax(clit_t cl)
{
    // (A* <= B*)
    if (clHas(cl, LE, AMBP))
        cl = clPut(cl, LE, ALL_COUP); // Se (A- <= B+), valgono tutti gli altri
    else if (clHas(cl, LE, AMBM) || clHas(cl, LE, APBP))
        cl = clIns(cl, LE, APBM); // Se (A- <= B-) o (A+ <= B+), vale anche (A+ <= B-)

    // (B* <= A*), simmetrico a (A* <= B*)
    if (clHas(cl, LEI, APBM))
        cl = clPut(cl, LEI, ALL_COUP);
    else if (clHas(cl, LEI, AMBM) || clHas(cl, LEI, APBP))
        cl = clIns(cl, LEI, AMBP);

    // (A* <= B*) o (B* <= A*) implicano ovviamente (A* = B*)
    cl = clPut(cl, E, litAdd(litAdd(clGet(cl, E), clGet(cl, LE)), clGet(cl, LEI)));

    // (A* </ B*)
    if (clHas(cl, LU, AMBP)) // (A- </ B+) sottointende tutti gli altri
        cl = clPut(cl, LU, ALL_COUP);
    else if (clHas(cl, LU, AMBM) || clHas(cl, LU, APBP))
        cl = clIns(cl, LU, APBM); // Se (A- </ B-) o (A+ </ B+), vale anche (A+ </ B-)

    // (B* </ A*), simmetrico a (A* </ B*)
    if (clHas(cl, LUI, APBM))
        cl = clPut(cl, LUI, ALL_COUP);
    else if (clHas(cl, LUI, AMBM) || clHas(cl, LUI, APBP))
        cl = clIns(cl, LUI, AMBP);

    // (A* </ B*) o (B* </ A*) implicano ovviamente (A* || B*)
    return clPut(cl, U, litAdd(litAdd(clGet(cl, U), clGet(cl, LU)), clGet(cl, LUI)));
}

clit_t clMin(clit_t cl)
{
    clit_t exp = clMax(cl);

    // (A* <= B*)
    if (clHas(cl, LE, AMBP))
        cl = litToCl(pcToLit(AMBP), LE); // Se (A- <= B+), gli altri sono ridondanti
    else if (clHas(cl, LE, AMBM) || clHas(cl, LE, APBP))
        cl = clRem(cl, LE, APBM); // Se (A- <= B-) o (A+ <= B+), (A+ <= B-) è ridondante

    // (B* <= A*), simmetrico a (A* <= B*)
    if (clHas(cl, LEI, APBM))
        cl = litToCl(pcToLit(APBM), LEI);
    else if (clHas(cl, LEI, AMBM) || clHas(cl, LEI, APBP))
        cl = clRem(cl, LEI, AMBP);

    // (A* <= B*) e (B* <= A*) includono (A* = B*). Per mascherare, usiamo la versione espansa
    cl = clPut(clMask(cl, E, ALL_COUP), E,
               litMask(litMask(clGet(cl, E), clGet(exp, LE)), clGet(exp, LEI)));

    // (A* || B*)
    if (clHas(cl, U, APBP)) // Se (A+ || B+), tutti gli altri sono ridondanti
        cl = litToCl(pcToLit(APBP), U);
    else if (clHas(cl, U, AMBP) || clHas(cl, U, APBM))
        cl = clRem(cl, U, AMBM); // Se (A- || B+) o (A+ || B-), (A- || B-) è ridondante

    // (A* </ B*)
    if (clHas(cl, LU, AMBP))
        cl = litToCl(pcToLit(AMBP), LU); // Se (A- </ B+), gli altri sono ridondanti
    else if (clHas(cl, LU, AMBM) || clHas(cl, LU, APBP))
        cl = clRem(cl, LU, APBM); // Se (A- </ B-) o (A+ </ B+), (A+ </ B-) è ridondante

    // (B* </ A*), simmetrico a (A* </ B*)
    if (clHas(cl, LUI, APBM))
        cl = litToCl(pcToLit(APBM), LUI);
    else if (clHas(cl, LUI, AMBM) || clHas(cl, LUI, APBP))
        cl = clRem(cl, LUI, AMBP);

    // (A* </ B*) o (B* </ A*) includono (A* // B*).
    return clPut(clMask(cl, U, ALL_COUP), U,
                 litMask(litMask(clGet(cl, U), clGet(exp, LU)), clGet(exp, LUI)));
}

rel_t clToRel(clit_t cl, clsidx_t ci)
{
    rel_t r = REL0;

    if (!cl)
        return REL_ALL;

    for (blit_t li = (blit_t)0; li < BLIT_N; ++li)
    {
        lit_t l = clGet(cl, li);

        if (l)
            r = relAdd(r, litToRel(l, li, ci));
    }

    return r;
}

size_t clToStr(char *s, clit_t cl, clsidx_t idx)
{
    // mappa di traduzione, anziché caratteri, ritorna stringhe comprensibili
    static const char *const trans_map[BLIT_N][COUPLES_N][L_NUM] = {
        [E] = {
            [AMBM] = {"A- ≠ B-", "A- = B-"},
            [AMBP] = {"A- ≠ B+", "A- = B+"},
            [APBM] = {"A+ ≠ B-", "A+ = B-"},
            [APBP] = {"A+ ≠ B+", "A+ = B+"},
        },
        [LE] = {
            [AMBM] = {"A- ≰ B-", "A- ≤ B-"},
            [AMBP] = {"A- ≰ B+", "A- ≤ B+"},
            [APBM] = {"A+ ≰ B-", "A+ ≤ B-"},
            [APBP] = {"A+ ≰ B+", "A+ ≤ B+"},
        },
        [LEI] = {
            [AMBM] = {"B- ≰ A-", "B- ≤ A-"},
            [AMBP] = {"B+ ≰ A-", "B+ ≤ A-"},
            [APBM] = {"B- ≰ A+", "B- ≤ A+"},
            [APBP] = {"B+ ≰ A+", "B+ ≤ A+"},
        },
        [LU] = {
            [AMBM] = {"A- ⋠ B-", "A- <∥ B-"},
            [AMBP] = {"A- ⋠ B+", "A- <∥ B+"},
            [APBM] = {"A+ ⋠ B-", "A+ <∥ B-"},
            [APBP] = {"A+ ⋠ B+", "A+ <∥ B+"},
        },
        [LUI] = {
            [AMBM] = {"B- ⋠ A-", "B- <∥ A-"},
            [AMBP] = {"B+ ⋠ A-", "B+ <∥ A-"},
            [APBM] = {"B- ⋠ A+", "B- <∥ A+"},
            [APBP] = {"B+ ⋠ A+", "B+ <∥ A+"},
        },
        [U] = {
            [AMBM] = {"A- ~ B-", "A- ∥ B-"},
            [AMBP] = {"A- ~ B+", "A- ∥ B+"},
            [APBM] = {"A+ ~ B-", "A+ ∥ B-"},
            [APBP] = {"A+ ~ B+", "A+ ∥ B+"},
        },
    };
    int first = 1;
    size_t n = 0, part = 0;

    for (blit_t i = (blit_t)0; i < BLIT_N; ++i)
    {
        lit_t l = clGet(cl, i);

        for (pcouples_t cp = (pcouples_t)0; cp < COUPLES_N; ++cp)
            if (litHas(l, cp))
            {
                if (!first)
                    s += part = sprintf(s, " v "), n += part;
                else
                    first = 0;
                s += part = sprintf(s, "(%s)", trans_map[i][cp][idx]);
                n += part;
            }
    }
    *s = '\0';

    return n;
}

rel_t blitToRel(blit_t i, pcouples_t pc, clsidx_t flag)
{
    if (flag == L_NEG) // così posso fare a mano solo i positivi
        return relInv(blitToRel(i, pc, L_POS));

    switch (i)
    {
    case E:
        switch (pc)
        {
        case AMBM: return RELSEC(BRADD4(IE, EQ, S, SI), REL_ALL);
        case AMBP: return BRTOR(MI);
        case APBM: return BRTOR(M);
        case APBP: return BRADD3(EQ, F, FI);
        default: return REL0;
        }
    case LE:
        switch (pc)
        {
        case AMBM: return RELSEC(BRADD11(IE, IB, IM, EQ, B, DI, FI, M, O, S, SI), REL_ALL);
        case AMBP: return RELSEC(BRADD16(IE, IB, IM, IMI, EQ, B, D, DI, F, FI, M, MI, O, OI, S, SI), REL_ALL);
        case APBM: return BRADD2(B, M);
        case APBP: return BRADD8(EQ, B, D, F, FI, M, O, S);
        default: return REL0;
        }
    case U:
        switch (pc)
        {
        case AMBM: return RELSEC(BRTOR(UN), REL_ALL);
        case AMBP: return RELSEC(BRADD2(UN, IBI), REL_ALL);
        case APBM: return RELSEC(BRADD2(UN, IB), REL_ALL);
        case APBP: return RELSEC(BRADD6(UN, IE, IB, IBI, IM, IMI), REL_ALL);
        default: return REL0;
        }
    case LU:
        switch (pc)
        {
        case AMBM: return RELSEC(BRADD8(UN, IB, IM, B, DI, FI, M, O), REL_ALL);
        case AMBP: return RELSEC(BRADD17(UN, IE, IB, IBI, IM, IMI, EQ, B, D, DI, F, FI, M, O, OI, S, SI), REL_ALL);
        case APBM: return RELSEC(BRADD3(UN, IB, B), REL_ALL);
        case APBP: return RELSEC(BRADD11(UN, IE, IB, IBI, IM, IMI, B, D, M, O, S), REL_ALL);
        default: return REL0;
        }
        // Posso ritornare la conversa dopo aver scambiato le coppie e la relazione
    case LEI: return relConv(blitToRel(LE, pcFlip(pc), flag));
    case LUI: return relConv(blitToRel(LU, pcFlip(pc), flag));
    default: return REL0;
    }
}

char litChar(pcouples_t pc, blit_t bl)
{
    switch (bl)
    {
    case E:
        switch (pc)
        {
        case AMBM: return 'a';
        case AMBP: return 'b';
        case APBM: return 'c';
        case APBP: return 'd';
        default: return '\0';
        }
    case LE:
        switch (pc)
        {
        case AMBM: return 'e';
        case AMBP: return 'f';
        case APBM: return 'g';
        case APBP: return 'h';
        default: return '\0';
        }
    case LEI:
        switch (pc)
        {
        case AMBM: return 'E';
        case AMBP: return 'F';
        case APBM: return 'G';
        case APBP: return 'H';
        default: return '\0';
        }
    case LU:
        switch (pc)
        {
        case AMBM: return 'i';
        case AMBP: return 'j';
        case APBM: return 'k';
        case APBP: return 'l';
        default: return '\0';
        }
    case LUI:
        switch (pc)
        {
        case AMBM: return 'I';
        case AMBP: return 'J';
        case APBM: return 'K';
        case APBP: return 'L';
        default: return '\0';
        }
    case U:
        switch (pc)
        {
        case AMBM: return 'm';
        case AMBP: return 'n';
        case APBM: return 'o';
        case APBP: return 'p';
        default: return '\0';
        }
    default: return '\0';
    }
}

litcoord_t litGetCoord(char c)
{
    switch (c)
    {
    case 'a': return (litcoord_t){E, AMBM};
    case 'b': return (litcoord_t){E, AMBP};
    case 'c': return (litcoord_t){E, APBM};
    case 'd': return (litcoord_t){E, APBP};
    case 'e': return (litcoord_t){LE, AMBM};
    case 'f': return (litcoord_t){LE, AMBP};
    case 'g': return (litcoord_t){LE, APBM};
    case 'h': return (litcoord_t){LE, APBP};
    case 'E': return (litcoord_t){LEI, AMBM};
    case 'F': return (litcoord_t){LEI, AMBP};
    case 'G': return (litcoord_t){LEI, APBM};
    case 'H': return (litcoord_t){LEI, APBP};
    case 'i': return (litcoord_t){LU, AMBM};
    case 'j': return (litcoord_t){LU, AMBP};
    case 'k': return (litcoord_t){LU, APBM};
    case 'l': return (litcoord_t){LU, APBP};
    case 'I': return (litcoord_t){LUI, AMBM};
    case 'J': return (litcoord_t){LUI, AMBP};
    case 'K': return (litcoord_t){LUI, APBM};
    case 'L': return (litcoord_t){LUI, APBP};
    case 'm': return (litcoord_t){U, AMBM};
    case 'n': return (litcoord_t){U, AMBP};
    case 'o': return (litcoord_t){U, APBM};
    case 'p': return (litcoord_t){U, APBP};
    default: return (litcoord_t){BLIT_N, COUPLES_N}; // errore
    }
}
