#pragma once

#include "settypes.h"

#include <stdio.h>


// Alloca e inizializza un insieme a 0
set_t setInit(uint dim);

// Inizializza una copia di set
set_t setInitCopy(set_t set);

// Inizializza un set da "dati grezzi"
set_t setInitRaw(uint8_t *data, uint dim);

// Incapsula i "dati grezzi", ma non crea una copia.
set_t setEmbedRaw(uint8_t *data, uint dim);

// Copia src in dst, e lo ritorna
void setCopy(set_t *dst, set_t src);

// Ritorna un elemento dell'insieme
uint8_t setGet(set_t set, uint idx);

// Inserisce un elemento nell'insieme, imposta il valore a piacere
void setPut(set_t *set, uint idx, uint8_t val);

// Inserisce l'elemento corrispondente all'indice nell'insieme
void setIns(set_t *set, uint idx);

// Rimuove l'elemento corrispondente all'indice dall'insieme
void setRem(set_t *set, uint idx);

// Stampa l'insieme su un file (binario o testuale)
void setPrint(FILE *f, set_t set, int bin);

// Azzera un insieme
void setEmpty(set_t set);

// Elimina un insieme
void setDel(set_t set);

// Unisce gli insiemi dst e src, inserendo gli elementi comuni in dst
int setMerge(set_t *dst, set_t src);

// Interseca due insiemi
int setSec(set_t *dst, set_t src);

// Ritorna 1 se il primo insieme è un sovra-insieme del secondo, 0 altrimenti
int setIsSuper(set_t set1, set_t set2);
