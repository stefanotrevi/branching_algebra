#pragma once

#include "closeflags.h"
#include "psettypes.h"
#include "rels/reltypes.h"
#include "rsettypes.h"

#include <stdio.h>

// Inizializza un insieme di dimensione REL_N
rset_t rsetInit(void);

// Elimina un insieme di relazioni
void rsetDel(rset_t rset);

// Ritorna una copia di set
rset_t rsetInitCopy(rset_t set);

// Inizializza un insieme di relazioni a partire da "dati grezzi"
rset_t rsetInitByRaw(uint8_t *data, uint dim);

// Inizializza un insieme di relazioni a partire da un array
rset_t rsetInitByArr(const rel_t *array, int n);

// Inizializza un insieme di rel_t a partire da uno di prel_t
rset_t rsetInitByPset(pset_t pset);

// Inizializza un insieme di rel_t a partire dalla disgiunzione di due insiemi puntuali
rset_t rsetInitByPdisj(pset_t pset1, pset_t pset2);

// Inizializza un insieme leggendo da un file
rset_t rsetInitByFile(FILE *f);

// Inizializza un insieme leggendo dal file con il nome specificato
rset_t rsetInitByFname(const char *fname);

// Ritorna un elemento dell'insieme
uint8_t rsetGet(rset_t rset, rel_t r);

// Inserisce un elemento nell'insieme, impostando un valore a piacere
void rsetPut(rset_t *rset, rel_t r, uint8_t val);

// Inserisce un elemento nell'insieme
void rsetIns(rset_t *rset, rel_t r);

// Rimuove un elemento dall'insieme
void rsetRem(rset_t *rset, rel_t r);

// Stampa un insieme di relazioni
void rsetPrint(FILE *f, rset_t rset);

// Stampa un insieme di relazioni in formato binario
void rsetWrite(FILE *f, rset_t rset);

// Raccoglie gli elementi di un insieme e li inserisce in un array
int rsetToArr(rset_t rset, rel_t *array);

// Unisce due insiemi
int rsetMerge(rset_t *dst, rset_t src);

// Interseca due insiemi
int rsetSec(rset_t *dst, rset_t src);

// Ritorna 1 se il primo insieme è un sovra-insieme del secondo, 0 altrimenti
int rsetIsSuper(rset_t set1, rset_t set2);

// Calcola la chiusura di un insieme di relazioni
int rsetClose(rset_t *set, closeflags_t flags);

// Calcola la chiusura e ritorna il +numero_elementi se non è corner, -numero_elementi altrimenti
int rsetCloseAndCheck(uint8_t *sup, uint8_t *hset, uint8_t *dset,
                      rel_t *done, rel_t *todo, uint n_todo, rel_t *todo_new);

// Ritorna se l'insieme è trattabile (secondo le algebre corner)
int rsetCheckCorner(rset_t set);
