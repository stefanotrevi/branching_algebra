#pragma once

#include "preltypes.h"
#include "reltypes.h"
#include "cpreltypes.h"
#include "sets/settypes.h"

#include <stdio.h>

// Ritorna una relazione fra punti vuota
prel_t prelInit(void);

// Somma due relazioni fra punti
prel_t prelAdd(prel_t pr1, prel_t pr2);

// Inserisce una relazione base tra punti ad una relazione tra punti
prel_t prelIns(prel_t pr, bprel_t bpr);

// Interseca due relazioni tra punti
prel_t prelSec(prel_t pr1, prel_t pr2);

// Ritorna il converso della relazione pr
prel_t prelConv(prel_t pr);

// Ritorna la relazione composta da due relazioni fra punti, usando una LUT
prel_t prelComp(prel_t pr1, prel_t pr2);

// Come prelComp, ma sfrutta la composizione di relazioni base
prel_t prelComp2(prel_t pr1, prel_t pr2);

// Calcola la path-consistency fra gli intervalli ij e jk (trovando ik)
prel_t prelPath(prel_t ij, prel_t jk, prel_t ik);

// Ritorna una relazione tra punti casuale
prel_t prelRand(void);

// Verifica se la relazione base tra punti è contenuta o meno
int prelHas(prel_t pr, bprel_t bpr);

// Ritorna se la relazione pr è nulla
int prelIsEmpty(prel_t pr);

// Ritorna se la relazione pr è la relazione tutto
int prelIsAll(prel_t pr);

// Inverte la coppia
pcouples_t pcFlip(pcouples_t pc);

// Converte una relazione tra punti in una stringa
int prelToStr(char *buf, prel_t pr);

// Converte una relazione tra punti in un carattere
const char *prelToChar(prel_t pr);

// Converte una relazione fra due estremi in una relazione fra intervalli
rel_t prelToRel(prel_t pr, pcouples_t pc);

// Converte un prel in un cprel contenente solo lui alla posizione pc
cprel_t prelToCprel(prel_t pr, pcouples_t pc);

// Stampa una relazione tra punti
FILE *prelPrint(FILE *f, prel_t pr);
