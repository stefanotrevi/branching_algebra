#pragma once

#include "rel.hpp"
#include "lut.hpp"
#include <cinttypes>

class Pspecs
{
public:
    using rel_t = uint8_t;
    enum brel_t : rel_t
    {
        UN,
        EQ,
        B,
        BI,
        BREL_N,
    };

    static constexpr rel_t CONV_LMASK{0b1000U};
    static constexpr rel_t CONV_RMASK{0b0100U};
    static constexpr rel_t CONV_SMASK{0b11U};
};

#define MPI_PREL_T MPI_UINT8_T // tipo per MPI

class Pbrel : public Brel<Pspecs>
{
};

class Prel : public Rel<Pspecs>
{
    Prel operator*(Prel s) { return lut::compose(*this, s); }
};

enum Partflag // flag per l'euristica della full-consistency
{
    PART_BASIC,
    PART_POINT,
    PART_HORN,
};
