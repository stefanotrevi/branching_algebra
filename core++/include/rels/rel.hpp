#pragma once

#include "defaults.hpp"

#include <ostream>
#include <string>
#include <vector>

template<typename Specs>
class Rel;

template<typename Specs>
class Brel
{
private:
    using brel_t = Specs::brel_t;
    using rel_t = Specs::rel_t;
    using Rel = Rel<Specs>;
    brel_t br;

public:
    Brel() : br{} {}
    Brel(brel_t brel) : br{brel} {}
    Brel(const std::string &str);

    operator rel_t() { return 1U << br; }
    operator brel_t() { return br; }

    int size();
    std::string to_string();
};

// Template for a generic relation: "Specs" must contain some mandatory members
template<typename Specs>
class Rel
{
private:
    // All the following elements must be provided by the Specs class
    using brel_t = Specs::brel_t; // underlying type for basic relations, must be an enumeration
    using rel_t = Specs::rel_t;   // underlying type for relations, must be an unsigned integer
    using Brel = Brel<Specs>;     // corresponding basic relations class

    static constexpr brel_t BREL_N{brel_t::BREL_N}; // Number of basic relations
    static constexpr rel_t LMSK{Specs::CONV_LMASK}; // left (or 'even') shift mask for converse
    static constexpr rel_t RMSK{Specs::CONV_RMASK}; // right (or 'odd') shift mask for converse
    static constexpr rel_t SMSK{Specs::CONV_SMASK}; // symmetric shift mask for converse
    // End of Requirements

    rel_t r{};

public:
    static constexpr rel_t REL_N{1U << BREL_N}; // useful in loops
    static constexpr rel_t REL_ALL{REL_N - 1U};

    Rel() {}
    Rel(rel_t rel) : r{rel} {}
    //    Rel(const std::string &str);

    constexpr explicit operator bool() const { return r; }
    constexpr operator rel_t() const { return r; }
    constexpr bool operator!() const { return !r; }
    // inverse
    constexpr Rel operator~() const { return ~r; }
    // converse
    constexpr Rel operator-() const { return ((r << 1U) & LMSK) | ((r >> 1U) & RMSK) | (r & SMSK); }
    // intersection
    constexpr Rel operator&(Rel s) const { return r & s; }
    // union
    constexpr Rel operator+(Rel s) const { return r | s; }
    // subtraction
    constexpr Rel operator-(Rel s) const { return r & ~s; }
    // membership
    constexpr bool operator[](Brel b) const { return r & b; }
    // weak composition
    constexpr Rel operator*(Rel s) const { return Specs::comptable(r, s); }

    // comparsion operators are defined by default in C++20, and do exactly what we need

    // Increment and decrement (useful in loops)
    constexpr Rel operator+=(Rel s) { return r += s; }
    constexpr Rel operator++() { return ++r; }
    constexpr Rel operator++(int) { return r++; }
    constexpr Rel operator-=(Rel s) { return r -= s; }
    constexpr Rel operator--() { return --r; }
    constexpr Rel operator--(int) { return r--; }

    // Number of elements contained
    constexpr int size() const { return popcount(r); }
    constexpr bool isAll() const { return r == REL_ALL; }
    constexpr bool isValid() const { return r < REL_N; }
    // Improper subset (for proper subset, check for inequality)
    constexpr bool isSub(Rel s) const { return (r & s) == r; }
};
