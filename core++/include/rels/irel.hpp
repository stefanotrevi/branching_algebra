#pragma once

#include "files.hpp"
#include "rel.hpp"

class Ispecs
{
public:
    using rel_t = uint32_t;
    enum brel_t : rel_t // relazioni base di BA
    {
        UN,
        IE,
        IB,
        IBI,
        IM,
        IMI,
        EQ,
        B,
        BI,
        D,
        DI,
        F,
        FI,
        M,
        MI,
        O,
        OI,
        S,
        SI,
        BREL_N,
    };
    class CompLut
    {
    private:
        /*
        * The composition table of interval relations is a 'bit-partitioned' table:
        * to avoid using 2TB of memory to store a full composition table, and to avoid the 
        * slowness of a really small table based on basic relations, we divide the relations
        * in two groups: all the ones resulting from setting any of the first 10 LSBs, and all 
        * the ones corresponding to the first 9 MSB (we try to split evenly).
        * We can then compute four tables by combining such elements, which help us compute
        * quickly the result of composition by using a modest amount of memory (~10^7 bytes).
        */
        rel_t *table;

        static constexpr rel_t LOW_BITS{10U};                // number of LOW bits
        static constexpr rel_t HIGH_BITS{BREL_N - LOW_BITS}; // number of HIGH bits

        static constexpr rel_t LOW_N{1U << LOW_BITS};   // LOW elements cardinality
        static constexpr rel_t HIGH_N{1U << HIGH_BITS}; // HIGH elements cardinality

        static constexpr rel_t LOW_MASK{LOW_N - 1U}; // selection mask

        // offsets of the 4 actual tables
        static constexpr rel_t LOW_LOW{0};
        static constexpr rel_t LOW_HIGH{LOW_N << LOW_BITS};
        static constexpr rel_t HIGH_LOW{LOW_HIGH + (LOW_N << HIGH_BITS)};
        static constexpr rel_t HIGH_HIGH{HIGH_LOW + (LOW_N << HIGH_BITS)};

        static constexpr rel_t TABLE_SIZE{HIGH_HIGH + (HIGH_N << HIGH_BITS)};

        rel_t high(rel_t r) const { return r & LOW_MASK; }
        rel_t low(rel_t r) const { return (r & ~LOW_MASK) >> LOW_BITS; };
        rel_t get(rel_t r, rel_t s, rel_t offset, rel_t lead_dim) const
        {
            return table[offset + r * lead_dim + s];
        }
        int build() const;

    public:
        CompLut();

        rel_t operator()(rel_t r, rel_t s) const
        {
            rel_t lr{low(r)}, hr{high(r)}, ls{low(s)}, hs{high(s)};

            return get(lr, ls, LOW_LOW, LOW_N) + get(lr, hs, LOW_HIGH, HIGH_N) +
                   get(hr, ls, HIGH_LOW, LOW_N) + get(hr, hs, HIGH_HIGH, HIGH_N);
        }
    };

    static constexpr char *MAP_PATH{FILE_DIR_RES "/ictab.dat"};
    static constexpr rel_t CONV_LMASK{0b01010101010100101000U};
    static constexpr rel_t CONV_RMASK{0b00101010101010010100U};
    static constexpr rel_t CONV_SMASK{0b1000011U};
    static const CompLut comptable;
};

#define MPI_REL_T MPI_UINT32_T // tipo per MPI

class Ibrel : public Brel<Ispecs>
{
};

class Irel : public Rel<Ispecs>
{
    Irel basic_compose(Irel s) const; // fallback composition method
    Irel pmap_compose(Irel s) const;  // composition via point-mapping (slowest one)
};

enum Partflag // flag per l'euristica della full-consistency
{
    PART_BASIC,
    PART_POINT,
    PART_HORN,
};
