#pragma once

#include "preltypes.h"
#include "reltypes.h"

// macro per forzare ottimizzazioni
#define PRELADD(pr1, pr2) ((prel_t)((pr1) | (pr2))) // equivale a prelAdd(pr1, pr2)
#define PRELSEC(pr1, pr2) ((prel_t)((pr1) & (pr2))) // equivale a prelSec(pr1, pr2)
#define BPRTOPR(bpr) ((prel_t)(1U << (bpr)))        // equivale a bprelToPrel(bpr)

// Servono macro fino a BPREL_N-1 (altrimenti basta usare PREL_ALL)
#define BPRADD2(b1, b2) PRELADD(BPRTOPR(b1), BPRTOPR(b2))
#define BPRADD3(b1, b2, b3) PRELADD(BPRADD2(b1, b2), BPRTOPR(b3))

// salva una relazione base fra punti in forma di clausola ORD
char *bprelToLitStr(char *s, bprel_t br, pcouples_t pc);

// trasforma una relazione tra punti base in una relazione tra punti
prel_t bprelToPrel(bprel_t bpr);

// ritorna la relazione composizione di due relazioni tra punti base
prel_t bprelComp(bprel_t bp1, bprel_t bp2);

// ritorna una stringa che rappresenta la relazione tra punti scelta
char *bprelToStr(bprel_t br);

// ritorna la relazione base conversa
bprel_t bprelConv(bprel_t br);

// Calcola la path-consistency fra relazioni base tra gli intervalli ij e jk, trovando ik
bprel_t bprelPath(bprel_t ij, bprel_t jk, bprel_t ik);

// Converte una relazione basica fra due estremi in una relazione fra intervalli
rel_t bprelToRel(bprel_t bp, pcouples_t pc);

// Converte una stringa in un pcouple
pcouples_t pcFromStr(const char *str);

// Converte una pcouple in una stringa
char *pcToStr(pcouples_t pc);
