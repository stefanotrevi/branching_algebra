#pragma once

#include "sets/psettypes.h"
#include "palgtypes.h"
#include "rels/reltypes.h"

// Ritorna l'insieme dell'algebra a punti scelta
pset_t palgInit(palgname_t name);

// Restituisce il nome dell'algebra scelta
const char *palgName(palgname_t name);

// Ricava l'algebra dal nome
palgname_t palgFromStr(const char *name);

// Converte l'algebra a punti scelta nelle relazioni intervallari corrispondenti
pset_t palgToRelSet(palgname_t name);

// Ritorna la lista delle algebre disponibili
void palgNotFound(void);
