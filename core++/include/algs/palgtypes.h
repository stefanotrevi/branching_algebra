#pragma once

typedef enum palgname
{
    PALG_GAMMA_A,
    PALG_GAMMA_B,
    PALG_GAMMA_D,
    PALG_GAMMA_E,
    PALG_DELTA_B,
    PALG_DELTA_C,
    PALG_DELTA_D,
    PALG_DELTA_E,
    PALG_OMEGA_A1,
    PALG_OMEGA_A2,
    PALG_OMEGA_B1,
    PALG_OMEGA_B2,
    PALG_OMEGA_C,
    PALG_OMEGA_D,
    PALG_OMEGA_E,
    PALG_ZETA_A,
    PALG_ZETA_B,
    PALG_ZETA_C1,
    PALG_ZETA_C2,
    PALG_ZETA_C3,
    PALG_ZETA_D,
    PALG_N,
} palgname_t;
