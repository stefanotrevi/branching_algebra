#pragma once

#include "rels/reltypes.h"

typedef struct net // una rete BA è un grafo, in cui gli indici sono intervalli
{
    int size;
    rel_t *rels; // matrice dei adiacenza delle relazioni nella rete
} net_t;
