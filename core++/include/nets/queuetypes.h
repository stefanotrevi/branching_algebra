#pragma once

#include "nettypes.h"

typedef struct qnode
{
    int i;
    struct qnode *next;
} qnode_t;

typedef struct queue
{
    qnode_t *head;
    qnode_t *tail;
} queue_t;
