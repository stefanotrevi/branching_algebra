#pragma once

#include "queuetypes.h"

// Ritorna una coda vuota
queue_t queueInit(void);

// Elimina una coda
void queueDel(queue_t q);

// Inserisce un elemento in fondo alla coda
queue_t *queuePut(queue_t *q, int i);

// Preleva un elemento dalla cima della coda
int queueGet(queue_t *q);

// Ritorna 1 se la coda è vuota, 0 altrimenti
int queueIsEmpty(queue_t q);
