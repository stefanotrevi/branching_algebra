#pragma once

#include "rels/reltypes.h"

// (2 * 64)-bit map, composta da due clit_t, per rappresentare una qualsiasi clausola ORD
typedef uint64_t cls_t;

/* (4 + 8)-bit map, 8 per i letterali negativi (A* != B*, A* lin B*), e altri
 * 4 per uno fra 16 positivi (=, <=, >=, ||)*/
typedef uint32_t ccls_t;

typedef enum clsidx
{
    L_NEG, // indice dei clit dei letterali negativi
    L_POS, // indice del clit dei letterali positivi
    L_NUM,
} clsidx_t;

#define MAX_CLS 128 // numero massimo di clausole (questo numero è "a caso")

typedef struct cnf // struttura per rappresentare una formula cnf
{
    cls_t cl[MAX_CLS];
    rel_t r; // relazione equivalente, utile averla in memoria
    int sz;
} cnf_t;

/* 
 * Macro per la generazione esaustiva di clausole ORD-Horn, per manipolare i ccls_t:
 * Le clausole Horn hanno AL MASSIMO un letterale positivo. I ccls_t, se incrementati da 0 al
 * valore massimo del corrispondente tipo, rappresentano TUTTE le possibili clausole ORD-Horn.
*/
#define CCLS_MASKPOS 0xFFFFFF00F000ULL           // maschera dei letterali positivi
#define CCLS_MASKNEG 0x00000000000FULL           // maschera dei letterali negativi
#define CCLS_NEGLIT 4ULL                         // numero di letterali negativi
#define CCLS_POSLIT 28ULL                        // numero di letterali positivi
#define CCLS_NEGMAX ((1U << CCLS_NEGLIT) - 1U)   // seleziona soltanto i letterali negativi
#define CCLS_POSMAX (CCLS_POSLIT << CCLS_NEGLIT) // seleziona soltanto i letterali positivi
#define CCLS_MAX (CCLS_NEGMAX | CCLS_POSMAX)     // clausola massima
#define MAX_CCNF_SZ (CCLS_MAX + 1U)              // lunghezza massima ccnf

typedef struct ccnf // struttura per rappresentare una formula cnf con clausole compresse
{
    ccls_t *ccl;
    int sz;
} ccnf_t;

#define M_DIV (BPREL_N - 1) // divisore per trasformare in stringa le relazioni
