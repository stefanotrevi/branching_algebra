#pragma once

#include "rels/cpreltypes.h"
#include "rels/breltypes.h"

typedef struct dnf
{
    cprel_t f[BREL_N];
    int sz;
} dnf_t;
