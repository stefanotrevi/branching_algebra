#pragma once

#include "cnftypes.h"
#include "littypes.h"

#include <stddef.h>

// Trasforma una posizione nel letterale corrispondente nella bitmap
lit_t pcToLit(pcouples_t pc);

// verifica se un letterale contiene la relazione fra i punti pc
int litHas(lit_t lit, pcouples_t pc);

// Unisce le coppie dei due letterali
lit_t litAdd(lit_t lit1, lit_t lit2);

// Inserisce la coppia PC a lit
lit_t litIns(lit_t lit, pcouples_t pc);

// rimuove i letterali comuni
lit_t litMask(lit_t lit, lit_t mask);

// Rimuove una coppia da un letterale
lit_t litSub(lit_t lit, pcouples_t pc);

// Converte un letterale in una relazione
rel_t litToRel(lit_t lit, blit_t li, clsidx_t ci);

// Inizializza un clit
clit_t clInit(void);

// Ritorna il numero di letterali positivi nel clit
int clCount(clit_t cl);

// Trasforma un clit in un array di letterali
lit_t *clToA(clit_t *cl);

// Ritorna lo shift per estrarre un particolare lit da un clit
clit_t clShift(blit_t i);

// Trasforma un lit in un clit con il lit nella posizione specificata
clit_t litToCl(lit_t l, blit_t i);

// Estrae da un clit il lit nella posizione specificata
lit_t clGet(clit_t cl, blit_t i);

// Verifica se un clit contiene la relazione fra i punti pc nel letterale i
int clHas(clit_t cl, blit_t i, pcouples_t pc);

// Unisce due clit
clit_t clAdd(clit_t cl1, clit_t cl2);

// Inserisce il lit nel clit, nella posizione specificata
clit_t clPut(clit_t cl, blit_t i, lit_t l);

// Inserisce la coppia pc al letterale i
clit_t clIns(clit_t cl, blit_t i, pcouples_t pc);

// Rimuove i letterali in comune fra i due clit
clit_t clSub(clit_t cl1, clit_t cl2);

// Rimuove i letterali in comune fra il clit e il lit
clit_t clMask(clit_t cl, blit_t i, lit_t l);

// Rimuove la coppia pc dal letterale i
clit_t clRem(clit_t cl, blit_t i, pcouples_t pc);

// Espande i letterali inserendo tutti gli implicanti
clit_t clMax(clit_t cl);

// Calcola i letterali minimi rimuovendo tutti gli implicati
clit_t clMin(clit_t cl);

// Converte un clit nella relazione corrispondente
rel_t clToRel(clit_t cl, clsidx_t ci);

// Converte un clit in una stringa. is_pos determina se i letterali sono positivi
size_t clToStr(char *s, clit_t cl, clsidx_t idx);

// trasforma il letterale cl[i][pc] nella relazione IA/BA corrispondente
rel_t blitToRel(blit_t i, pcouples_t pc, clsidx_t flag);

// Ritorna il carattere corrispondente ad un letterale
char litChar(pcouples_t pc, blit_t bl);

// Ritorna il (le coordinate del) letterale corrispondente ad un carattere
litcoord_t litGetCoord(char c);
