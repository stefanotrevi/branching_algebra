#pragma once

#include "cnftypes.h"
#include "littypes.h"
#include <stddef.h>

// Inizializza una clausola CNF
cls_t clsInit(void);

// Ritorna lo shift per estrarre un particolare clit da una clausola
cls_t clsShift(clsidx_t i);

// Trasforma un clit in un cls_t con gli stessi elementi nella posizione corrispondente
cls_t clToCls(clit_t cl, clsidx_t i);

// Verifica se una clausola contiene la relazione fra i punti pc nel letterale lit_i nel clit cls_i
int clsHas(cls_t cls, clsidx_t cls_i, blit_t lit_i, pcouples_t pc);

// Conta quanti letterali ci sono nella clausola
int clsCount(cls_t cls);

// Conta quanti letterali positivi sono presenti nella clausola
int clsPosCount(cls_t cls);

// Unisce gli elementi di due cls
cls_t clsAdd(cls_t cls1, cls_t cls2);

// Inserisce il clit nella posizione specificata del cls, unendone gli elementi
cls_t clsPut(cls_t cls, clsidx_t i, clit_t cl);

// Inserisce nella cls il letterale bl nella posizione desiderata
cls_t clsIns(cls_t cls, clsidx_t i, blit_t bl, pcouples_t pc);

// Inserisce nella cls il letterale corrispondente al carattere c (positivo o negativo)
cls_t clsInsChar(cls_t cls, char c, clsidx_t idx);

// Ritorna il clit nella posizione specificata
clit_t clsGet(cls_t cls, clsidx_t i);

// Converte una clausola CNF nella relazione corrispondente
rel_t clsToRel(cls_t cls);

// Converte una cls in una stringa
size_t clsToStr(char *buf, cls_t cls);

// Inizializza una CNF
cnf_t cnfInit(void);

// Inserisce solo se la relazione equivalente è diversa da quella ottenuta inserendo cls
int cnfIns(cnf_t *cnf, cls_t cls);

// Inizializza una clausola vuota
ccls_t cclsInit(void);

// Estrae una clausola cnf da una clausola compressa
cls_t cclsToCls(ccls_t ccl);

// Inizializza una CNF compressa
ccnf_t ccnfInit(void);

// Inserisce, se possibile, la prossima CNF, con max_depth clausole. Ritorna 0 in caso di fallimento
int ccnfNext(ccnf_t *cnf, int max_depth);

// Ritorna la relazione r implicata dalla CNF compressa
rel_t ccnfToRel(ccnf_t cnf);
