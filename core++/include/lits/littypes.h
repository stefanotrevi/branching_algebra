#pragma once

#include "rels/bpreltypes.h"

#include <inttypes.h>

typedef enum blit // relazioni ORD (o letterali base)
{
    E,   // equals
    LE,  // less or equals
    LEI, // less or equals inverso
    U,   // unrelated
    LU,  // less or unrelated
    LUI, // less or unrelated inverso
    BLIT_N,
} blit_t;

typedef uint8_t lit_t; // rappresenta tutti i letterali con gli stessi estremi

typedef uint32_t clit_t; // rappresenta tutti i letterali con lo stesso segno

typedef struct litcoord // struttura utile per la mappatura in caratteri
{
    blit_t bl;
    pcouples_t pc;
} litcoord_t;

#define ALL_LIT ((1U << (uint)(BLIT_N * COUPLES_N)) - 1U)
