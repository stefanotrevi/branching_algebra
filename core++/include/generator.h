#pragma once

#include "algs/palgtypes.h"
#include "algs/ralgtypes.h"
#include "rels/preltypes.h"

// Genera direttamente le relazioni Horn: (clausole Horn->relazioni->chiudi per intersezione)
void genHorn(int rank, int size);

// Genera l'insieme di tutte le relazioni "vicine"
void genNear(void);

// Genera tutte le relazioni puntizzabili
void genPoint(void);

// Genera tutte le clausole TORD-Horn diverse
void genTHornClauses(void);

// Genera i derivati dalle algebre disgiuntive
void genDisPoint(palgname_t fst, palgname_t snd, int rank, int size);

// Genera e valuta la massimalità di un'algebra a piacere fra quelle predefinite
void genRalg(ralgname_t alg, int rank, int size);

// Genera le relazioni binarie disgiuntive
void genDisjRelations(void);
