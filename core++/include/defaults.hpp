#pragma once

#define GLUE0(x, y) x##y
#define GLUE(x, y) GLUE0(x, y)
#define STRING0(x) #x
#define STRING(x) STRING0(x)

#ifdef _WIN32
    #include <intrin.h>
#else
    #include <x86intrin.h>
#endif

typedef unsigned int uint;

template<typename T>
constexpr int popcount(const T Val) noexcept
{
    return sizeof(T) <= sizeof(uint) ? _popcnt32(Val) : _popcnt64(Val);
}
