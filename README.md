# Interval Algebra Framework
**This framework was used to obtain part of the results presented in our paper ["Branching interval algebra: An almost complete picture"](https://doi.org/10.1016/j.ic.2021.104809), feel free to check it out!**

The framework consists of two parts: **core** and **utils**.

## Core libraries

The core is a set of libraries written in the C and CUDA programming languages to work with set of 
events (points or intervals), and temporal relations (linear or branching). 
Each sub-directory contains the functions and data structures to
represent and interact with different apsects of Interval or algebra. 

* **algs**: generation of maximal tractable subalgebras of IA, BPA and DBPA (disjunctive BPA).

* **lits**: TORD literals, clauses and formulae (both CNFs and DNFs).

* **nets**: networks (adjacency matrix) to conveniently represent and operate on sets of events and relations. 

* **rels**: point/interval linear/branching relations and operations over them (union, intersection, composition...), 
conversion functions.

* **sets**: data structures and functions to efficiently represent and operate on sets of interaval 
or point relations.

* **chronolib**: efficient, high-precision, easy to use and portable chronometer.

* **files**: portable functions for file operations, and predefined names for the input-output files used by the library.

* **filter**: TORD-Horn formulae filtering, requires the utilities `espresso` and `eqntott`.

* **generator**: "zero-setup" utility functions to generate interesting results.

* **lut**: generation and loading of the look-up tables used at runtime.

## Utils
The utils directory contains a couple auxiliary programs useful for checking results:

* `checker`: reads from *stdin* the name (without path and file extensions) of two sets located in the *core/out/* directory, prints useful comparative statistics, as well as more detailed informations about the second set.

* `translator`: reads from *stdin* a string representing a TORD equation, and prints the equivalent BA relation.

## Building

**Supported Systems**:
* Windows or Linux OS
* Intel Processor with AVX2 instruction set (e.g. Skylake)
* NVIDIA GPU with support for Unified Virtual Addressing

It is possible to configure the Makefile to change the behaviour of the compiled programs:
* **BRANCH**: if set to 1, libraries will be compiled in *branching mode*, i.e. all the 'reasoning' 
will be done in BA, otherwise the program will run in *linear mode*, basically ignoring branching 
relations (this is useful to compare IA and BA results).

* **DEBUG**: if set to 1, modifies compilation flags so to generate a debuggable program.

* **MPI_ENABLE**: if set to 1, uses the `mpicc` compiler to allow for parallel execution on a 
multi-CPU/GPU environment, to speed up the execution of some particularly intensive functions (e.g. 
maximality checking).

